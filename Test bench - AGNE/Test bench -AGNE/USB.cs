using System;
using System.IO;
using System.IO.Ports;

namespace Test_bench_AGNE
{
    public class USB
    {
        private SerialPort _modbusPort;
        private Relay_Control _relayControl;

        public USB(SerialPort modbusPort, Relay_Control relayControl)
        {
            _modbusPort = modbusPort;
            _relayControl = relayControl;

        }
        private static bool Connect_USB()
        {
            return false;
        }

        private static bool Disconnect_USB()
        {
            return false;
        }

        public bool Upload_Config(string configName, string sourceDirectory, string uploadDirectory)
        {
            return Upload_File(configName, ".bin", sourceDirectory, uploadDirectory);
        }

        public bool Upload_Firmware(string firmwareName, string sourceDirectory, string uploadDirectory)
        {
            return Upload_File(firmwareName, ".aes", sourceDirectory, uploadDirectory);
        }

        public static bool Upload_File(string file_Name, string file_Name_Extension, string source_Dir, string upload_directory)
        {

            string backupDir = @"F:\";

            Connect_USB();

            try
            {
                string[] file_List = Directory.GetFiles(source_Dir, "*." + file_Name_Extension);
                string[] fileList = Directory.GetFiles(backupDir, "*." + file_Name_Extension);
                foreach (string fileName in fileList)
                {
                    try
                    {
                        File.Delete(Path.Combine(backupDir, fileName)); //removes files from device before adding new files
                    }
                    catch { }

                }
                foreach (string file in file_List)
                {
                    string fName2 = file.Substring(source_Dir.Length + 1);
                    string fName = file[(source_Dir.Length + 1)..];
                    if (fName == (file_Name + "." + file_Name_Extension))
                    {
                        File.Copy(Path.Combine(source_Dir, fName), Path.Combine(backupDir, fName), true);
                        //Thread.Sleep(1000); // Sleep needed?
                        break;
                    }
                }
            }
            catch (DirectoryNotFoundException)
            {
                Logging.Write_Exceptions("Device drive " + backupDir + " not found");
                Disconnect_USB();
                return false;
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
                Disconnect_USB();
                return false;
            }

            Disconnect_USB();
            return true;
        }
    }
}
