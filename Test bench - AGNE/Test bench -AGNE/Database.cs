﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Test_bench_AGNE
{
    public class Database
    {
        private readonly string connString = "Server=srv2.bce.lt;Port=10002;Username=fm500q;Password=gariunai;Database=fm500q";

        public Database()
        {
        }


        public List<string> Get_SDK_Firmwares()
        {
            List<string> firmwares = new ();
            firmwares.Add("Select Firmware");
            try
            {
                using (NpgsqlConnection npgsqlConnection = new (connString))
                {
                    npgsqlConnection.Open();
                    NpgsqlDataReader npgsqlDataReader = (new NpgsqlCommand(@"SELECT DISTINCT firmwarename, date FROM firmwares WHERE firmwarename LIKE '%\_00_%' 
                                                                                                                                  OR firmwarename LIKE '%\_01_%' 
                                                                                                                                  OR firmwarename LIKE '%\_0B_%' 
                                                                                                                ORDER BY date DESC", npgsqlConnection)).ExecuteReader();
                    while (npgsqlDataReader.Read())
                    {
                        if(!firmwares.Contains(npgsqlDataReader[0].ToString().Substring(4)))
                        {
                            firmwares.Add(npgsqlDataReader[0].ToString().Substring(4));
                        }
                    }
                }

                return firmwares;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex);
                return firmwares;
            }
        }

    }
}
