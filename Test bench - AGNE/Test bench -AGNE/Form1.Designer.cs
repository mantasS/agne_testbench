﻿
namespace Test_bench_AGNE
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Can1_checkbox = new System.Windows.Forms.CheckBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.device36_checkBox = new System.Windows.Forms.CheckBox();
            this.device35_checkBox = new System.Windows.Forms.CheckBox();
            this.device34_checkBox = new System.Windows.Forms.CheckBox();
            this.device33_checkBox = new System.Windows.Forms.CheckBox();
            this.device32_checkBox = new System.Windows.Forms.CheckBox();
            this.device31_checkBox = new System.Windows.Forms.CheckBox();
            this.device30_checkBox = new System.Windows.Forms.CheckBox();
            this.device29_checkBox = new System.Windows.Forms.CheckBox();
            this.device24_checkBox = new System.Windows.Forms.CheckBox();
            this.device23_checkBox = new System.Windows.Forms.CheckBox();
            this.device28_checkBox = new System.Windows.Forms.CheckBox();
            this.device27_checkBox = new System.Windows.Forms.CheckBox();
            this.device26_checkBox = new System.Windows.Forms.CheckBox();
            this.device25_checkBox = new System.Windows.Forms.CheckBox();
            this.device22_checkBox = new System.Windows.Forms.CheckBox();
            this.device21_checkBox = new System.Windows.Forms.CheckBox();
            this.device20_checkBox = new System.Windows.Forms.CheckBox();
            this.device19_checkBox = new System.Windows.Forms.CheckBox();
            this.device18_checkBox = new System.Windows.Forms.CheckBox();
            this.device17_checkBox = new System.Windows.Forms.CheckBox();
            this.device16_checkBox = new System.Windows.Forms.CheckBox();
            this.device15_checkBox = new System.Windows.Forms.CheckBox();
            this.device14_checkBox = new System.Windows.Forms.CheckBox();
            this.device13_checkBox = new System.Windows.Forms.CheckBox();
            this.device12_checkBox = new System.Windows.Forms.CheckBox();
            this.device11_checkBox = new System.Windows.Forms.CheckBox();
            this.device6_checkBox = new System.Windows.Forms.CheckBox();
            this.device5_checkBox = new System.Windows.Forms.CheckBox();
            this.device10_checkBox = new System.Windows.Forms.CheckBox();
            this.device9_checkBox = new System.Windows.Forms.CheckBox();
            this.device8_checkBox = new System.Windows.Forms.CheckBox();
            this.device7_checkBox = new System.Windows.Forms.CheckBox();
            this.device4_checkBox = new System.Windows.Forms.CheckBox();
            this.device3_checkBox = new System.Windows.Forms.CheckBox();
            this.device2_checkbox = new System.Windows.Forms.CheckBox();
            this.device1_checkbox = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Stop_Test_Button = new System.Windows.Forms.Button();
            this.Start_Test_Button = new System.Windows.Forms.Button();
            this.Firmware_ComboBox = new System.Windows.Forms.ComboBox();
            this.Hardware_result_groupBox = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CAN1_textBox = new System.Windows.Forms.TextBox();
            this.OUT_12V_textBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.RS485_label = new System.Windows.Forms.Label();
            this.CAN2_textBox = new System.Windows.Forms.TextBox();
            this.RS485_textBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.RS232_label = new System.Windows.Forms.Label();
            this.ADC2_textBox = new System.Windows.Forms.TextBox();
            this.RS232_textBox = new System.Windows.Forms.TextBox();
            this.ADC2_label = new System.Windows.Forms.Label();
            this.OUT4_label = new System.Windows.Forms.Label();
            this.ADC3_textBox = new System.Windows.Forms.TextBox();
            this.OUT4_textBox = new System.Windows.Forms.TextBox();
            this.ADC3_label = new System.Windows.Forms.Label();
            this.OUT3_label = new System.Windows.Forms.Label();
            this.ADC4_textBox = new System.Windows.Forms.TextBox();
            this.OUT3_textBox = new System.Windows.Forms.TextBox();
            this.ADC4_label = new System.Windows.Forms.Label();
            this.OUT2_label = new System.Windows.Forms.Label();
            this.ADC5_textBox = new System.Windows.Forms.TextBox();
            this.OUT2_textBox = new System.Windows.Forms.TextBox();
            this.ADC5_label = new System.Windows.Forms.Label();
            this.OUT1_label = new System.Windows.Forms.Label();
            this.IN1_Motion_textBox = new System.Windows.Forms.TextBox();
            this.OUT1_textBox = new System.Windows.Forms.TextBox();
            this.IN1_label = new System.Windows.Forms.Label();
            this.IN8_label = new System.Windows.Forms.Label();
            this.IN2_textBox = new System.Windows.Forms.TextBox();
            this.IN8_textBox = new System.Windows.Forms.TextBox();
            this.IN2_label = new System.Windows.Forms.Label();
            this.IN7_label = new System.Windows.Forms.Label();
            this.IN3_textBox = new System.Windows.Forms.TextBox();
            this.IN7_textBox = new System.Windows.Forms.TextBox();
            this.IN3_label = new System.Windows.Forms.Label();
            this.IN6_label = new System.Windows.Forms.Label();
            this.IN4_textBox = new System.Windows.Forms.TextBox();
            this.IN6_textBox = new System.Windows.Forms.TextBox();
            this.IN4_label = new System.Windows.Forms.Label();
            this.IN5_label = new System.Windows.Forms.Label();
            this.IN5_textBox = new System.Windows.Forms.TextBox();
            this.Tachograph_label = new System.Windows.Forms.Label();
            this.Tachograph_textBox = new System.Windows.Forms.TextBox();
            this.Peripheral_results_groupBox = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Script_textBox = new System.Windows.Forms.TextBox();
            this.BLE_extender_textBox = new System.Windows.Forms.TextBox();
            this.BLE_Extender_label = new System.Windows.Forms.Label();
            this.Ibutton_textBox = new System.Windows.Forms.TextBox();
            this.Ibutton_label = new System.Windows.Forms.Label();
            this.Temp_sensor_textBox = new System.Windows.Forms.TextBox();
            this.Temp_sensor_label = new System.Windows.Forms.Label();
            this.Functions_Test_Results_groupBox = new System.Windows.Forms.GroupBox();
            this.CurrentTestLogBox = new System.Windows.Forms.TextBox();
            this.LogText = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.Info_textBox = new System.Windows.Forms.TextBox();
            this.New_device_groupBox = new System.Windows.Forms.GroupBox();
            this.newDevice_sdkdevice_checkBox = new System.Windows.Forms.CheckBox();
            this.newDevice_addDevice_button = new System.Windows.Forms.Button();
            this.New_device_cancel_button = new System.Windows.Forms.Button();
            this.Ble_Extender_checkBox = new System.Windows.Forms.CheckBox();
            this.Ibutton_checkBox = new System.Windows.Forms.CheckBox();
            this.Temp_sensor_checkBox = new System.Windows.Forms.CheckBox();
            this.Tachograph_checkBox = new System.Windows.Forms.CheckBox();
            this.RS485_checkBox = new System.Windows.Forms.CheckBox();
            this.RS232_checkBox = new System.Windows.Forms.CheckBox();
            this.OUT_12V_checkBox = new System.Windows.Forms.CheckBox();
            this.OUT4_checkBox = new System.Windows.Forms.CheckBox();
            this.OUT3_checkBox = new System.Windows.Forms.CheckBox();
            this.OUT2_checkBox = new System.Windows.Forms.CheckBox();
            this.OUT1_checkBox = new System.Windows.Forms.CheckBox();
            this.In8_checkBox = new System.Windows.Forms.CheckBox();
            this.In7_checkBox = new System.Windows.Forms.CheckBox();
            this.In6_checkBox = new System.Windows.Forms.CheckBox();
            this.In5_checkBox = new System.Windows.Forms.CheckBox();
            this.IN4_checkbox = new System.Windows.Forms.CheckBox();
            this.In3_checkbox = new System.Windows.Forms.CheckBox();
            this.In2_checkbox = new System.Windows.Forms.CheckBox();
            this.In1_motion_checkbox = new System.Windows.Forms.CheckBox();
            this.Adc5_checkbox = new System.Windows.Forms.CheckBox();
            this.Adc4_checkbox = new System.Windows.Forms.CheckBox();
            this.Adc3_checkbox = new System.Windows.Forms.CheckBox();
            this.Adc2_checkbox = new System.Windows.Forms.CheckBox();
            this.Can2_checkbox = new System.Windows.Forms.CheckBox();
            this.newDevice_phoneNumber_textBox = new System.Windows.Forms.TextBox();
            this.newDevice_hwVersion_textBox = new System.Windows.Forms.TextBox();
            this.newDevice_IMEI_textBox = new System.Windows.Forms.TextBox();
            this.newDevice_AssemblyRev_textBox = new System.Windows.Forms.TextBox();
            this.newDevice_RelayId_textBox = new System.Windows.Forms.TextBox();
            this.newdevice_deviceName_textBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.configsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenConfigFile_toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openLogsFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewDeviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.deviceComPortName = new System.Windows.Forms.Label();
            this.modbusCommPortName = new System.Windows.Forms.Label();
            this.psuPortName = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.auxPsuPortName = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.Hardware_result_groupBox.SuspendLayout();
            this.Peripheral_results_groupBox.SuspendLayout();
            this.Functions_Test_Results_groupBox.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.New_device_groupBox.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Can1_checkbox
            // 
            this.Can1_checkbox.AutoSize = true;
            this.Can1_checkbox.Checked = true;
            this.Can1_checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Can1_checkbox.Location = new System.Drawing.Point(15, 194);
            this.Can1_checkbox.Name = "Can1_checkbox";
            this.Can1_checkbox.Size = new System.Drawing.Size(57, 19);
            this.Can1_checkbox.TabIndex = 57;
            this.Can1_checkbox.Text = "CAN1";
            this.Can1_checkbox.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.device36_checkBox);
            this.groupBox1.Controls.Add(this.device35_checkBox);
            this.groupBox1.Controls.Add(this.device34_checkBox);
            this.groupBox1.Controls.Add(this.device33_checkBox);
            this.groupBox1.Controls.Add(this.device32_checkBox);
            this.groupBox1.Controls.Add(this.device31_checkBox);
            this.groupBox1.Controls.Add(this.device30_checkBox);
            this.groupBox1.Controls.Add(this.device29_checkBox);
            this.groupBox1.Controls.Add(this.device24_checkBox);
            this.groupBox1.Controls.Add(this.device23_checkBox);
            this.groupBox1.Controls.Add(this.device28_checkBox);
            this.groupBox1.Controls.Add(this.device27_checkBox);
            this.groupBox1.Controls.Add(this.device26_checkBox);
            this.groupBox1.Controls.Add(this.device25_checkBox);
            this.groupBox1.Controls.Add(this.device22_checkBox);
            this.groupBox1.Controls.Add(this.device21_checkBox);
            this.groupBox1.Controls.Add(this.device20_checkBox);
            this.groupBox1.Controls.Add(this.device19_checkBox);
            this.groupBox1.Controls.Add(this.device18_checkBox);
            this.groupBox1.Controls.Add(this.device17_checkBox);
            this.groupBox1.Controls.Add(this.device16_checkBox);
            this.groupBox1.Controls.Add(this.device15_checkBox);
            this.groupBox1.Controls.Add(this.device14_checkBox);
            this.groupBox1.Controls.Add(this.device13_checkBox);
            this.groupBox1.Controls.Add(this.device12_checkBox);
            this.groupBox1.Controls.Add(this.device11_checkBox);
            this.groupBox1.Controls.Add(this.device6_checkBox);
            this.groupBox1.Controls.Add(this.device5_checkBox);
            this.groupBox1.Controls.Add(this.device10_checkBox);
            this.groupBox1.Controls.Add(this.device9_checkBox);
            this.groupBox1.Controls.Add(this.device8_checkBox);
            this.groupBox1.Controls.Add(this.device7_checkBox);
            this.groupBox1.Controls.Add(this.device4_checkBox);
            this.groupBox1.Controls.Add(this.device3_checkBox);
            this.groupBox1.Controls.Add(this.device2_checkbox);
            this.groupBox1.Controls.Add(this.device1_checkbox);
            this.groupBox1.Location = new System.Drawing.Point(363, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1210, 198);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Devices";
            // 
            // device36_checkBox
            // 
            this.device36_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device36_checkBox.Checked = true;
            this.device36_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device36_checkBox.Location = new System.Drawing.Point(1024, 164);
            this.device36_checkBox.Name = "device36_checkBox";
            this.device36_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device36_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device36_checkBox.TabIndex = 70;
            this.device36_checkBox.UseVisualStyleBackColor = false;
            this.device36_checkBox.Visible = false;
            this.device36_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device35_checkBox
            // 
            this.device35_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device35_checkBox.Checked = true;
            this.device35_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device35_checkBox.Location = new System.Drawing.Point(1024, 135);
            this.device35_checkBox.Name = "device35_checkBox";
            this.device35_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device35_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device35_checkBox.TabIndex = 68;
            this.device35_checkBox.UseVisualStyleBackColor = false;
            this.device35_checkBox.Visible = false;
            this.device35_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device34_checkBox
            // 
            this.device34_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device34_checkBox.Checked = true;
            this.device34_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device34_checkBox.Location = new System.Drawing.Point(1024, 106);
            this.device34_checkBox.Name = "device34_checkBox";
            this.device34_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device34_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device34_checkBox.TabIndex = 66;
            this.device34_checkBox.UseVisualStyleBackColor = false;
            this.device34_checkBox.Visible = false;
            this.device34_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device33_checkBox
            // 
            this.device33_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device33_checkBox.Checked = true;
            this.device33_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device33_checkBox.Location = new System.Drawing.Point(1024, 77);
            this.device33_checkBox.Name = "device33_checkBox";
            this.device33_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device33_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device33_checkBox.TabIndex = 64;
            this.device33_checkBox.UseVisualStyleBackColor = false;
            this.device33_checkBox.Visible = false;
            this.device33_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device32_checkBox
            // 
            this.device32_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device32_checkBox.Checked = true;
            this.device32_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device32_checkBox.Location = new System.Drawing.Point(1024, 48);
            this.device32_checkBox.Name = "device32_checkBox";
            this.device32_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device32_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device32_checkBox.TabIndex = 62;
            this.device32_checkBox.UseVisualStyleBackColor = false;
            this.device32_checkBox.Visible = false;
            this.device32_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device31_checkBox
            // 
            this.device31_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device31_checkBox.Checked = true;
            this.device31_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device31_checkBox.Location = new System.Drawing.Point(1024, 19);
            this.device31_checkBox.Name = "device31_checkBox";
            this.device31_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device31_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device31_checkBox.TabIndex = 60;
            this.device31_checkBox.UseVisualStyleBackColor = false;
            this.device31_checkBox.Visible = false;
            this.device31_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device30_checkBox
            // 
            this.device30_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device30_checkBox.Checked = true;
            this.device30_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device30_checkBox.Location = new System.Drawing.Point(824, 164);
            this.device30_checkBox.Name = "device30_checkBox";
            this.device30_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device30_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device30_checkBox.TabIndex = 58;
            this.device30_checkBox.UseVisualStyleBackColor = false;
            this.device30_checkBox.Visible = false;
            this.device30_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device29_checkBox
            // 
            this.device29_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device29_checkBox.Checked = true;
            this.device29_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device29_checkBox.Location = new System.Drawing.Point(824, 135);
            this.device29_checkBox.Name = "device29_checkBox";
            this.device29_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device29_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device29_checkBox.TabIndex = 56;
            this.device29_checkBox.UseVisualStyleBackColor = false;
            this.device29_checkBox.Visible = false;
            this.device29_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device24_checkBox
            // 
            this.device24_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device24_checkBox.Checked = true;
            this.device24_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device24_checkBox.Location = new System.Drawing.Point(618, 164);
            this.device24_checkBox.Name = "device24_checkBox";
            this.device24_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device24_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device24_checkBox.TabIndex = 54;
            this.device24_checkBox.UseVisualStyleBackColor = false;
            this.device24_checkBox.Visible = false;
            this.device24_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device23_checkBox
            // 
            this.device23_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device23_checkBox.Checked = true;
            this.device23_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device23_checkBox.Location = new System.Drawing.Point(618, 135);
            this.device23_checkBox.Name = "device23_checkBox";
            this.device23_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device23_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device23_checkBox.TabIndex = 52;
            this.device23_checkBox.UseVisualStyleBackColor = false;
            this.device23_checkBox.Visible = false;
            this.device23_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device28_checkBox
            // 
            this.device28_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device28_checkBox.Checked = true;
            this.device28_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device28_checkBox.Location = new System.Drawing.Point(824, 106);
            this.device28_checkBox.Name = "device28_checkBox";
            this.device28_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device28_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device28_checkBox.TabIndex = 50;
            this.device28_checkBox.UseVisualStyleBackColor = false;
            this.device28_checkBox.Visible = false;
            this.device28_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device27_checkBox
            // 
            this.device27_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device27_checkBox.Checked = true;
            this.device27_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device27_checkBox.Location = new System.Drawing.Point(824, 77);
            this.device27_checkBox.Name = "device27_checkBox";
            this.device27_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device27_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device27_checkBox.TabIndex = 48;
            this.device27_checkBox.UseVisualStyleBackColor = false;
            this.device27_checkBox.Visible = false;
            this.device27_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device26_checkBox
            // 
            this.device26_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device26_checkBox.Checked = true;
            this.device26_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device26_checkBox.Location = new System.Drawing.Point(824, 48);
            this.device26_checkBox.Name = "device26_checkBox";
            this.device26_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device26_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device26_checkBox.TabIndex = 46;
            this.device26_checkBox.UseVisualStyleBackColor = false;
            this.device26_checkBox.Visible = false;
            this.device26_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device25_checkBox
            // 
            this.device25_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device25_checkBox.Checked = true;
            this.device25_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device25_checkBox.Location = new System.Drawing.Point(824, 19);
            this.device25_checkBox.Name = "device25_checkBox";
            this.device25_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device25_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device25_checkBox.TabIndex = 44;
            this.device25_checkBox.UseVisualStyleBackColor = false;
            this.device25_checkBox.Visible = false;
            this.device25_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device22_checkBox
            // 
            this.device22_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device22_checkBox.Checked = true;
            this.device22_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device22_checkBox.Location = new System.Drawing.Point(618, 106);
            this.device22_checkBox.Name = "device22_checkBox";
            this.device22_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device22_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device22_checkBox.TabIndex = 42;
            this.device22_checkBox.UseVisualStyleBackColor = false;
            this.device22_checkBox.Visible = false;
            this.device22_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device21_checkBox
            // 
            this.device21_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device21_checkBox.Checked = true;
            this.device21_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device21_checkBox.Location = new System.Drawing.Point(618, 77);
            this.device21_checkBox.Name = "device21_checkBox";
            this.device21_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device21_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device21_checkBox.TabIndex = 40;
            this.device21_checkBox.UseVisualStyleBackColor = false;
            this.device21_checkBox.Visible = false;
            this.device21_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device20_checkBox
            // 
            this.device20_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device20_checkBox.Checked = true;
            this.device20_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device20_checkBox.Location = new System.Drawing.Point(618, 48);
            this.device20_checkBox.Name = "device20_checkBox";
            this.device20_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device20_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device20_checkBox.TabIndex = 38;
            this.device20_checkBox.UseVisualStyleBackColor = false;
            this.device20_checkBox.Visible = false;
            this.device20_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device19_checkBox
            // 
            this.device19_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device19_checkBox.Checked = true;
            this.device19_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device19_checkBox.Location = new System.Drawing.Point(618, 19);
            this.device19_checkBox.Name = "device19_checkBox";
            this.device19_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device19_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device19_checkBox.TabIndex = 36;
            this.device19_checkBox.UseVisualStyleBackColor = false;
            this.device19_checkBox.Visible = false;
            this.device19_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device18_checkBox
            // 
            this.device18_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device18_checkBox.Checked = true;
            this.device18_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device18_checkBox.Location = new System.Drawing.Point(414, 164);
            this.device18_checkBox.Name = "device18_checkBox";
            this.device18_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device18_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device18_checkBox.TabIndex = 34;
            this.device18_checkBox.UseVisualStyleBackColor = false;
            this.device18_checkBox.Visible = false;
            this.device18_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device17_checkBox
            // 
            this.device17_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device17_checkBox.Checked = true;
            this.device17_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device17_checkBox.Location = new System.Drawing.Point(414, 135);
            this.device17_checkBox.Name = "device17_checkBox";
            this.device17_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device17_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device17_checkBox.TabIndex = 32;
            this.device17_checkBox.UseVisualStyleBackColor = false;
            this.device17_checkBox.Visible = false;
            this.device17_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device16_checkBox
            // 
            this.device16_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device16_checkBox.Checked = true;
            this.device16_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device16_checkBox.Location = new System.Drawing.Point(414, 106);
            this.device16_checkBox.Name = "device16_checkBox";
            this.device16_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device16_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device16_checkBox.TabIndex = 30;
            this.device16_checkBox.UseVisualStyleBackColor = false;
            this.device16_checkBox.Visible = false;
            this.device16_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device15_checkBox
            // 
            this.device15_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device15_checkBox.Checked = true;
            this.device15_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device15_checkBox.Location = new System.Drawing.Point(414, 77);
            this.device15_checkBox.Name = "device15_checkBox";
            this.device15_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device15_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device15_checkBox.TabIndex = 28;
            this.device15_checkBox.UseVisualStyleBackColor = false;
            this.device15_checkBox.Visible = false;
            // 
            // device14_checkBox
            // 
            this.device14_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device14_checkBox.Checked = true;
            this.device14_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device14_checkBox.Location = new System.Drawing.Point(414, 48);
            this.device14_checkBox.Name = "device14_checkBox";
            this.device14_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device14_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device14_checkBox.TabIndex = 26;
            this.device14_checkBox.UseVisualStyleBackColor = false;
            this.device14_checkBox.Visible = false;
            this.device14_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device13_checkBox
            // 
            this.device13_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device13_checkBox.Checked = true;
            this.device13_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device13_checkBox.Location = new System.Drawing.Point(414, 19);
            this.device13_checkBox.Name = "device13_checkBox";
            this.device13_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device13_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device13_checkBox.TabIndex = 24;
            this.device13_checkBox.UseVisualStyleBackColor = false;
            this.device13_checkBox.Visible = false;
            this.device13_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device12_checkBox
            // 
            this.device12_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device12_checkBox.Checked = true;
            this.device12_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device12_checkBox.Location = new System.Drawing.Point(211, 164);
            this.device12_checkBox.Name = "device12_checkBox";
            this.device12_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device12_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device12_checkBox.TabIndex = 22;
            this.device12_checkBox.UseVisualStyleBackColor = false;
            this.device12_checkBox.Visible = false;
            this.device12_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device11_checkBox
            // 
            this.device11_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device11_checkBox.Checked = true;
            this.device11_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device11_checkBox.Location = new System.Drawing.Point(211, 135);
            this.device11_checkBox.Name = "device11_checkBox";
            this.device11_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device11_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device11_checkBox.TabIndex = 20;
            this.device11_checkBox.UseVisualStyleBackColor = false;
            this.device11_checkBox.Visible = false;
            this.device11_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device6_checkBox
            // 
            this.device6_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device6_checkBox.Checked = true;
            this.device6_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device6_checkBox.Location = new System.Drawing.Point(5, 164);
            this.device6_checkBox.Name = "device6_checkBox";
            this.device6_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device6_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device6_checkBox.TabIndex = 18;
            this.device6_checkBox.UseVisualStyleBackColor = false;
            this.device6_checkBox.Visible = false;
            this.device6_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device5_checkBox
            // 
            this.device5_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device5_checkBox.Checked = true;
            this.device5_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device5_checkBox.Location = new System.Drawing.Point(5, 135);
            this.device5_checkBox.Name = "device5_checkBox";
            this.device5_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device5_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device5_checkBox.TabIndex = 16;
            this.device5_checkBox.UseVisualStyleBackColor = false;
            this.device5_checkBox.Visible = false;
            this.device5_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device10_checkBox
            // 
            this.device10_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device10_checkBox.Checked = true;
            this.device10_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device10_checkBox.Location = new System.Drawing.Point(211, 106);
            this.device10_checkBox.Name = "device10_checkBox";
            this.device10_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device10_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device10_checkBox.TabIndex = 14;
            this.device10_checkBox.UseVisualStyleBackColor = false;
            this.device10_checkBox.Visible = false;
            this.device10_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device9_checkBox
            // 
            this.device9_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device9_checkBox.Checked = true;
            this.device9_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device9_checkBox.Location = new System.Drawing.Point(211, 77);
            this.device9_checkBox.Name = "device9_checkBox";
            this.device9_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device9_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device9_checkBox.TabIndex = 12;
            this.device9_checkBox.UseVisualStyleBackColor = false;
            this.device9_checkBox.Visible = false;
            this.device9_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device8_checkBox
            // 
            this.device8_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device8_checkBox.Checked = true;
            this.device8_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device8_checkBox.Location = new System.Drawing.Point(211, 48);
            this.device8_checkBox.Name = "device8_checkBox";
            this.device8_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device8_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device8_checkBox.TabIndex = 10;
            this.device8_checkBox.UseVisualStyleBackColor = false;
            this.device8_checkBox.Visible = false;
            this.device8_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device7_checkBox
            // 
            this.device7_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device7_checkBox.Checked = true;
            this.device7_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device7_checkBox.Location = new System.Drawing.Point(211, 19);
            this.device7_checkBox.Name = "device7_checkBox";
            this.device7_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device7_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device7_checkBox.TabIndex = 8;
            this.device7_checkBox.UseVisualStyleBackColor = false;
            this.device7_checkBox.Visible = false;
            this.device7_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device4_checkBox
            // 
            this.device4_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device4_checkBox.Checked = true;
            this.device4_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device4_checkBox.Location = new System.Drawing.Point(5, 106);
            this.device4_checkBox.Name = "device4_checkBox";
            this.device4_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device4_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device4_checkBox.TabIndex = 6;
            this.device4_checkBox.UseVisualStyleBackColor = false;
            this.device4_checkBox.Visible = false;
            this.device4_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device3_checkBox
            // 
            this.device3_checkBox.BackColor = System.Drawing.Color.LimeGreen;
            this.device3_checkBox.Checked = true;
            this.device3_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device3_checkBox.Location = new System.Drawing.Point(5, 77);
            this.device3_checkBox.Name = "device3_checkBox";
            this.device3_checkBox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device3_checkBox.Size = new System.Drawing.Size(190, 18);
            this.device3_checkBox.TabIndex = 4;
            this.device3_checkBox.UseVisualStyleBackColor = false;
            this.device3_checkBox.Visible = false;
            this.device3_checkBox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device2_checkbox
            // 
            this.device2_checkbox.BackColor = System.Drawing.Color.LimeGreen;
            this.device2_checkbox.Checked = true;
            this.device2_checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device2_checkbox.Location = new System.Drawing.Point(5, 48);
            this.device2_checkbox.Name = "device2_checkbox";
            this.device2_checkbox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device2_checkbox.Size = new System.Drawing.Size(190, 18);
            this.device2_checkbox.TabIndex = 2;
            this.device2_checkbox.UseVisualStyleBackColor = false;
            this.device2_checkbox.Visible = false;
            this.device2_checkbox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // device1_checkbox
            // 
            this.device1_checkbox.BackColor = System.Drawing.Color.LimeGreen;
            this.device1_checkbox.Checked = true;
            this.device1_checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.device1_checkbox.Location = new System.Drawing.Point(5, 19);
            this.device1_checkbox.Name = "device1_checkbox";
            this.device1_checkbox.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.device1_checkbox.Size = new System.Drawing.Size(190, 18);
            this.device1_checkbox.TabIndex = 0;
            this.device1_checkbox.Text = "textas";
            this.device1_checkbox.UseVisualStyleBackColor = false;
            this.device1_checkbox.Visible = false;
            this.device1_checkbox.CheckedChanged += new System.EventHandler(this.Device_checkbox_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Stop_Test_Button);
            this.groupBox3.Controls.Add(this.Start_Test_Button);
            this.groupBox3.Controls.Add(this.Firmware_ComboBox);
            this.groupBox3.Location = new System.Drawing.Point(9, 50);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(348, 125);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            // 
            // Stop_Test_Button
            // 
            this.Stop_Test_Button.Location = new System.Drawing.Point(87, 15);
            this.Stop_Test_Button.Name = "Stop_Test_Button";
            this.Stop_Test_Button.Size = new System.Drawing.Size(75, 23);
            this.Stop_Test_Button.TabIndex = 2;
            this.Stop_Test_Button.Text = "Stop Test";
            this.Stop_Test_Button.UseVisualStyleBackColor = true;
            this.Stop_Test_Button.Click += new System.EventHandler(this.Stop_Test_Button_Click);
            // 
            // Start_Test_Button
            // 
            this.Start_Test_Button.Location = new System.Drawing.Point(6, 15);
            this.Start_Test_Button.Name = "Start_Test_Button";
            this.Start_Test_Button.Size = new System.Drawing.Size(75, 23);
            this.Start_Test_Button.TabIndex = 1;
            this.Start_Test_Button.Text = "Start Test";
            this.Start_Test_Button.UseVisualStyleBackColor = true;
            this.Start_Test_Button.EnabledChanged += new System.EventHandler(this.Start_Test_Button_EnableChanged);
            this.Start_Test_Button.Click += new System.EventHandler(this.Start_Test_Button_Click);
            // 
            // Firmware_ComboBox
            // 
            this.Firmware_ComboBox.BackColor = System.Drawing.SystemColors.Info;
            this.Firmware_ComboBox.FormattingEnabled = true;
            this.Firmware_ComboBox.Location = new System.Drawing.Point(7, 44);
            this.Firmware_ComboBox.Name = "Firmware_ComboBox";
            this.Firmware_ComboBox.Size = new System.Drawing.Size(202, 23);
            this.Firmware_ComboBox.TabIndex = 0;
            this.Firmware_ComboBox.Text = "Select Firmware";
            this.Firmware_ComboBox.SelectedIndexChanged += new System.EventHandler(this.Firmware_ComboBox_IndexChanged);
            this.Firmware_ComboBox.Click += new System.EventHandler(this.Firmware_ComboBox_Click);
            // 
            // Hardware_result_groupBox
            // 
            this.Hardware_result_groupBox.Controls.Add(this.label3);
            this.Hardware_result_groupBox.Controls.Add(this.CAN1_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.OUT_12V_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.label1);
            this.Hardware_result_groupBox.Controls.Add(this.RS485_label);
            this.Hardware_result_groupBox.Controls.Add(this.CAN2_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.RS485_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.label2);
            this.Hardware_result_groupBox.Controls.Add(this.RS232_label);
            this.Hardware_result_groupBox.Controls.Add(this.ADC2_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.RS232_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.ADC2_label);
            this.Hardware_result_groupBox.Controls.Add(this.OUT4_label);
            this.Hardware_result_groupBox.Controls.Add(this.ADC3_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.OUT4_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.ADC3_label);
            this.Hardware_result_groupBox.Controls.Add(this.OUT3_label);
            this.Hardware_result_groupBox.Controls.Add(this.ADC4_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.OUT3_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.ADC4_label);
            this.Hardware_result_groupBox.Controls.Add(this.OUT2_label);
            this.Hardware_result_groupBox.Controls.Add(this.ADC5_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.OUT2_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.ADC5_label);
            this.Hardware_result_groupBox.Controls.Add(this.OUT1_label);
            this.Hardware_result_groupBox.Controls.Add(this.IN1_Motion_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.OUT1_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.IN1_label);
            this.Hardware_result_groupBox.Controls.Add(this.IN8_label);
            this.Hardware_result_groupBox.Controls.Add(this.IN2_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.IN8_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.IN2_label);
            this.Hardware_result_groupBox.Controls.Add(this.IN7_label);
            this.Hardware_result_groupBox.Controls.Add(this.IN3_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.IN7_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.IN3_label);
            this.Hardware_result_groupBox.Controls.Add(this.IN6_label);
            this.Hardware_result_groupBox.Controls.Add(this.IN4_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.IN6_textBox);
            this.Hardware_result_groupBox.Controls.Add(this.IN4_label);
            this.Hardware_result_groupBox.Controls.Add(this.IN5_label);
            this.Hardware_result_groupBox.Controls.Add(this.IN5_textBox);
            this.Hardware_result_groupBox.Location = new System.Drawing.Point(10, 254);
            this.Hardware_result_groupBox.Name = "Hardware_result_groupBox";
            this.Hardware_result_groupBox.Size = new System.Drawing.Size(386, 595);
            this.Hardware_result_groupBox.TabIndex = 3;
            this.Hardware_result_groupBox.TabStop = false;
            this.Hardware_result_groupBox.Text = "Hardware results";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 543);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 15);
            this.label3.TabIndex = 43;
            this.label3.Text = "OUT_12V";
            // 
            // CAN1_textBox
            // 
            this.CAN1_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.CAN1_textBox.Location = new System.Drawing.Point(86, 18);
            this.CAN1_textBox.Name = "CAN1_textBox";
            this.CAN1_textBox.Size = new System.Drawing.Size(100, 23);
            this.CAN1_textBox.TabIndex = 0;
            // 
            // OUT_12V_textBox
            // 
            this.OUT_12V_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.OUT_12V_textBox.Location = new System.Drawing.Point(86, 540);
            this.OUT_12V_textBox.Name = "OUT_12V_textBox";
            this.OUT_12V_textBox.Size = new System.Drawing.Size(100, 23);
            this.OUT_12V_textBox.TabIndex = 42;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "CAN1";
            // 
            // RS485_label
            // 
            this.RS485_label.AutoSize = true;
            this.RS485_label.Location = new System.Drawing.Point(203, 50);
            this.RS485_label.Name = "RS485_label";
            this.RS485_label.Size = new System.Drawing.Size(38, 15);
            this.RS485_label.TabIndex = 41;
            this.RS485_label.Text = "RS485";
            // 
            // CAN2_textBox
            // 
            this.CAN2_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.CAN2_textBox.Location = new System.Drawing.Point(86, 47);
            this.CAN2_textBox.Name = "CAN2_textBox";
            this.CAN2_textBox.Size = new System.Drawing.Size(100, 23);
            this.CAN2_textBox.TabIndex = 2;
            // 
            // RS485_textBox
            // 
            this.RS485_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.RS485_textBox.Location = new System.Drawing.Point(278, 47);
            this.RS485_textBox.Name = "RS485_textBox";
            this.RS485_textBox.Size = new System.Drawing.Size(100, 23);
            this.RS485_textBox.TabIndex = 40;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "CAN2";
            // 
            // RS232_label
            // 
            this.RS232_label.AutoSize = true;
            this.RS232_label.Location = new System.Drawing.Point(203, 21);
            this.RS232_label.Name = "RS232_label";
            this.RS232_label.Size = new System.Drawing.Size(38, 15);
            this.RS232_label.TabIndex = 39;
            this.RS232_label.Text = "RS232";
            // 
            // ADC2_textBox
            // 
            this.ADC2_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.ADC2_textBox.Location = new System.Drawing.Point(86, 76);
            this.ADC2_textBox.Name = "ADC2_textBox";
            this.ADC2_textBox.Size = new System.Drawing.Size(100, 23);
            this.ADC2_textBox.TabIndex = 6;
            // 
            // RS232_textBox
            // 
            this.RS232_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.RS232_textBox.Location = new System.Drawing.Point(278, 18);
            this.RS232_textBox.Name = "RS232_textBox";
            this.RS232_textBox.Size = new System.Drawing.Size(100, 23);
            this.RS232_textBox.TabIndex = 38;
            // 
            // ADC2_label
            // 
            this.ADC2_label.AutoSize = true;
            this.ADC2_label.Location = new System.Drawing.Point(11, 79);
            this.ADC2_label.Name = "ADC2_label";
            this.ADC2_label.Size = new System.Drawing.Size(37, 15);
            this.ADC2_label.TabIndex = 7;
            this.ADC2_label.Text = "ADC2";
            // 
            // OUT4_label
            // 
            this.OUT4_label.AutoSize = true;
            this.OUT4_label.Location = new System.Drawing.Point(11, 514);
            this.OUT4_label.Name = "OUT4_label";
            this.OUT4_label.Size = new System.Drawing.Size(36, 15);
            this.OUT4_label.TabIndex = 37;
            this.OUT4_label.Text = "OUT4";
            // 
            // ADC3_textBox
            // 
            this.ADC3_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.ADC3_textBox.Location = new System.Drawing.Point(86, 105);
            this.ADC3_textBox.Name = "ADC3_textBox";
            this.ADC3_textBox.Size = new System.Drawing.Size(100, 23);
            this.ADC3_textBox.TabIndex = 8;
            // 
            // OUT4_textBox
            // 
            this.OUT4_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.OUT4_textBox.Location = new System.Drawing.Point(86, 511);
            this.OUT4_textBox.Name = "OUT4_textBox";
            this.OUT4_textBox.Size = new System.Drawing.Size(100, 23);
            this.OUT4_textBox.TabIndex = 36;
            // 
            // ADC3_label
            // 
            this.ADC3_label.AutoSize = true;
            this.ADC3_label.Location = new System.Drawing.Point(11, 108);
            this.ADC3_label.Name = "ADC3_label";
            this.ADC3_label.Size = new System.Drawing.Size(37, 15);
            this.ADC3_label.TabIndex = 9;
            this.ADC3_label.Text = "ADC3";
            // 
            // OUT3_label
            // 
            this.OUT3_label.AutoSize = true;
            this.OUT3_label.Location = new System.Drawing.Point(11, 485);
            this.OUT3_label.Name = "OUT3_label";
            this.OUT3_label.Size = new System.Drawing.Size(36, 15);
            this.OUT3_label.TabIndex = 35;
            this.OUT3_label.Text = "OUT3";
            // 
            // ADC4_textBox
            // 
            this.ADC4_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.ADC4_textBox.Location = new System.Drawing.Point(86, 134);
            this.ADC4_textBox.Name = "ADC4_textBox";
            this.ADC4_textBox.Size = new System.Drawing.Size(100, 23);
            this.ADC4_textBox.TabIndex = 10;
            // 
            // OUT3_textBox
            // 
            this.OUT3_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.OUT3_textBox.Location = new System.Drawing.Point(86, 482);
            this.OUT3_textBox.Name = "OUT3_textBox";
            this.OUT3_textBox.Size = new System.Drawing.Size(100, 23);
            this.OUT3_textBox.TabIndex = 34;
            // 
            // ADC4_label
            // 
            this.ADC4_label.AutoSize = true;
            this.ADC4_label.Location = new System.Drawing.Point(11, 137);
            this.ADC4_label.Name = "ADC4_label";
            this.ADC4_label.Size = new System.Drawing.Size(37, 15);
            this.ADC4_label.TabIndex = 11;
            this.ADC4_label.Text = "ADC4";
            // 
            // OUT2_label
            // 
            this.OUT2_label.AutoSize = true;
            this.OUT2_label.Location = new System.Drawing.Point(11, 456);
            this.OUT2_label.Name = "OUT2_label";
            this.OUT2_label.Size = new System.Drawing.Size(36, 15);
            this.OUT2_label.TabIndex = 33;
            this.OUT2_label.Text = "OUT2";
            // 
            // ADC5_textBox
            // 
            this.ADC5_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.ADC5_textBox.Location = new System.Drawing.Point(86, 163);
            this.ADC5_textBox.Name = "ADC5_textBox";
            this.ADC5_textBox.Size = new System.Drawing.Size(100, 23);
            this.ADC5_textBox.TabIndex = 12;
            // 
            // OUT2_textBox
            // 
            this.OUT2_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.OUT2_textBox.Location = new System.Drawing.Point(86, 453);
            this.OUT2_textBox.Name = "OUT2_textBox";
            this.OUT2_textBox.Size = new System.Drawing.Size(100, 23);
            this.OUT2_textBox.TabIndex = 32;
            // 
            // ADC5_label
            // 
            this.ADC5_label.AutoSize = true;
            this.ADC5_label.Location = new System.Drawing.Point(11, 166);
            this.ADC5_label.Name = "ADC5_label";
            this.ADC5_label.Size = new System.Drawing.Size(37, 15);
            this.ADC5_label.TabIndex = 13;
            this.ADC5_label.Text = "ADC5";
            // 
            // OUT1_label
            // 
            this.OUT1_label.AutoSize = true;
            this.OUT1_label.Location = new System.Drawing.Point(11, 427);
            this.OUT1_label.Name = "OUT1_label";
            this.OUT1_label.Size = new System.Drawing.Size(36, 15);
            this.OUT1_label.TabIndex = 31;
            this.OUT1_label.Text = "OUT1";
            // 
            // IN1_Motion_textBox
            // 
            this.IN1_Motion_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.IN1_Motion_textBox.Location = new System.Drawing.Point(86, 192);
            this.IN1_Motion_textBox.Name = "IN1_Motion_textBox";
            this.IN1_Motion_textBox.Size = new System.Drawing.Size(100, 23);
            this.IN1_Motion_textBox.TabIndex = 14;
            // 
            // OUT1_textBox
            // 
            this.OUT1_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.OUT1_textBox.Location = new System.Drawing.Point(86, 424);
            this.OUT1_textBox.Name = "OUT1_textBox";
            this.OUT1_textBox.Size = new System.Drawing.Size(100, 23);
            this.OUT1_textBox.TabIndex = 30;
            // 
            // IN1_label
            // 
            this.IN1_label.AutoSize = true;
            this.IN1_label.Location = new System.Drawing.Point(11, 195);
            this.IN1_label.Name = "IN1_label";
            this.IN1_label.Size = new System.Drawing.Size(69, 15);
            this.IN1_label.TabIndex = 15;
            this.IN1_label.Text = "IN1_Motion";
            // 
            // IN8_label
            // 
            this.IN8_label.AutoSize = true;
            this.IN8_label.Location = new System.Drawing.Point(11, 398);
            this.IN8_label.Name = "IN8_label";
            this.IN8_label.Size = new System.Drawing.Size(25, 15);
            this.IN8_label.TabIndex = 29;
            this.IN8_label.Text = "IN8";
            // 
            // IN2_textBox
            // 
            this.IN2_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.IN2_textBox.Location = new System.Drawing.Point(86, 221);
            this.IN2_textBox.Name = "IN2_textBox";
            this.IN2_textBox.Size = new System.Drawing.Size(100, 23);
            this.IN2_textBox.TabIndex = 16;
            // 
            // IN8_textBox
            // 
            this.IN8_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.IN8_textBox.Location = new System.Drawing.Point(86, 395);
            this.IN8_textBox.Name = "IN8_textBox";
            this.IN8_textBox.Size = new System.Drawing.Size(100, 23);
            this.IN8_textBox.TabIndex = 28;
            // 
            // IN2_label
            // 
            this.IN2_label.AutoSize = true;
            this.IN2_label.Location = new System.Drawing.Point(11, 224);
            this.IN2_label.Name = "IN2_label";
            this.IN2_label.Size = new System.Drawing.Size(25, 15);
            this.IN2_label.TabIndex = 17;
            this.IN2_label.Text = "IN2";
            // 
            // IN7_label
            // 
            this.IN7_label.AutoSize = true;
            this.IN7_label.Location = new System.Drawing.Point(11, 369);
            this.IN7_label.Name = "IN7_label";
            this.IN7_label.Size = new System.Drawing.Size(25, 15);
            this.IN7_label.TabIndex = 27;
            this.IN7_label.Text = "IN7";
            // 
            // IN3_textBox
            // 
            this.IN3_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.IN3_textBox.Location = new System.Drawing.Point(86, 250);
            this.IN3_textBox.Name = "IN3_textBox";
            this.IN3_textBox.Size = new System.Drawing.Size(100, 23);
            this.IN3_textBox.TabIndex = 18;
            // 
            // IN7_textBox
            // 
            this.IN7_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.IN7_textBox.Location = new System.Drawing.Point(86, 366);
            this.IN7_textBox.Name = "IN7_textBox";
            this.IN7_textBox.Size = new System.Drawing.Size(100, 23);
            this.IN7_textBox.TabIndex = 26;
            // 
            // IN3_label
            // 
            this.IN3_label.AutoSize = true;
            this.IN3_label.Location = new System.Drawing.Point(11, 253);
            this.IN3_label.Name = "IN3_label";
            this.IN3_label.Size = new System.Drawing.Size(25, 15);
            this.IN3_label.TabIndex = 19;
            this.IN3_label.Text = "IN3";
            // 
            // IN6_label
            // 
            this.IN6_label.AutoSize = true;
            this.IN6_label.Location = new System.Drawing.Point(11, 340);
            this.IN6_label.Name = "IN6_label";
            this.IN6_label.Size = new System.Drawing.Size(25, 15);
            this.IN6_label.TabIndex = 25;
            this.IN6_label.Text = "IN6";
            // 
            // IN4_textBox
            // 
            this.IN4_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.IN4_textBox.Location = new System.Drawing.Point(86, 279);
            this.IN4_textBox.Name = "IN4_textBox";
            this.IN4_textBox.Size = new System.Drawing.Size(100, 23);
            this.IN4_textBox.TabIndex = 20;
            // 
            // IN6_textBox
            // 
            this.IN6_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.IN6_textBox.Location = new System.Drawing.Point(86, 337);
            this.IN6_textBox.Name = "IN6_textBox";
            this.IN6_textBox.Size = new System.Drawing.Size(100, 23);
            this.IN6_textBox.TabIndex = 24;
            // 
            // IN4_label
            // 
            this.IN4_label.AutoSize = true;
            this.IN4_label.Location = new System.Drawing.Point(11, 282);
            this.IN4_label.Name = "IN4_label";
            this.IN4_label.Size = new System.Drawing.Size(25, 15);
            this.IN4_label.TabIndex = 21;
            this.IN4_label.Text = "IN4";
            // 
            // IN5_label
            // 
            this.IN5_label.AutoSize = true;
            this.IN5_label.Location = new System.Drawing.Point(11, 311);
            this.IN5_label.Name = "IN5_label";
            this.IN5_label.Size = new System.Drawing.Size(25, 15);
            this.IN5_label.TabIndex = 23;
            this.IN5_label.Text = "IN5";
            // 
            // IN5_textBox
            // 
            this.IN5_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.IN5_textBox.Location = new System.Drawing.Point(86, 308);
            this.IN5_textBox.Name = "IN5_textBox";
            this.IN5_textBox.Size = new System.Drawing.Size(100, 23);
            this.IN5_textBox.TabIndex = 22;
            // 
            // Tachograph_label
            // 
            this.Tachograph_label.AutoSize = true;
            this.Tachograph_label.Location = new System.Drawing.Point(6, 25);
            this.Tachograph_label.Name = "Tachograph_label";
            this.Tachograph_label.Size = new System.Drawing.Size(69, 15);
            this.Tachograph_label.TabIndex = 5;
            this.Tachograph_label.Text = "Tachograph";
            // 
            // Tachograph_textBox
            // 
            this.Tachograph_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.Tachograph_textBox.Location = new System.Drawing.Point(81, 22);
            this.Tachograph_textBox.Name = "Tachograph_textBox";
            this.Tachograph_textBox.Size = new System.Drawing.Size(100, 23);
            this.Tachograph_textBox.TabIndex = 4;
            // 
            // Peripheral_results_groupBox
            // 
            this.Peripheral_results_groupBox.Controls.Add(this.label12);
            this.Peripheral_results_groupBox.Controls.Add(this.Script_textBox);
            this.Peripheral_results_groupBox.Controls.Add(this.BLE_extender_textBox);
            this.Peripheral_results_groupBox.Controls.Add(this.BLE_Extender_label);
            this.Peripheral_results_groupBox.Controls.Add(this.Ibutton_textBox);
            this.Peripheral_results_groupBox.Controls.Add(this.Ibutton_label);
            this.Peripheral_results_groupBox.Controls.Add(this.Temp_sensor_textBox);
            this.Peripheral_results_groupBox.Controls.Add(this.Temp_sensor_label);
            this.Peripheral_results_groupBox.Controls.Add(this.Tachograph_textBox);
            this.Peripheral_results_groupBox.Controls.Add(this.Tachograph_label);
            this.Peripheral_results_groupBox.Location = new System.Drawing.Point(402, 254);
            this.Peripheral_results_groupBox.Name = "Peripheral_results_groupBox";
            this.Peripheral_results_groupBox.Size = new System.Drawing.Size(195, 594);
            this.Peripheral_results_groupBox.TabIndex = 4;
            this.Peripheral_results_groupBox.TabStop = false;
            this.Peripheral_results_groupBox.Text = "Peripherals results";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 142);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 15);
            this.label12.TabIndex = 45;
            this.label12.Text = "Script";
            // 
            // Script_textBox
            // 
            this.Script_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.Script_textBox.Location = new System.Drawing.Point(81, 138);
            this.Script_textBox.Name = "Script_textBox";
            this.Script_textBox.Size = new System.Drawing.Size(100, 23);
            this.Script_textBox.TabIndex = 44;
            // 
            // BLE_extender_textBox
            // 
            this.BLE_extender_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.BLE_extender_textBox.Location = new System.Drawing.Point(81, 109);
            this.BLE_extender_textBox.Name = "BLE_extender_textBox";
            this.BLE_extender_textBox.Size = new System.Drawing.Size(100, 23);
            this.BLE_extender_textBox.TabIndex = 10;
            // 
            // BLE_Extender_label
            // 
            this.BLE_Extender_label.AutoSize = true;
            this.BLE_Extender_label.Location = new System.Drawing.Point(6, 112);
            this.BLE_Extender_label.Name = "BLE_Extender_label";
            this.BLE_Extender_label.Size = new System.Drawing.Size(75, 15);
            this.BLE_Extender_label.TabIndex = 11;
            this.BLE_Extender_label.Text = "BLE extender";
            // 
            // Ibutton_textBox
            // 
            this.Ibutton_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.Ibutton_textBox.Location = new System.Drawing.Point(81, 80);
            this.Ibutton_textBox.Name = "Ibutton_textBox";
            this.Ibutton_textBox.Size = new System.Drawing.Size(100, 23);
            this.Ibutton_textBox.TabIndex = 8;
            // 
            // Ibutton_label
            // 
            this.Ibutton_label.AutoSize = true;
            this.Ibutton_label.Location = new System.Drawing.Point(6, 83);
            this.Ibutton_label.Name = "Ibutton_label";
            this.Ibutton_label.Size = new System.Drawing.Size(46, 15);
            this.Ibutton_label.TabIndex = 9;
            this.Ibutton_label.Text = "Ibutton";
            // 
            // Temp_sensor_textBox
            // 
            this.Temp_sensor_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.Temp_sensor_textBox.Location = new System.Drawing.Point(81, 51);
            this.Temp_sensor_textBox.Name = "Temp_sensor_textBox";
            this.Temp_sensor_textBox.Size = new System.Drawing.Size(100, 23);
            this.Temp_sensor_textBox.TabIndex = 6;
            // 
            // Temp_sensor_label
            // 
            this.Temp_sensor_label.AutoSize = true;
            this.Temp_sensor_label.Location = new System.Drawing.Point(6, 54);
            this.Temp_sensor_label.Name = "Temp_sensor_label";
            this.Temp_sensor_label.Size = new System.Drawing.Size(73, 15);
            this.Temp_sensor_label.TabIndex = 7;
            this.Temp_sensor_label.Text = "Temp sensor";
            // 
            // Functions_Test_Results_groupBox
            // 
            this.Functions_Test_Results_groupBox.Controls.Add(this.CurrentTestLogBox);
            this.Functions_Test_Results_groupBox.Controls.Add(this.LogText);
            this.Functions_Test_Results_groupBox.Location = new System.Drawing.Point(603, 254);
            this.Functions_Test_Results_groupBox.Name = "Functions_Test_Results_groupBox";
            this.Functions_Test_Results_groupBox.Size = new System.Drawing.Size(970, 593);
            this.Functions_Test_Results_groupBox.TabIndex = 5;
            this.Functions_Test_Results_groupBox.TabStop = false;
            this.Functions_Test_Results_groupBox.Text = "Functions test results";
            // 
            // CurrentTestLogBox
            // 
            this.CurrentTestLogBox.Location = new System.Drawing.Point(6, 43);
            this.CurrentTestLogBox.Multiline = true;
            this.CurrentTestLogBox.Name = "CurrentTestLogBox";
            this.CurrentTestLogBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.CurrentTestLogBox.Size = new System.Drawing.Size(439, 540);
            this.CurrentTestLogBox.TabIndex = 13;
            // 
            // LogText
            // 
            this.LogText.AutoSize = true;
            this.LogText.Location = new System.Drawing.Point(6, 25);
            this.LogText.Name = "LogText";
            this.LogText.Size = new System.Drawing.Size(93, 15);
            this.LogText.TabIndex = 12;
            this.LogText.Text = "Current Test Log";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.Info_textBox);
            this.groupBox7.Location = new System.Drawing.Point(10, 176);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(346, 72);
            this.groupBox7.TabIndex = 6;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Info";
            // 
            // Info_textBox
            // 
            this.Info_textBox.BackColor = System.Drawing.SystemColors.Info;
            this.Info_textBox.Location = new System.Drawing.Point(7, 22);
            this.Info_textBox.Name = "Info_textBox";
            this.Info_textBox.Size = new System.Drawing.Size(333, 23);
            this.Info_textBox.TabIndex = 72;
            // 
            // New_device_groupBox
            // 
            this.New_device_groupBox.Controls.Add(this.newDevice_sdkdevice_checkBox);
            this.New_device_groupBox.Controls.Add(this.newDevice_addDevice_button);
            this.New_device_groupBox.Controls.Add(this.New_device_cancel_button);
            this.New_device_groupBox.Controls.Add(this.Ble_Extender_checkBox);
            this.New_device_groupBox.Controls.Add(this.Ibutton_checkBox);
            this.New_device_groupBox.Controls.Add(this.Temp_sensor_checkBox);
            this.New_device_groupBox.Controls.Add(this.Tachograph_checkBox);
            this.New_device_groupBox.Controls.Add(this.RS485_checkBox);
            this.New_device_groupBox.Controls.Add(this.RS232_checkBox);
            this.New_device_groupBox.Controls.Add(this.OUT_12V_checkBox);
            this.New_device_groupBox.Controls.Add(this.OUT4_checkBox);
            this.New_device_groupBox.Controls.Add(this.OUT3_checkBox);
            this.New_device_groupBox.Controls.Add(this.OUT2_checkBox);
            this.New_device_groupBox.Controls.Add(this.OUT1_checkBox);
            this.New_device_groupBox.Controls.Add(this.In8_checkBox);
            this.New_device_groupBox.Controls.Add(this.In7_checkBox);
            this.New_device_groupBox.Controls.Add(this.In6_checkBox);
            this.New_device_groupBox.Controls.Add(this.In5_checkBox);
            this.New_device_groupBox.Controls.Add(this.IN4_checkbox);
            this.New_device_groupBox.Controls.Add(this.In3_checkbox);
            this.New_device_groupBox.Controls.Add(this.In2_checkbox);
            this.New_device_groupBox.Controls.Add(this.In1_motion_checkbox);
            this.New_device_groupBox.Controls.Add(this.Adc5_checkbox);
            this.New_device_groupBox.Controls.Add(this.Adc4_checkbox);
            this.New_device_groupBox.Controls.Add(this.Adc3_checkbox);
            this.New_device_groupBox.Controls.Add(this.Adc2_checkbox);
            this.New_device_groupBox.Controls.Add(this.Can2_checkbox);
            this.New_device_groupBox.Controls.Add(this.Can1_checkbox);
            this.New_device_groupBox.Controls.Add(this.newDevice_phoneNumber_textBox);
            this.New_device_groupBox.Controls.Add(this.newDevice_hwVersion_textBox);
            this.New_device_groupBox.Controls.Add(this.newDevice_IMEI_textBox);
            this.New_device_groupBox.Controls.Add(this.newDevice_AssemblyRev_textBox);
            this.New_device_groupBox.Controls.Add(this.newDevice_RelayId_textBox);
            this.New_device_groupBox.Controls.Add(this.newdevice_deviceName_textBox);
            this.New_device_groupBox.Controls.Add(this.label10);
            this.New_device_groupBox.Controls.Add(this.label8);
            this.New_device_groupBox.Controls.Add(this.label7);
            this.New_device_groupBox.Controls.Add(this.label6);
            this.New_device_groupBox.Controls.Add(this.label5);
            this.New_device_groupBox.Controls.Add(this.label4);
            this.New_device_groupBox.Location = new System.Drawing.Point(9, 254);
            this.New_device_groupBox.Name = "New_device_groupBox";
            this.New_device_groupBox.Size = new System.Drawing.Size(384, 595);
            this.New_device_groupBox.TabIndex = 44;
            this.New_device_groupBox.TabStop = false;
            this.New_device_groupBox.Text = "New device";
            this.New_device_groupBox.Visible = false;
            // 
            // newDevice_sdkdevice_checkBox
            // 
            this.newDevice_sdkdevice_checkBox.AutoSize = true;
            this.newDevice_sdkdevice_checkBox.Checked = true;
            this.newDevice_sdkdevice_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.newDevice_sdkdevice_checkBox.Location = new System.Drawing.Point(268, 397);
            this.newDevice_sdkdevice_checkBox.Name = "newDevice_sdkdevice_checkBox";
            this.newDevice_sdkdevice_checkBox.Size = new System.Drawing.Size(84, 19);
            this.newDevice_sdkdevice_checkBox.TabIndex = 84;
            this.newDevice_sdkdevice_checkBox.Text = "SDK device";
            this.newDevice_sdkdevice_checkBox.UseVisualStyleBackColor = true;
            // 
            // newDevice_addDevice_button
            // 
            this.newDevice_addDevice_button.Location = new System.Drawing.Point(223, 566);
            this.newDevice_addDevice_button.Name = "newDevice_addDevice_button";
            this.newDevice_addDevice_button.Size = new System.Drawing.Size(75, 23);
            this.newDevice_addDevice_button.TabIndex = 83;
            this.newDevice_addDevice_button.Text = "Add device";
            this.newDevice_addDevice_button.UseVisualStyleBackColor = true;
            this.newDevice_addDevice_button.Click += new System.EventHandler(this.NewDevice_addDevice_button_Click);
            // 
            // New_device_cancel_button
            // 
            this.New_device_cancel_button.Location = new System.Drawing.Point(304, 566);
            this.New_device_cancel_button.Name = "New_device_cancel_button";
            this.New_device_cancel_button.Size = new System.Drawing.Size(75, 23);
            this.New_device_cancel_button.TabIndex = 82;
            this.New_device_cancel_button.Text = "Cancel";
            this.New_device_cancel_button.UseVisualStyleBackColor = true;
            this.New_device_cancel_button.Click += new System.EventHandler(this.New_device_cancel_button_Click);
            // 
            // Ble_Extender_checkBox
            // 
            this.Ble_Extender_checkBox.AutoSize = true;
            this.Ble_Extender_checkBox.Checked = true;
            this.Ble_Extender_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Ble_Extender_checkBox.Location = new System.Drawing.Point(137, 426);
            this.Ble_Extender_checkBox.Name = "Ble_Extender_checkBox";
            this.Ble_Extender_checkBox.Size = new System.Drawing.Size(91, 19);
            this.Ble_Extender_checkBox.TabIndex = 81;
            this.Ble_Extender_checkBox.Text = "Ble Extender";
            this.Ble_Extender_checkBox.UseVisualStyleBackColor = true;
            // 
            // Ibutton_checkBox
            // 
            this.Ibutton_checkBox.AutoSize = true;
            this.Ibutton_checkBox.Checked = true;
            this.Ibutton_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Ibutton_checkBox.Location = new System.Drawing.Point(137, 397);
            this.Ibutton_checkBox.Name = "Ibutton_checkBox";
            this.Ibutton_checkBox.Size = new System.Drawing.Size(65, 19);
            this.Ibutton_checkBox.TabIndex = 80;
            this.Ibutton_checkBox.Text = "Ibutton";
            this.Ibutton_checkBox.UseVisualStyleBackColor = true;
            // 
            // Temp_sensor_checkBox
            // 
            this.Temp_sensor_checkBox.AutoSize = true;
            this.Temp_sensor_checkBox.Checked = true;
            this.Temp_sensor_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Temp_sensor_checkBox.Location = new System.Drawing.Point(15, 426);
            this.Temp_sensor_checkBox.Name = "Temp_sensor_checkBox";
            this.Temp_sensor_checkBox.Size = new System.Drawing.Size(92, 19);
            this.Temp_sensor_checkBox.TabIndex = 79;
            this.Temp_sensor_checkBox.Text = "Temp sensor";
            this.Temp_sensor_checkBox.UseVisualStyleBackColor = true;
            // 
            // Tachograph_checkBox
            // 
            this.Tachograph_checkBox.AutoSize = true;
            this.Tachograph_checkBox.Checked = true;
            this.Tachograph_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Tachograph_checkBox.Location = new System.Drawing.Point(15, 397);
            this.Tachograph_checkBox.Name = "Tachograph_checkBox";
            this.Tachograph_checkBox.Size = new System.Drawing.Size(88, 19);
            this.Tachograph_checkBox.TabIndex = 78;
            this.Tachograph_checkBox.Text = "Tachograph";
            this.Tachograph_checkBox.UseVisualStyleBackColor = true;
            // 
            // RS485_checkBox
            // 
            this.RS485_checkBox.AutoSize = true;
            this.RS485_checkBox.Checked = true;
            this.RS485_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RS485_checkBox.Location = new System.Drawing.Point(268, 368);
            this.RS485_checkBox.Name = "RS485_checkBox";
            this.RS485_checkBox.Size = new System.Drawing.Size(57, 19);
            this.RS485_checkBox.TabIndex = 77;
            this.RS485_checkBox.Text = "RS485";
            this.RS485_checkBox.UseVisualStyleBackColor = true;
            // 
            // RS232_checkBox
            // 
            this.RS232_checkBox.AutoSize = true;
            this.RS232_checkBox.Checked = true;
            this.RS232_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.RS232_checkBox.Location = new System.Drawing.Point(268, 339);
            this.RS232_checkBox.Name = "RS232_checkBox";
            this.RS232_checkBox.Size = new System.Drawing.Size(57, 19);
            this.RS232_checkBox.TabIndex = 76;
            this.RS232_checkBox.Text = "RS232";
            this.RS232_checkBox.UseVisualStyleBackColor = true;
            // 
            // OUT_12V_checkBox
            // 
            this.OUT_12V_checkBox.AutoSize = true;
            this.OUT_12V_checkBox.Checked = true;
            this.OUT_12V_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OUT_12V_checkBox.Location = new System.Drawing.Point(268, 310);
            this.OUT_12V_checkBox.Name = "OUT_12V_checkBox";
            this.OUT_12V_checkBox.Size = new System.Drawing.Size(73, 19);
            this.OUT_12V_checkBox.TabIndex = 75;
            this.OUT_12V_checkBox.Text = "OUT_12V";
            this.OUT_12V_checkBox.UseVisualStyleBackColor = true;
            // 
            // OUT4_checkBox
            // 
            this.OUT4_checkBox.AutoSize = true;
            this.OUT4_checkBox.Checked = true;
            this.OUT4_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OUT4_checkBox.Location = new System.Drawing.Point(268, 281);
            this.OUT4_checkBox.Name = "OUT4_checkBox";
            this.OUT4_checkBox.Size = new System.Drawing.Size(55, 19);
            this.OUT4_checkBox.TabIndex = 74;
            this.OUT4_checkBox.Text = "OUT4";
            this.OUT4_checkBox.UseVisualStyleBackColor = true;
            // 
            // OUT3_checkBox
            // 
            this.OUT3_checkBox.AutoSize = true;
            this.OUT3_checkBox.Checked = true;
            this.OUT3_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OUT3_checkBox.Location = new System.Drawing.Point(268, 252);
            this.OUT3_checkBox.Name = "OUT3_checkBox";
            this.OUT3_checkBox.Size = new System.Drawing.Size(55, 19);
            this.OUT3_checkBox.TabIndex = 73;
            this.OUT3_checkBox.Text = "OUT3";
            this.OUT3_checkBox.UseVisualStyleBackColor = true;
            // 
            // OUT2_checkBox
            // 
            this.OUT2_checkBox.AutoSize = true;
            this.OUT2_checkBox.Checked = true;
            this.OUT2_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OUT2_checkBox.Location = new System.Drawing.Point(268, 223);
            this.OUT2_checkBox.Name = "OUT2_checkBox";
            this.OUT2_checkBox.Size = new System.Drawing.Size(55, 19);
            this.OUT2_checkBox.TabIndex = 72;
            this.OUT2_checkBox.Text = "OUT2";
            this.OUT2_checkBox.UseVisualStyleBackColor = true;
            // 
            // OUT1_checkBox
            // 
            this.OUT1_checkBox.AutoSize = true;
            this.OUT1_checkBox.Checked = true;
            this.OUT1_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.OUT1_checkBox.Location = new System.Drawing.Point(268, 194);
            this.OUT1_checkBox.Name = "OUT1_checkBox";
            this.OUT1_checkBox.Size = new System.Drawing.Size(55, 19);
            this.OUT1_checkBox.TabIndex = 71;
            this.OUT1_checkBox.Text = "OUT1";
            this.OUT1_checkBox.UseVisualStyleBackColor = true;
            // 
            // In8_checkBox
            // 
            this.In8_checkBox.AutoSize = true;
            this.In8_checkBox.Checked = true;
            this.In8_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.In8_checkBox.Location = new System.Drawing.Point(137, 368);
            this.In8_checkBox.Name = "In8_checkBox";
            this.In8_checkBox.Size = new System.Drawing.Size(44, 19);
            this.In8_checkBox.TabIndex = 70;
            this.In8_checkBox.Text = "IN8";
            this.In8_checkBox.UseVisualStyleBackColor = true;
            // 
            // In7_checkBox
            // 
            this.In7_checkBox.AutoSize = true;
            this.In7_checkBox.Checked = true;
            this.In7_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.In7_checkBox.Location = new System.Drawing.Point(137, 339);
            this.In7_checkBox.Name = "In7_checkBox";
            this.In7_checkBox.Size = new System.Drawing.Size(44, 19);
            this.In7_checkBox.TabIndex = 69;
            this.In7_checkBox.Text = "IN7";
            this.In7_checkBox.UseVisualStyleBackColor = true;
            // 
            // In6_checkBox
            // 
            this.In6_checkBox.AutoSize = true;
            this.In6_checkBox.Checked = true;
            this.In6_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.In6_checkBox.Location = new System.Drawing.Point(137, 310);
            this.In6_checkBox.Name = "In6_checkBox";
            this.In6_checkBox.Size = new System.Drawing.Size(44, 19);
            this.In6_checkBox.TabIndex = 68;
            this.In6_checkBox.Text = "IN6";
            this.In6_checkBox.UseVisualStyleBackColor = true;
            // 
            // In5_checkBox
            // 
            this.In5_checkBox.AutoSize = true;
            this.In5_checkBox.Checked = true;
            this.In5_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.In5_checkBox.Location = new System.Drawing.Point(137, 281);
            this.In5_checkBox.Name = "In5_checkBox";
            this.In5_checkBox.Size = new System.Drawing.Size(44, 19);
            this.In5_checkBox.TabIndex = 67;
            this.In5_checkBox.Text = "IN5";
            this.In5_checkBox.UseVisualStyleBackColor = true;
            // 
            // IN4_checkbox
            // 
            this.IN4_checkbox.AutoSize = true;
            this.IN4_checkbox.Checked = true;
            this.IN4_checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.IN4_checkbox.Location = new System.Drawing.Point(137, 252);
            this.IN4_checkbox.Name = "IN4_checkbox";
            this.IN4_checkbox.Size = new System.Drawing.Size(44, 19);
            this.IN4_checkbox.TabIndex = 66;
            this.IN4_checkbox.Text = "IN4";
            this.IN4_checkbox.UseVisualStyleBackColor = true;
            // 
            // In3_checkbox
            // 
            this.In3_checkbox.AutoSize = true;
            this.In3_checkbox.Checked = true;
            this.In3_checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.In3_checkbox.Location = new System.Drawing.Point(137, 223);
            this.In3_checkbox.Name = "In3_checkbox";
            this.In3_checkbox.Size = new System.Drawing.Size(44, 19);
            this.In3_checkbox.TabIndex = 65;
            this.In3_checkbox.Text = "IN3";
            this.In3_checkbox.UseVisualStyleBackColor = true;
            // 
            // In2_checkbox
            // 
            this.In2_checkbox.AutoSize = true;
            this.In2_checkbox.Checked = true;
            this.In2_checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.In2_checkbox.Location = new System.Drawing.Point(137, 194);
            this.In2_checkbox.Name = "In2_checkbox";
            this.In2_checkbox.Size = new System.Drawing.Size(44, 19);
            this.In2_checkbox.TabIndex = 64;
            this.In2_checkbox.Text = "IN2";
            this.In2_checkbox.UseVisualStyleBackColor = true;
            // 
            // In1_motion_checkbox
            // 
            this.In1_motion_checkbox.AutoSize = true;
            this.In1_motion_checkbox.Checked = true;
            this.In1_motion_checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.In1_motion_checkbox.Location = new System.Drawing.Point(15, 368);
            this.In1_motion_checkbox.Name = "In1_motion_checkbox";
            this.In1_motion_checkbox.Size = new System.Drawing.Size(95, 19);
            this.In1_motion_checkbox.TabIndex = 63;
            this.In1_motion_checkbox.Text = "IN1_MOTION";
            this.In1_motion_checkbox.UseVisualStyleBackColor = true;
            // 
            // Adc5_checkbox
            // 
            this.Adc5_checkbox.AutoSize = true;
            this.Adc5_checkbox.Checked = true;
            this.Adc5_checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Adc5_checkbox.Location = new System.Drawing.Point(15, 339);
            this.Adc5_checkbox.Name = "Adc5_checkbox";
            this.Adc5_checkbox.Size = new System.Drawing.Size(56, 19);
            this.Adc5_checkbox.TabIndex = 62;
            this.Adc5_checkbox.Text = "ADC5";
            this.Adc5_checkbox.UseVisualStyleBackColor = true;
            // 
            // Adc4_checkbox
            // 
            this.Adc4_checkbox.AutoSize = true;
            this.Adc4_checkbox.Checked = true;
            this.Adc4_checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Adc4_checkbox.Location = new System.Drawing.Point(15, 310);
            this.Adc4_checkbox.Name = "Adc4_checkbox";
            this.Adc4_checkbox.Size = new System.Drawing.Size(56, 19);
            this.Adc4_checkbox.TabIndex = 61;
            this.Adc4_checkbox.Text = "ADC4";
            this.Adc4_checkbox.UseVisualStyleBackColor = true;
            // 
            // Adc3_checkbox
            // 
            this.Adc3_checkbox.AutoSize = true;
            this.Adc3_checkbox.Checked = true;
            this.Adc3_checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Adc3_checkbox.Location = new System.Drawing.Point(15, 281);
            this.Adc3_checkbox.Name = "Adc3_checkbox";
            this.Adc3_checkbox.Size = new System.Drawing.Size(56, 19);
            this.Adc3_checkbox.TabIndex = 60;
            this.Adc3_checkbox.Text = "ADC3";
            this.Adc3_checkbox.UseVisualStyleBackColor = true;
            // 
            // Adc2_checkbox
            // 
            this.Adc2_checkbox.AutoSize = true;
            this.Adc2_checkbox.Checked = true;
            this.Adc2_checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Adc2_checkbox.Location = new System.Drawing.Point(15, 252);
            this.Adc2_checkbox.Name = "Adc2_checkbox";
            this.Adc2_checkbox.Size = new System.Drawing.Size(56, 19);
            this.Adc2_checkbox.TabIndex = 59;
            this.Adc2_checkbox.Text = "ADC2";
            this.Adc2_checkbox.UseVisualStyleBackColor = true;
            // 
            // Can2_checkbox
            // 
            this.Can2_checkbox.AutoSize = true;
            this.Can2_checkbox.Checked = true;
            this.Can2_checkbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Can2_checkbox.Location = new System.Drawing.Point(15, 223);
            this.Can2_checkbox.Name = "Can2_checkbox";
            this.Can2_checkbox.Size = new System.Drawing.Size(57, 19);
            this.Can2_checkbox.TabIndex = 58;
            this.Can2_checkbox.Text = "CAN2";
            this.Can2_checkbox.UseVisualStyleBackColor = true;
            // 
            // newDevice_phoneNumber_textBox
            // 
            this.newDevice_phoneNumber_textBox.Location = new System.Drawing.Point(105, 163);
            this.newDevice_phoneNumber_textBox.Name = "newDevice_phoneNumber_textBox";
            this.newDevice_phoneNumber_textBox.Size = new System.Drawing.Size(242, 23);
            this.newDevice_phoneNumber_textBox.TabIndex = 56;
            // 
            // newDevice_hwVersion_textBox
            // 
            this.newDevice_hwVersion_textBox.Location = new System.Drawing.Point(105, 134);
            this.newDevice_hwVersion_textBox.Name = "newDevice_hwVersion_textBox";
            this.newDevice_hwVersion_textBox.Size = new System.Drawing.Size(242, 23);
            this.newDevice_hwVersion_textBox.TabIndex = 54;
            // 
            // newDevice_IMEI_textBox
            // 
            this.newDevice_IMEI_textBox.Location = new System.Drawing.Point(105, 105);
            this.newDevice_IMEI_textBox.Name = "newDevice_IMEI_textBox";
            this.newDevice_IMEI_textBox.Size = new System.Drawing.Size(242, 23);
            this.newDevice_IMEI_textBox.TabIndex = 53;
            // 
            // newDevice_AssemblyRev_textBox
            // 
            this.newDevice_AssemblyRev_textBox.Location = new System.Drawing.Point(105, 76);
            this.newDevice_AssemblyRev_textBox.Name = "newDevice_AssemblyRev_textBox";
            this.newDevice_AssemblyRev_textBox.Size = new System.Drawing.Size(242, 23);
            this.newDevice_AssemblyRev_textBox.TabIndex = 52;
            // 
            // newDevice_RelayId_textBox
            // 
            this.newDevice_RelayId_textBox.Location = new System.Drawing.Point(105, 47);
            this.newDevice_RelayId_textBox.Name = "newDevice_RelayId_textBox";
            this.newDevice_RelayId_textBox.Size = new System.Drawing.Size(242, 23);
            this.newDevice_RelayId_textBox.TabIndex = 51;
            // 
            // newdevice_deviceName_textBox
            // 
            this.newdevice_deviceName_textBox.Location = new System.Drawing.Point(105, 18);
            this.newdevice_deviceName_textBox.Name = "newdevice_deviceName_textBox";
            this.newdevice_deviceName_textBox.Size = new System.Drawing.Size(242, 23);
            this.newdevice_deviceName_textBox.TabIndex = 44;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 166);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 15);
            this.label10.TabIndex = 50;
            this.label10.Text = "Sim number";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 137);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 15);
            this.label8.TabIndex = 48;
            this.label8.Text = "HW version";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 15);
            this.label7.TabIndex = 47;
            this.label7.Text = "IMEI";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 15);
            this.label6.TabIndex = 46;
            this.label6.Text = "Assembly Rev.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 50);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 15);
            this.label5.TabIndex = 45;
            this.label5.Text = "Relay ID";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 15);
            this.label4.TabIndex = 44;
            this.label4.Text = "Device name";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1584, 24);
            this.menuStrip1.TabIndex = 45;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // configsToolStripMenuItem
            // 
            this.configsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenConfigFile_toolStripMenuItem,
            this.openLogsFolderToolStripMenuItem,
            this.addNewDeviceToolStripMenuItem});
            this.configsToolStripMenuItem.Name = "configsToolStripMenuItem";
            this.configsToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.configsToolStripMenuItem.Text = "Configs";
            // 
            // OpenConfigFile_toolStripMenuItem
            // 
            this.OpenConfigFile_toolStripMenuItem.Name = "OpenConfigFile_toolStripMenuItem";
            this.OpenConfigFile_toolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.OpenConfigFile_toolStripMenuItem.Text = "Open Config Folder";
            this.OpenConfigFile_toolStripMenuItem.Click += new System.EventHandler(this.OpenConfigFile_toolStripMenuItem_Click);
            // 
            // openLogsFolderToolStripMenuItem
            // 
            this.openLogsFolderToolStripMenuItem.Name = "openLogsFolderToolStripMenuItem";
            this.openLogsFolderToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.openLogsFolderToolStripMenuItem.Text = "Open Logs Folder";
            this.openLogsFolderToolStripMenuItem.Click += new System.EventHandler(this.OpenLogsFolder_toolStripMenuItem_Click);
            // 
            // addNewDeviceToolStripMenuItem
            // 
            this.addNewDeviceToolStripMenuItem.Name = "addNewDeviceToolStripMenuItem";
            this.addNewDeviceToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.addNewDeviceToolStripMenuItem.Text = "Add new device";
            this.addNewDeviceToolStripMenuItem.Click += new System.EventHandler(this.AddNewDeviceToolStripMenuItem_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1428, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 15);
            this.label9.TabIndex = 14;
            this.label9.Text = "Modbus Port:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(1285, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 15);
            this.label11.TabIndex = 46;
            this.label11.Text = "Device Port:\r\n";
            // 
            // deviceComPortName
            // 
            this.deviceComPortName.AutoSize = true;
            this.deviceComPortName.Location = new System.Drawing.Point(1351, 32);
            this.deviceComPortName.Name = "deviceComPortName";
            this.deviceComPortName.Size = new System.Drawing.Size(75, 15);
            this.deviceComPortName.TabIndex = 47;
            this.deviceComPortName.Text = "NOT_FOUND";
            // 
            // modbusCommPortName
            // 
            this.modbusCommPortName.AutoSize = true;
            this.modbusCommPortName.Location = new System.Drawing.Point(1502, 32);
            this.modbusCommPortName.Name = "modbusCommPortName";
            this.modbusCommPortName.Size = new System.Drawing.Size(75, 15);
            this.modbusCommPortName.TabIndex = 48;
            this.modbusCommPortName.Text = "NOT_FOUND";
            // 
            // psuPortName
            // 
            this.psuPortName.AutoSize = true;
            this.psuPortName.Location = new System.Drawing.Point(1204, 32);
            this.psuPortName.Name = "psuPortName";
            this.psuPortName.Size = new System.Drawing.Size(75, 15);
            this.psuPortName.TabIndex = 50;
            this.psuPortName.Text = "NOT_FOUND";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1154, 32);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 15);
            this.label13.TabIndex = 49;
            this.label13.Text = "PSU Port:\r\n";
            // 
            // auxPsuPortName
            // 
            this.auxPsuPortName.AutoSize = true;
            this.auxPsuPortName.Location = new System.Drawing.Point(1073, 32);
            this.auxPsuPortName.Name = "auxPsuPortName";
            this.auxPsuPortName.Size = new System.Drawing.Size(75, 15);
            this.auxPsuPortName.TabIndex = 52;
            this.auxPsuPortName.Text = "NOT_FOUND";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(996, 32);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 15);
            this.label15.TabIndex = 51;
            this.label15.Text = "AUX PSU Port:\r\n";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1584, 861);
            this.Controls.Add(this.auxPsuPortName);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.psuPortName);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.modbusCommPortName);
            this.Controls.Add(this.deviceComPortName);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Peripheral_results_groupBox);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.Hardware_result_groupBox);
            this.Controls.Add(this.Functions_Test_Results_groupBox);
            this.Controls.Add(this.New_device_groupBox);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "AGNE - Testing Bench";
            this.Closed += new System.EventHandler(this.Form1_Close);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.Hardware_result_groupBox.ResumeLayout(false);
            this.Hardware_result_groupBox.PerformLayout();
            this.Peripheral_results_groupBox.ResumeLayout(false);
            this.Peripheral_results_groupBox.PerformLayout();
            this.Functions_Test_Results_groupBox.ResumeLayout(false);
            this.Functions_Test_Results_groupBox.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.New_device_groupBox.ResumeLayout(false);
            this.New_device_groupBox.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.CheckBox device1_checkbox;
        public System.Windows.Forms.CheckBox device5_checkBox;
        public System.Windows.Forms.CheckBox device10_checkBox;
        public System.Windows.Forms.CheckBox device9_checkBox;
        public System.Windows.Forms.CheckBox device8_checkBox;
        public System.Windows.Forms.CheckBox device7_checkBox;
        public System.Windows.Forms.CheckBox device4_checkBox;
        public System.Windows.Forms.CheckBox device3_checkBox;
        public System.Windows.Forms.CheckBox device2_checkbox;
        public System.Windows.Forms.CheckBox device36_checkBox;
        public System.Windows.Forms.CheckBox device35_checkBox;
        public System.Windows.Forms.CheckBox device34_checkBox;
        public System.Windows.Forms.CheckBox device33_checkBox;
        public System.Windows.Forms.CheckBox device32_checkBox;
        public System.Windows.Forms.CheckBox device31_checkBox;
        public System.Windows.Forms.CheckBox device30_checkBox;
        public System.Windows.Forms.CheckBox device29_checkBox;
        public System.Windows.Forms.CheckBox device24_checkBox;
        public System.Windows.Forms.CheckBox device23_checkBox;
        public System.Windows.Forms.CheckBox device28_checkBox;
        public System.Windows.Forms.CheckBox device27_checkBox;
        public System.Windows.Forms.CheckBox device26_checkBox;
        public System.Windows.Forms.CheckBox device25_checkBox;
        public System.Windows.Forms.CheckBox device22_checkBox;
        public System.Windows.Forms.CheckBox device21_checkBox;
        public System.Windows.Forms.CheckBox device20_checkBox;
        public System.Windows.Forms.CheckBox device19_checkBox;
        public System.Windows.Forms.CheckBox device18_checkBox;
        public System.Windows.Forms.CheckBox device17_checkBox;
        public System.Windows.Forms.CheckBox device16_checkBox;
        public System.Windows.Forms.CheckBox device15_checkBox;
        public System.Windows.Forms.CheckBox device14_checkBox;
        public System.Windows.Forms.CheckBox device13_checkBox;
        public System.Windows.Forms.CheckBox device12_checkBox;
        public System.Windows.Forms.CheckBox device11_checkBox;
        public System.Windows.Forms.CheckBox device6_checkBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button Stop_Test_Button;
        private System.Windows.Forms.Button Start_Test_Button;
        public System.Windows.Forms.ComboBox Firmware_ComboBox;
        private System.Windows.Forms.GroupBox Hardware_result_groupBox;
        private System.Windows.Forms.Label Tachograph_label;
        private System.Windows.Forms.TextBox Tachograph_textBox;
        private System.Windows.Forms.GroupBox Peripheral_results_groupBox;
        private System.Windows.Forms.TextBox Ibutton_textBox;
        private System.Windows.Forms.Label Ibutton_label;
        private System.Windows.Forms.TextBox Temp_sensor_textBox;
        private System.Windows.Forms.Label Temp_sensor_label;
        private System.Windows.Forms.TextBox BLE_extender_textBox;
        private System.Windows.Forms.Label BLE_Extender_label;
        private System.Windows.Forms.GroupBox Functions_Test_Results_groupBox;
        private System.Windows.Forms.GroupBox groupBox7;
        public System.Windows.Forms.TextBox Info_textBox;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox CAN1_textBox;
        public System.Windows.Forms.TextBox OUT_12V_textBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label RS485_label;
        public System.Windows.Forms.TextBox CAN2_textBox;
        public System.Windows.Forms.TextBox RS485_textBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label RS232_label;
        public System.Windows.Forms.TextBox ADC2_textBox;
        public System.Windows.Forms.TextBox RS232_textBox;
        private System.Windows.Forms.Label ADC2_label;
        private System.Windows.Forms.Label OUT4_label;
        private System.Windows.Forms.TextBox ADC3_textBox;
        private System.Windows.Forms.TextBox OUT4_textBox;
        private System.Windows.Forms.Label ADC3_label;
        private System.Windows.Forms.Label OUT3_label;
        private System.Windows.Forms.TextBox ADC4_textBox;
        private System.Windows.Forms.TextBox OUT3_textBox;
        private System.Windows.Forms.Label ADC4_label;
        private System.Windows.Forms.Label OUT2_label;
        private System.Windows.Forms.TextBox ADC5_textBox;
        private System.Windows.Forms.TextBox OUT2_textBox;
        private System.Windows.Forms.Label ADC5_label;
        private System.Windows.Forms.Label OUT1_label;
        private System.Windows.Forms.TextBox IN1_Motion_textBox;
        private System.Windows.Forms.TextBox OUT1_textBox;
        private System.Windows.Forms.Label IN1_label;
        private System.Windows.Forms.Label IN8_label;
        private System.Windows.Forms.TextBox IN2_textBox;
        private System.Windows.Forms.TextBox IN8_textBox;
        private System.Windows.Forms.Label IN2_label;
        private System.Windows.Forms.Label IN7_label;
        private System.Windows.Forms.TextBox IN3_textBox;
        private System.Windows.Forms.TextBox IN7_textBox;
        private System.Windows.Forms.Label IN3_label;
        private System.Windows.Forms.Label IN6_label;
        private System.Windows.Forms.TextBox IN4_textBox;
        private System.Windows.Forms.TextBox IN6_textBox;
        private System.Windows.Forms.Label IN4_label;
        private System.Windows.Forms.Label IN5_label;
        private System.Windows.Forms.TextBox IN5_textBox;
        private System.Windows.Forms.GroupBox New_device_groupBox;
        private System.Windows.Forms.TextBox newDevice_phoneNumber_textBox;
        private System.Windows.Forms.TextBox newDevice_hwVersion_textBox;
        private System.Windows.Forms.TextBox newDevice_IMEI_textBox;
        private System.Windows.Forms.TextBox newDevice_AssemblyRev_textBox;
        private System.Windows.Forms.TextBox newDevice_RelayId_textBox;
        private System.Windows.Forms.TextBox newdevice_deviceName_textBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox RS485_checkBox;
        private System.Windows.Forms.CheckBox RS232_checkBox;
        private System.Windows.Forms.CheckBox OUT_12V_checkBox;
        private System.Windows.Forms.CheckBox OUT4_checkBox;
        private System.Windows.Forms.CheckBox OUT3_checkBox;
        private System.Windows.Forms.CheckBox OUT2_checkBox;
        private System.Windows.Forms.CheckBox OUT1_checkBox;
        private System.Windows.Forms.CheckBox In8_checkBox;
        private System.Windows.Forms.CheckBox In7_checkBox;
        private System.Windows.Forms.CheckBox In6_checkBox;
        private System.Windows.Forms.CheckBox In5_checkBox;
        private System.Windows.Forms.CheckBox IN4_checkbox;
        private System.Windows.Forms.CheckBox In3_checkbox;
        private System.Windows.Forms.CheckBox In2_checkbox;
        private System.Windows.Forms.CheckBox In1_motion_checkbox;
        private System.Windows.Forms.CheckBox Adc5_checkbox;
        private System.Windows.Forms.CheckBox Adc4_checkbox;
        private System.Windows.Forms.CheckBox Adc3_checkbox;
        private System.Windows.Forms.CheckBox Adc2_checkbox;
        private System.Windows.Forms.CheckBox Can2_checkbox;
        private System.Windows.Forms.CheckBox Ble_Extender_checkBox;
        private System.Windows.Forms.CheckBox Ibutton_checkBox;
        private System.Windows.Forms.CheckBox Temp_sensor_checkBox;
        private System.Windows.Forms.CheckBox Tachograph_checkBox;
        private System.Windows.Forms.Button New_device_cancel_button;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem configsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OpenConfigFile_toolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openLogsFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewDeviceToolStripMenuItem;
        private System.Windows.Forms.Button newDevice_addDevice_button;
        private System.Windows.Forms.CheckBox newDevice_sdkdevice_checkBox;
        private System.Windows.Forms.CheckBox Can1_checkbox;
        public System.Windows.Forms.TextBox CurrentTestLogBox;
        private System.Windows.Forms.Label LogText;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label deviceComPortName;
        public System.Windows.Forms.Label modbusCommPortName;
        public System.Windows.Forms.Label psuPortName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.TextBox Script_textBox;
        public System.Windows.Forms.Label auxPsuPortName;
        private System.Windows.Forms.Label label15;
    }
}

