﻿using System.Threading;
using System.Threading.Tasks;
using Test_bench_AGNE.Models.Global;
using Test_bench_AGNE.Services.CommPort;
using Test_bench_AGNE.Services.Wialon.Models;

namespace Test_bench_AGNE.Services
{
    public partial class ModbusPort_Thread
    {
        public string commPortStatus = "";
        private Modbus _modbus = new ();

        public void Check_Comport_State(Form1 form1)
        {
            try
            {
                string comm_status = "";
                var task = Task.Run(() => { Check_Comport_Status(); });
                while (true)
                {
                    if (!string.IsNullOrEmpty(commPortStatus))
                    {
                        if (comm_status != commPortStatus || Form1.serialParser.modbusCommPort.PortName != form1.modbusCommPortName.Text)
                        {
                            form1.Update_ModbusCommPortName_Label(Form1.serialParser.modbusCommPort.PortName);
                            comm_status = commPortStatus;
                            form1.Update_Info_textbox(comm_status);

                            if (comm_status.Contains("Modbus Port Open - ") && form1.deviceComPortName.Text != "NOT_FOUND" && form1.psuPortName.Text != "NOT_FOUND")
                            {
                                form1.Update_StartTest_Button_Status(true);
                            }

                            if (comm_status.Contains("Modbus serial port not found"))
                            {
                                form1.Update_StartTest_Button_Status(false);
                            }

                        }
                    }

                    

                    Thread.Sleep(250);
                }
            }
            catch (ThreadInterruptedException ex)
            {
                Logging.Write_Exceptions(ex);
            }
        }

        public void Check_Comport_Status()
        {
            try
            {
                while (true)
                {
                    if (Form1.serialParser.modbusCommPort != null)
                    {
                        if (!GlobalVariables.modbusCommPortOpenAndWOrking)
                        {
                            Find_And_Open_Port();
                        }
                        else
                        {
                            if (!Form1.serialParser.modbusCommPort.IsOpen)
                            {
                                GlobalVariables.modbusCommPortOpenAndWOrking = false;
                                Find_And_Open_Port();
                            }
                        }
                    }
                    Thread.Sleep(1500);
                }
            }
            catch (ThreadInterruptedException ex)
            {
                Logging.Write_Exceptions(ex);
            }
        }

        private void Find_And_Open_Port()
        {
            if (!Main_CommPort_Controls.Get_Com_Port(ref Form1.serialParser.modbusCommPort, GlobalVariables.RS485ModbusComPortName))
            {
                commPortStatus = "Modbus serial port not found";
                Form1.serialParser.modbusCommPort.PortName = "NOT_FOUND";
            }
            else
            {
                if (Main_CommPort_Controls.Open_Port(ref Form1.serialParser.modbusCommPort, 9600, false))
                {
                    GlobalVariables.modbusCommPortOpenAndWOrking = true;
                    Form1.serialParser._relayControl = new Relay_Control(_modbus, Form1.serialParser.modbusCommPort);
                    commPortStatus = "Modbus Port Open - " + Form1.serialParser.modbusCommPort.PortName;
                }
            }
        }
    }
}
