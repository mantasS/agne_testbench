﻿using System;

namespace Test_bench_AGNE.Services
{
    public partial class Modbus
    {
        private static UInt16 ModRTU_CRC(byte[] buf)
        {
            int len = buf.Length;
            UInt16 crc = 0xFFFF;

            for (int pos = 0; pos < len; pos++)
            {
                crc ^= (UInt16)buf[pos];

                for (int i = 8; i != 0; i--)
                {
                    if ((crc & 0x0001) != 0)
                    {
                        crc >>= 1;
                        crc ^= 0xA001;
                    }
                    else
                    {
                        crc >>= 1;
                    }
                }
            }

            return crc;
        }

        public static byte[] CRC(byte[] buffer)
        {
            UInt16 crc = ModRTU_CRC(buffer);

            byte crc_low = (byte)(crc & 0xFF);
            byte crc_high = (byte)((crc >> 8) & 0xFF);

            byte[] res_buffer = {crc_low, crc_high};

            return res_buffer;
        }
    }
}
