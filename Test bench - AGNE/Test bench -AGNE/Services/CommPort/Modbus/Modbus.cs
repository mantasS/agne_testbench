﻿using System;
using System.IO.Ports;
using System.Text;
using System.Threading;
using Test_bench_AGNE.Services.Wialon.Models;

namespace Test_bench_AGNE.Services
{
    public partial class Modbus
    {
        readonly byte modbusCommand = 0x06;

        private bool Send(SerialPort port, byte slave_ID, byte function, Int16 addrress, byte data_command, byte delay)
        {
            for (int i = 0; i < 2; i++)
            {
                byte address_low = (byte)(addrress & 0xFF);
                byte address_high = (byte)((addrress >> 8) & 0xFF);
                byte[] temp_buffer = { slave_ID, function, address_high, address_low, data_command, delay };
                byte[] crc = Modbus.CRC(temp_buffer);

                byte[] send_buffer = { slave_ID, function, address_high, address_low, data_command, delay, crc[0], crc[1] };

                port.Write(send_buffer, 0, send_buffer.Length);

                if(Check_response(port, send_buffer))
                {
                    return true;
                }
                Thread.Sleep(50);
            }

            return false;
        }

        private bool Check_response(SerialPort port, byte[] send_buffer)
        {
            byte[] response_buffer = new byte[8];
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    if (port.BytesToRead > 5)
                    {
                        var len = port.BytesToRead;
                        for (byte j = 0; j < len; j++)
                        {
                            response_buffer[j] = (byte)port.ReadByte();
                        }
                        return Compare_Two_Buffers(send_buffer, response_buffer);
                    }
                    Thread.Sleep(50);
                }
                
            }
            catch
            {
                return false;
            }

            return false;
        }

        private bool Compare_Two_Buffers(byte[] buffer_one, byte[] buffer_two)
        {
            var len1 = buffer_one.Length;
            var len2 = buffer_two.Length;

            if(len1 != len2)
            {
                return false;
            }

            for (int i = 0; i < len1; i++)
            {
                if (buffer_one[i] != buffer_two[i])
                {
                    return false;
                }
            }
            return true;
        }

        public bool Turn_On_All_Relays(SerialPort port, byte slave_ID)
        {
            return Send(port, slave_ID, modbusCommand, 0x0000, Modbus_Command.turnOnAllRelays, 0x00);
        }

        public bool Turn_On_All_Relays(SerialPort port, byte slave_ID, byte delay)
        {
            return Send(port, slave_ID, modbusCommand, 0x0000, Modbus_Command.turnOnAllRelays, delay);
        }

        public bool Turn_Off_All_Relays(SerialPort port, byte slave_ID)
        {
            return Send(port, slave_ID, modbusCommand, 0x0000, Modbus_Command.turnOffAllRelays, 0x00);
        }

        public bool Turn_Off_All_Relays(SerialPort port, byte slave_ID, byte delay)
        {
            return Send(port, slave_ID, modbusCommand, 0x0000, Modbus_Command.turnOffAllRelays, delay);
        }

        public bool Turn_On_Relay(SerialPort port, byte slave_ID, byte relay_ID)
        {
            return Send(port, slave_ID, modbusCommand, relay_ID, Modbus_Command.turnOnRelay, 0x00);
        }

        public bool Turn_On_Relay(SerialPort port, byte slave_ID, byte relay_ID, byte delay)
        {
            return Send(port, slave_ID, modbusCommand, relay_ID, Modbus_Command.turnOnRelayForSetTime, delay);
        }

        public bool Turn_Off_Relay(SerialPort port, byte slave_ID, byte relay_ID)
        {
            return Send(port, slave_ID, modbusCommand, relay_ID, Modbus_Command.turnOffRelay, 0x00);
        }
    }
}
