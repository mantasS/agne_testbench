using System;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;
using Test_bench_AGNE.Services;
using Test_bench_AGNE.Services.Wialon;
using Test_bench_AGNE.Services.CommPort;
using Test_bench_AGNE.Models.Device;

namespace Test_bench_AGNE
{
    public class Comm_Port_Parser
    {

        public SerialPort modbusCommPort = new ();
        public SerialPort deviceCommPort = new ();
        public SerialPort psuCommPort = new ();
        public SerialPort auxiliaryPsuCommPort = new ();

        private Modbus _modbus = new ();
        private Wialon wialon;
        public Relay_Control _relayControl;
        public string deviceResponse;
        public string deviceStartString;
        public bool deviceStartFlag = false;
        public string PSUResponse;
        public string TestStartMessage;
        public Device_Parameters _deviceParameters;

        public Comm_Port_Parser()
        {
            wialon = new Wialon();
        }


        public async void SerialPort_Data_Parser(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort serialPort = sender as SerialPort;
            string command_string;

            Thread.Sleep(30);

            if (serialPort == deviceCommPort)
            {
                try
                {
                    command_string = serialPort.ReadExisting();

                    if (!command_string.Contains("\n"))
                    {
                        Thread.Sleep(50);
                        command_string = command_string + serialPort.ReadExisting();
                    }

                    if (!string.IsNullOrEmpty(command_string))
                    {
                        if (deviceStartString != null)
                        {
                            if (command_string.Contains(deviceStartString))
                            {
                                deviceStartFlag = true;
                            }
                        }

                        if (TestStartMessage != null)
                        {
                            if (command_string.Contains(TestStartMessage))
                            {
                                deviceCommPort.Write(TestStartMessage + ":test");
                            }
                        }

                        Logging.Logs_WriteLines(command_string);
                    }
                }
                catch (Exception ex)
                {
                    Logging.Write_Exceptions(ex);
                    return;
                }

                try
                {
                    var commands = command_string.Split("\n");

                    foreach (var comm in commands)
                    {
                        if (!comm.Contains("$"))
                        {
                            if(command_string.Length > 3)
                            {
                                deviceResponse = command_string;
                            }
                            continue;
                        }

                        Logging.Logs_WriteLine("Got command: " + comm.Trim('\r', '\n'));
                        var command = comm.Split(":")[0];
                        string action = null;
                        if(comm.Split(":").Length > 1 )
                        {
                            action = comm.Split(":")[1].Trim('\r', '\n');
                        }

                        bool result = false;

                        switch (command)
                        {
                            case "$RS485":
                                {
                                    result = RS485_Action(action);
                                    break;
                                }
                            case "$RS232":
                                {
                                    result = RS232_Action(action);
                                    break;
                                }
                            case "$CAN":
                                {
                                    result = Can_Action(action);
                                    break;
                                }
                            case "$OneWire":
                                {
                                    result = OneWire_Action(action);
                                    break;
                                }
                            case "$ADC5":
                                {
                                    result = Adc_Action(action);
                                    break;
                                }
                            case "$ADC4":
                                {
                                    result = Adc_Action(action);
                                    break;
                                }
                            case "$ADC3":
                                {
                                    result = Adc_Action(action);
                                    break;
                                }
                            case "$IN2":
                                {
                                    result = Input_Action(action);
                                    break;
                                }
                            case "$IN3":
                                {
                                    result = Input_Action(action);
                                    break;
                                }
                            case "$IN4":
                                {
                                    result = Input_Action(action);
                                    break;
                                }
                            case "$IN5":
                                {
                                    result = Input_Action(action);
                                    break;
                                }
                            case "$OUT1":
                                {
                                    result = Output_Action(action);
                                    break;
                                }
                            case "$OUT2":
                                {
                                    result = Output_Action(action);
                                    break;
                                }
                            case "$OUT3":
                                {
                                    result = Output_Action(action);
                                    break;
                                }
                            case "$OUT4":
                                {
                                    result = Output_Action(action);
                                    break;
                                }
                            case "$PowerSupply":
                                {
                                    result = Power_Supply_Action(action);
                                    break;
                                }
                            case "$SecondaryPowerSupply":
                                {
                                    result = Secondary_Power_Supply_Action(action);
                                    break;
                                }
                            case "$Relay":
                                {
                                    result = Relay_Control(action);
                                    break;
                                }
                            case "$Commands":
                                {
                                    result = Testbench_commands(action);
                                    break;
                                }
                            case "$Request":
                                {
                                    result = Testbench_requests(action, serialPort);
                                    break;
                                }
                            case "$Wialon":
                                {
                                    result = await Wialon_requests(action, serialPort);
                                    break;
                                }
                            default:
                                {
                                    deviceResponse = command_string;
                                    break;
                                }
                        }

                        if (result)
                        {
                            send(comm.Trim('\r', '\n') + ",OK", serialPort);
                        }
                    }

                }
                catch (TimeoutException ex)
                {
                    Logging.Write_Exceptions(ex);
                }
                catch (IndexOutOfRangeException ex)
                {
                    Logging.Write_Exceptions(ex);
                    Logging.Write_Exceptions(command_string);
                }
                catch (Exception ex)
                {
                    Logging.Write_Exceptions(ex);
                }
            }
            else if (serialPort == psuCommPort)
            {
                try
                {
                    command_string = serialPort.ReadExisting();
                    Logging.Logs_WriteLine(command_string);
                }
                catch (Exception ex)
                {
                    Logging.Write_Exceptions(ex);
                    return;
                }

                PSUResponse = serialPort.ReadExisting();
                deviceCommPort.Write(PSUResponse);
            }
        }


        
        
        private bool RS485_Action(string action)
        {
            bool result = false;
            switch (action)
            {
                case "LLS":
                    {
                        result = _relayControl._mainRelay.Connect_LLSTo_RS485();
                        break;
                    }
            }

            return result;
        }
        
        private bool RS232_Action(string action)
        {
            bool result = false;

            switch (action.Split(",")[0])
            {
                case "LLS":
                    {
                        if (byte.TryParse(action.Split(",")[1], out byte delay))
                        {
                            result = _relayControl._mainRelay.Connect_LLS_To_RS232(delay);

                        }
                        else
                        {
                            result = _relayControl._mainRelay.Connect_LLS_To_RS232();

                        }
                        break;
                    }
                case "Comm_With_Test_Bench":
                    {
                        result = _relayControl._mainRelay.Connect_RS232_To_COMPort();
                        break;
                    }
            }
            return result;
        }
        
        private bool Can_Action(string action)
        {
            bool result = false;
            switch (action.Split(",")[0])
            {
                case "CAN1":
                    {
                        switch (action.Split(",")[1])
                        {
                            case "CAN2":
                                {
                                    result = _relayControl._mainRelay.Connect_CAN2_To_CAN1();
                                    break;
                                }
                            case "ADC5":
                                {
                                    result = _relayControl._mainRelay.Connect_CAN1_As_ADC5();
                                    break;
                                }
                            case "OUT4":
                                {
                                    result = _relayControl._mainRelay.Connect_CAN1_As_Out4();
                                    break;
                                }
                            case "OUT1":
                                {
                                    result = _relayControl._mainRelay.Connect_CAN1_As_Out1();
                                    break;
                                }
                            case "TACHO":
                                {
                                    break;
                                }
                        }
                        break;
                    }
                case "CAN2":
                    {
                        switch (action.Split(",")[1])
                        {
                            case "CAN1":
                                {
                                    result = _relayControl._mainRelay.Connect_CAN2_To_CAN1();
                                    break;
                                }
                            case "OUT3":
                                {
                                    result = _relayControl._mainRelay.Connect_CAN2_As_Out3();
                                    break;
                                }
                            case "TACHO":
                                {
                                    result = _relayControl._mainRelay.Connect_CAN2_To_Tacho();
                                    break;
                                }
                        }
                        break;
                    }
            }

            
            return result;
        }
        
        private bool OneWire_Action(string action)
        {
            bool result = false;
            switch (action)
            {
                case "Ibutton":
                    {
                        result = _relayControl._mainRelay.Connect_One_Wire_To_Ibutton();
                        break;
                    }
                case "D8":
                    {
                        result = _relayControl._mainRelay.Connect_One_Wire_To_D8();
                        break;
                    }
                case "BleExt":
                    {
                        result = _relayControl._mainRelay.Connect_One_Wire_To_BleExtender();
                        break;
                    }
                case "Temp":
                    {
                        result = _relayControl._mainRelay.Connect_One_Wire_To_Temp_Sensor();
                        break;
                    }
                case "ADC":
                    {
                        result = _relayControl._mainRelay.Connect_One_Wire_To_ADC_Inputs();
                        break;
                    }
                case "Reset":
                    {
                        result = _relayControl._mainRelay.Reset_One_Wire_Relays();
                        break;
                    }
            }
            return result;
        }
        
        private static bool Adc_Action(string action)
        {
            bool result = false;
            if(int.TryParse(action, out int actionInInt))
            {
                switch (actionInInt)
                {
                    case 0:
                        {
                            break;
                        }
                    case 1:
                        {
                            break;
                        }
                }
            }
            
            return result;
        }
        
        private static bool Input_Action(string action)
        {
            bool result = false;
            if(int.TryParse(action, out int actionInInt))
            {
                switch (actionInInt)
                {
                    case 0:
                        {
                            break;
                        }
                    case 1:
                        {
                            break;
                        }
                }
            }
            
            return result;
        }
        
        private static bool Output_Action(string action)
        {
            bool result = false;
            int.TryParse(action, out int actionInInt);
            switch (actionInInt)
            {
                case 0:
                    {
                        break;
                    }
                case 1:
                    {
                        break;
                    }
            }
            return result;
        }
        
        private bool Power_Supply_Action(string action)
        {
            bool result = false;

            switch (action.Split(",")[0])
            {
                case "TurnOn":
                    {
                        psuCommPort.Write(PowerSupply.Output_Command(true));
                        result = true;
                        break;
                    }
                case "TurnOff":
                    {
                        psuCommPort.Write(PowerSupply.Output_Command(false));
                        result = true;
                        break;
                    }
                case "SetVoltage":
                    {
                        if(float.TryParse(action.Split(",")[1], out float set_Value))
                        {
                            if (set_Value != 0)
                            {
                                psuCommPort.Write(PowerSupply.Set_Voltage_Command(set_Value));
                            }
                            result = true;
                        }
                        
                        break;
                    }
                case "SetCurrent":
                    {
                        if(float.TryParse(action.Split(",")[1], out float set_Value))
                        {
                            if (set_Value != 0)
                            {
                                psuCommPort.Write(PowerSupply.Set_Current_Command(set_Value));
                            }
                            result = true;
                        }
                        break;
                    }
                case "GetOutputVoltage":
                    {
                        psuCommPort.Write(PowerSupply.Get_Output_Voltage_Command());
                        break;
                    }
                case "GetOutputCurrent":
                    {
                        psuCommPort.Write(PowerSupply.Get_Output_Current_Command());
                        break;
                    }
                case "GetSetVoltage":
                    {
                        psuCommPort.Write(PowerSupply.Get_Set_Voltage_Command());
                        break;
                    }
                case "GetSetCurrent":
                    {
                        psuCommPort.Write(PowerSupply.Get_Set_Current_Command());
                        break;
                    }

            }
            return result;
        }
        
        private bool Secondary_Power_Supply_Action(string action)
        {
            bool result = false;
            switch (action.Split(",")[0])
            {
                case "Connect":
                    {
                        result = _relayControl._mainRelay.Connect_Secondary_Power_Supply();
                        break;
                    }
                case "Disconnect":
                    {
                        result = _relayControl._mainRelay.Disconnect_Secondary_Power_Supply();
                        break;
                    }
                case "TurnOn":
                    {
                        auxiliaryPsuCommPort.Write(AuxiliaryPowerSupply.Output_Command(true));
                        result = true;
                        break;
                    }
                case "TurnOff":
                    {
                        auxiliaryPsuCommPort.Write(AuxiliaryPowerSupply.Output_Command(false));
                        result = true;
                        break;
                    }
                case "SetVoltage":
                    {
                        if (float.TryParse(action.Split(",")[1], out float set_Value))
                        {
                            if (set_Value != 0)
                            {
                                auxiliaryPsuCommPort.Write(AuxiliaryPowerSupply.Set_Voltage_Command(set_Value));
                            }
                            result = true;
                        }

                        break;
                    }
                case "SetCurrent":
                    {
                        if (float.TryParse(action.Split(",")[1], out float set_Value))
                        {
                            if (set_Value != 0)
                            {
                                auxiliaryPsuCommPort.Write(AuxiliaryPowerSupply.Set_Current_Command(set_Value));
                            }
                            result = true;
                        }
                        break;
                    }
                case "GetOutputVoltage":
                    {
                        auxiliaryPsuCommPort.Write(AuxiliaryPowerSupply.Get_Output_Voltage_Command());
                        break;
                    }
                case "GetOutputCurrent":
                    {
                        auxiliaryPsuCommPort.Write(AuxiliaryPowerSupply.Get_Output_Current_Command());
                        break;
                    }
                case "GetSetVoltage":
                    {
                        auxiliaryPsuCommPort.Write(AuxiliaryPowerSupply.Get_Set_Voltage_Command());
                        break;
                    }
                case "GetSetCurrent":
                    {
                        auxiliaryPsuCommPort.Write(AuxiliaryPowerSupply.Get_Set_Current_Command());
                        break;
                    }
            }
            return result;
        }
        
        private bool Relay_Control(string action)
        {
            bool result = false;
            if(byte.TryParse(action.Split(",")[0], out byte slave_Id))
            {
                if(byte.TryParse(action.Split(",")[1], out byte relay_Id))
                {
                    if(byte.TryParse(action.Split(",")[2], out byte state))
                    {
                        if(byte.TryParse(action.Split(",")[3], out byte delay))
                        {
                            switch (state)
                            {
                                case 0:
                                    {
                                        result = _modbus.Turn_Off_Relay(modbusCommPort, slave_Id, relay_Id);
                                        break;
                                    }
                                case 1:
                                    {
                                        if (delay != 0)
                                        {
                                            result = _modbus.Turn_On_Relay(modbusCommPort, slave_Id, relay_Id, delay);
                                        }
                                        else
                                        {
                                            result = _modbus.Turn_On_Relay(modbusCommPort, slave_Id, relay_Id);
                                        }
                                        break;
                                    }
                            }
                        }
                    }
                }
            }
            
            return result;
        }
        
        private static bool Testbench_commands(string action)
        {
            bool result = false;
            switch (action)
            {
                case "SendSMS":
                    {

                        break;
                    }
            }
            return result;
        }
        
        private bool Testbench_requests(string action, SerialPort serialPort)
        {
            bool result = false;
            switch (action)
            {
                case "PhoneNumber":
                    {
                        send("$PhoneNumber:" + _deviceParameters.SIM_Number, serialPort);
                        break;
                    }
                case "IMEI":
                    {
                        send("$IMEI:" + _deviceParameters.IMEI, serialPort);
                        break;
                    }
                case "UTCTime":
                    {
                        send("UTCTime:" + DateTimeOffset.UtcNow.ToUnixTimeSeconds(), serialPort);
                        break;
                    }
            }
            return result;
        }

        private async Task<bool>  Wialon_requests(string action, SerialPort serialPort)
        {

            switch (action.Split(",")[0])
            {
                case "Get_Sensor_Data":
                    {
                        var sensor_Data = await wialon.Get_Sensor_Data(long.Parse(_deviceParameters.IMEI), action.Split(",")[1]);
                        send("$Wialon:Get_Sensor_Data," + action.Split(",")[1] + "," + sensor_Data[1] + "," + sensor_Data[0], serialPort);
                        break;
                    }
                case "Get_Sensor_Data_Extended":
                    {
                        var sensor_Data = await wialon.Get_Sensor_Data(long.Parse(_deviceParameters.IMEI), action.Split(",")[1]);
                        send("$Wialon:Get_Sensor_Data_Extended," + action.Split(",")[1] + "," + sensor_Data[1] + "," + sensor_Data[2] + "," + sensor_Data[3], serialPort);

                        /*if (int.Parse(sensor_Data[2]) > 200)
                        {
                            send("$Wialon:Get_Sensor_Data_Extended," + action.Split(",")[1] + "," + sensor_Data[1] + "," + sensor_Data[2] + "," + sensor_Data[3], serialPort);
                        }
                        else
                        {
                            send("$Wialon:Get_Sensor_Data_Extended," + action.Split(",")[1] + "," + sensor_Data[0] + "," + sensor_Data[1] + "," + sensor_Data[2] + "," + sensor_Data[3], serialPort);
                        }*/

                        break;
                    }
                case "Send_GPRS_Command":
                    {
                        return await wialon.Send_GPRS_Command_ByName(long.Parse(_deviceParameters.IMEI), action.Split(",")[1]);
                    }
            }
            return false;
        }

        public static void send(string data, SerialPort serialPort)
        {
            serialPort.Write(data);
            Logging.Logs_WriteLine("Response: " + data);
        }
    }
}
