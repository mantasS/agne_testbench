﻿using System.Threading;
using System.Threading.Tasks;
using Test_bench_AGNE.Models.Global;
using Test_bench_AGNE.Services.Wialon.Models;

namespace Test_bench_AGNE.Services.CommPort
{
    class RS232_Thread
    {
        public string commPortStatus = "";

        public void Check_Comport_State(Form1 form1)
        {
            try
            {
                string comm_status = "";
                var task = Task.Run(() => { Check_Comport_Status(); });
                while (true)
                {
                    if (!string.IsNullOrEmpty(commPortStatus))
                    {
                        if (comm_status != commPortStatus || Form1.serialParser.deviceCommPort.PortName != form1.deviceComPortName.Text)
                        {
                            form1.Update_DeviceCommPortName_Label(Form1.serialParser.deviceCommPort.PortName);
                            comm_status = commPortStatus;
                            form1.Update_Info_textbox(comm_status);

                            if (comm_status.Contains("Device Com Port Open - "))
                            {
                                form1.Update_StartTest_Button_Status(true);
                            }
                            else if (comm_status.Contains("Device communication serial port not found"))
                            {
                                form1.Update_StartTest_Button_Status(false);
                            }

                        }
                    }
                    
                    Thread.Sleep(250);
                }
            }
            catch (ThreadInterruptedException ex)
            {
                Logging.Write_Exceptions(ex);
            }
        }

        public void Check_Comport_Status()
        {

            try
            {
                while (true)
                {

                    if (Form1.serialParser.deviceCommPort != null)
                    {
                        if (!GlobalVariables.deviceCommPortOpenAndWOrking)
                        {
                            Find_And_Open_Port();
                        }
                        else
                        {
                            if (!Form1.serialParser.deviceCommPort.IsOpen)
                            {
                                GlobalVariables.deviceCommPortOpenAndWOrking = false;
                                Find_And_Open_Port();
                            }
                        }
                    }
                    Thread.Sleep(1500);
                }
            }
            catch (ThreadInterruptedException ex)
            {
                Logging.Write_Exceptions(ex);
            }
        }

        private void Find_And_Open_Port()
        {
            if (!Main_CommPort_Controls.Get_Com_Port(ref Form1.serialParser.deviceCommPort, GlobalVariables.deviceComPortName))
            {
                commPortStatus = "Device communication serial port not found";
                Form1.serialParser.deviceCommPort.PortName = "NOT_FOUND";
            }
            else
            {
                if (Main_CommPort_Controls.Open_Port(ref Form1.serialParser.deviceCommPort, 115200, true))
                {
                    Form1.serialParser.deviceCommPort.ReadTimeout = 1000;
                    commPortStatus = "Device Com Port Open - " + Form1.serialParser.deviceCommPort.PortName;
                    GlobalVariables.deviceCommPortOpenAndWOrking = true;
                }
            }
        }
    }
}
