﻿using System.Threading;
using System.Threading.Tasks;
using Test_bench_AGNE.Models.Global;

namespace Test_bench_AGNE.Services.CommPort
{
    public class PowerSupply
    {
        public string powerSupplyPortStatus = "";

        public static string Output_Command(bool state)
        {
            return "OUTPUT" + (state ? 1 : 0) + "\\n";
        }

        public static string Set_Voltage_Command(float voltage)
        {
            return "VSET1:" + voltage.ToString("0.00") + "\\n";
        }
        public static string Set_Current_Command(float current)
        {
            return "ISET1:" + current.ToString("0.00") + "\\n";
        }

        public static string Get_Output_Voltage_Command()
        {
            return "VOUT1?\\n";
        }

        public static string Get_Set_Voltage_Command()
        {
            return "VSET1?\\n";
        }

        public static string Get_Output_Current_Command()
        {
            return "IOUT1?\\n";
        }

        public static string Get_Set_Current_Command()
        {
            return "ISET1?\\n";
        }

        public void Check_For_PSU_Comport(Form1 form1)
        {
            try
            {
                string comm_status = "";
                var task = Task.Run(() => { Check_Comport_Status(); });
                while (true)
                {
                    if (!string.IsNullOrEmpty(powerSupplyPortStatus))
                    {
                        if (comm_status != powerSupplyPortStatus || Form1.serialParser.psuCommPort.PortName != form1.psuPortName.Text)
                        {
                            form1.Update_PSUPortName_Label(Form1.serialParser.psuCommPort.PortName);
                            comm_status = powerSupplyPortStatus;
                            form1.Update_Info_textbox(comm_status);


                            if (comm_status.Contains("PSU Port Open - "))
                            {
                                form1.Update_StartTest_Button_Status(true);
                            }
                            if (comm_status.Contains("PSU port not found"))
                            {
                                form1.Update_StartTest_Button_Status(false);
                            }
                        }
                    }
                    
                    Thread.Sleep(250);
                }
            }
            catch (ThreadInterruptedException ex)
            {
                Logging.Write_Exceptions(ex);
            }
        }

        public void Check_Comport_Status()
        {
            try
            {
                while (true)
                {
                    if (Form1.serialParser.psuCommPort != null)
                    {
                        if (!GlobalVariables.PSUCommPortOpenAndWOrking)
                        {
                            Find_And_Open_Port();
                        }
                        else
                        {
                            if (!Form1.serialParser.psuCommPort.IsOpen)
                            {
                                GlobalVariables.PSUCommPortOpenAndWOrking = false;
                                Find_And_Open_Port();
                            }
                        }
                    }
                    Thread.Sleep(1500);
                }
            }
            catch (ThreadInterruptedException ex)
            {
                Logging.Write_Exceptions(ex);
            }
        }

        private void Find_And_Open_Port()
        {
            if (!Main_CommPort_Controls.Get_Com_Port(ref Form1.serialParser.psuCommPort, GlobalVariables.PSUComPortName))
            {
                powerSupplyPortStatus = "PSU Port not found";
                Form1.serialParser.psuCommPort.PortName = "NOT_FOUND";
            }
            else
            {
                if(Main_CommPort_Controls.Open_Port(ref Form1.serialParser.psuCommPort, 9600, true))
                {
                    GlobalVariables.PSUCommPortOpenAndWOrking = true;
                    powerSupplyPortStatus = "PSU Port Open - " + Form1.serialParser.psuCommPort.PortName;
                }
            }
        }
    }
}
