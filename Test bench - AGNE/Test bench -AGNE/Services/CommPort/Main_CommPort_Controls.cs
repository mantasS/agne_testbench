using System;
using System.IO.Ports;
using System.Management;

namespace Test_bench_AGNE.Services.CommPort
{
    public static class Main_CommPort_Controls
    {
        public static bool Get_Com_Port(ref SerialPort serialPort, string comm_description)
        {
            var res = Find_COM_Port(comm_description);
            if (!string.IsNullOrEmpty(res))
            {
                var index = res.IndexOf("COM", 0);///COMID
                serialPort.PortName = res.Substring(index, res.Length - index - 1);
                return true;
            }

            
            return false;
        }
        
        public static string Find_COM_Port(string com_descrption)
        {
            string comport_Name = "";
            ConnectionOptions connectionOption = new ();
            ManagementScope managementScope = new (new ManagementPath("\\\\.\\root\\cimv2"), connectionOption);
            ManagementObjectCollection managementObjectCollections = (new ManagementObjectSearcher(managementScope, new ObjectQuery("SELECT * FROM Win32_PnPEntity"))).Get();
            managementObjectCollections.GetEnumerator();
            foreach (ManagementObject managementObject in managementObjectCollections)
            {
                try
                {
                    if (managementObject["Name"] != null)
                    {
                        if (managementObject["Name"].ToString().Contains(com_descrption))
                        {
                            comport_Name = managementObject["Name"].ToString();///COMID
                            return comport_Name;
                        }
                    }

                }
                catch (Exception ex)
                {
                    Logging.Write_Exceptions(ex);
                }
            }

            return comport_Name;
        }

        public static bool Open_Port(ref SerialPort serialPort, int baudrate, bool useParser)
        {
            try
            {
                serialPort.BaudRate = baudrate;
                serialPort.Parity = Parity.None;
                serialPort.DataBits = 8;
                serialPort.StopBits = StopBits.One;
                serialPort.Handshake = Handshake.None;

                if (useParser)
                {
                    serialPort.DataReceived += new SerialDataReceivedEventHandler(Form1.serialParser.SerialPort_Data_Parser);
                }

                if (!serialPort.IsOpen)
                {
                    serialPort.Open();
                    return true;
                }
                else
                {
                    serialPort.Open();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
            }
            return false;
        }
    }
}
