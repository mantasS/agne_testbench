﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Test_bench_AGNE.Models.Device;
using Test_bench_AGNE.Models.Global;
using Test_bench_AGNE.Services.Wialon.Models.XDMData;

namespace Test_bench_AGNE.Services.XDM
{
    public partial class XDM
    {
        public string xdmApiAccessToken;
        public long xdmApiAccessTokenTimeout;
        private readonly string apiClientSecret = "CKFIVzm5Bmex8tLomalZerF6cx8I7Xrl";

        ///////XDM API:
        /// <summary>
        /// Get XDM API Access Token using client credentials
        /// </summary>
        /// <returns>True or False status of http request (on True saves access Token and access Token timeout)</returns>
        public async Task<bool> Get_API_Access_Token()
        {
            var httpClient = new HttpClient();
            var url = "https://auth.xgfleet.eu/connect/token";
            HttpResponseMessage response = await httpClient.PostAsync(url, new StringContent("" +
                "client_id=fmsetApiTestClient&client_secret=" + apiClientSecret + "&grant_type=client_credentials&scope=fmset.api.resource",
                Encoding.UTF8, "application/x-www-form-urlencoded"));
            var responseFromServer = await response.Content.ReadAsStringAsync();

            try
            {
                Logging.Write_Response(responseFromServer);
                var parsed_LoginData = JsonConvert.DeserializeObject<XDMData>(responseFromServer);
                xdmApiAccessToken = parsed_LoginData.access_token;
                xdmApiAccessTokenTimeout = DateTimeOffset.Now.ToUnixTimeSeconds() + parsed_LoginData.expires_in;

                if (!String.IsNullOrEmpty(xdmApiAccessToken))
                {
                    return true;
                }
                else return false;
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
                return false;
            }
        }

        /// <summary>
        /// Check if XDM token is not timed out
        /// If its timeout is reached new token is generated
        /// </summary>
        private async void Check_Xdm_Api_Access_Token()
        {
            if (xdmApiAccessTokenTimeout <= DateTimeOffset.Now.ToUnixTimeSeconds())
            {
                await Get_API_Access_Token();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imeis"></param>
        /// <returns></returns>
        public async Task<UserTemplates> Get_SdkDevice_Configs_ByDealerID(int dealerId)
        {
            XDMData xdmData = new ();
            Filter filter = new ();
            xdmData.paginator = Return_paginator();
            filter.dealerId = dealerId;
            xdmData.filter = filter;


            return JsonConvert.DeserializeObject<UserTemplates>(await XDM_Api_Post("/api/external/v1/userTemplates/filter", xdmData));
        }

        /// <summary>
        /// Get List with all available config names and their ids for List of imeis
        /// </summary>
        /// <param name="imeis"></param>
        /// <returns>list with string name, int id</returns>
        public async Task<List<Config>> Get_SdkDevice_Configs_ByIMEI(List<string> imeis)
        {
            Filter filter = new ();
            filter.imeis = imeis;
            return JsonConvert.DeserializeObject<List<Config>>(await XDM_Api_Post("/api/external/v1/configs/forDevices", filter));
        }

        /// <summary>
        /// Get List with all available config names and their ids for one imei
        /// </summary>
        /// <param name="imei"></param>
        /// <returns>list with string name, int id</returns>
        public async Task<List<Config>> Get_SdkDevice_Configs_ByIMEI(string imei)
        {
            List<string> imeis = new();
            imeis.Add(imei);

            return await Get_SdkDevice_Configs_ByIMEI(imeis);
        }

        /// <summary>
        /// Get Config id searched by name of Config
        /// </summary>
        /// <param name="imeis"></param>
        /// <param name="configName"></param>
        /// <returns>id of config searched</returns>
        public async Task<int> Get_SdkDevice_Configs_Id_ByIMEI(List<string> imeis, string configName)
        {
            var parsed_ConfigData = await Get_SdkDevice_Configs_ByIMEI(imeis);

            return Get_Id_By_Name(parsed_ConfigData, configName);
        }


        /// <summary>
        /// Get Config id searched by name of Config
        /// </summary>
        /// <param name="imeis"></param>
        /// <param name="configName"></param>
        /// <returns>id of config searched</returns>
        public async Task<int> Get_SdkDevice_Configs_Id_ByDealer(string configName)
        {
            var parsed_ConfigData = await Get_SdkDevice_Configs_ByDealerID(myAccountDealerId);

            return Get_Id_By_Name(parsed_ConfigData.results, configName);
        }

        /// <summary>
        /// Get all available firmwares for List of IMEIS 
        /// </summary>
        /// <param name="imeis"></param>
        /// <returns> List with firmwares, their ids </returns>
        public async Task<List<Firmwares>> Get_SdkDevice_Firmwares_ByIMEI(List<string> imeis)
        {
            Filter filter = new ();
            filter.imeis = imeis;

            return JsonConvert.DeserializeObject<List<Firmwares>>(await XDM_Api_Post("/api/external/v1/firmware/forDevices", filter));
        }

        /// <summary>
        /// Get all available firmwares for List of IMEI
        /// </summary>
        /// <param name="imei"></param>
        /// <returns>List with firmwares, their ids</returns>
        public async Task<List<Firmwares>> Get_SdkDevice_Firmwares_ByIMEI(string imei)
        {
            return await Get_SdkDevice_Firmwares_ByIMEI(Get_List_From_One_Item(imei));
        }

        /// <summary>
        /// Returns firmware Id found by firmware name
        /// </summary>
        /// <param name="imeis"></param>
        /// <param name="firmwareName"></param>
        /// <returns>firmware id</returns>
        public async Task<int> Get_SdkDevice_Firmware_Id(List<string> imeis, string firmwareName)
        {
            var firmwares = await Get_SdkDevice_Firmwares_ByIMEI(imeis);

            return Get_Id_By_Name(firmwares, firmwareName);
        }

        /// <summary>
        /// Gets information for list of imeis
        /// </summary>
        /// <param name="imeis"></param>
        /// <returns></returns>
        public async Task<XDMData> Get_SdkDevice_Information_ByIMEI(List<string> imeis)
        {
            XDMData requestData = new ();
            requestData.filter = new ();
            requestData.paginator = Return_paginator();
            requestData.filter.imeis = imeis;

            return JsonConvert.DeserializeObject<XDMData>(await XDM_Api_Post("/api/external/v1/devicesSdk/filter", requestData));
        }

        /// <summary>
        /// Gets information for one device with specific imei
        /// </summary>
        /// <param name="imei"></param>
        /// <returns>information of device. Info is listed in XDM API documentation, paragraph 3.2</returns>
        public async Task<XDMData> Get_SdkDevice_Information_ByIMEI(string imei)
        {
            return await Get_SdkDevice_Information_ByIMEI(Get_List_From_One_Item(imei));
        }

        /// <summary>
        /// Gets a list with all possible sdk hardware versions
        /// </summary>
        /// <param name="imeis"></param>
        /// <returns></returns>
        public async Task<XDMData> Get_SdkDevice_Versions(List<string> imeis)
        {
            XDMData requestData = new ();
            requestData.filter = new ();
            requestData.paginator = Return_paginator();
            requestData.filter.imeis = imeis;

            return JsonConvert.DeserializeObject<XDMData>(await XDM_Api_Post("/api/external/v1/hardwares/sdkVersions", requestData));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imeis"></param>
        /// <param name="notes"></param>
        /// <returns></returns>
        public async Task<bool> Update_Devices_Notes(List<string> imeis, string notes)
        {
            return await Update_Mass_Devices_SDK(imeis, null, null, notes, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <param name="notes"></param>
        /// <returns></returns>
        public async Task<bool> Update_Device_Notes(string imei, string notes)
        {
            return await Update_Mass_Devices_SDK(Get_List_From_One_Item(imei), null, null, notes, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imeis"></param>
        /// <param name="configName"></param>
        /// <returns></returns>
        public async Task<bool> Update_Devices_Config(List<string> imeis, string configName)
        {
            int configId = await Get_SdkDevice_Configs_Id_ByIMEI(imeis, configName);
            if (configId == 0)
            {
                var configs = await Get_SdkDevice_Configs_ByDealerID(myAccountDealerId);
                var configIDDealer = Get_Id_By_Name(configs.results, configName);
                if (configIDDealer != 0)
                {
                    Logging.Logs_WriteLine("Config doesn't exist for current device");
                    return false;
                }
            }
            return await Update_Mass_Devices_SDK(imeis, null, configId, null, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <param name="configName"></param>
        /// <returns></returns>
        public async Task<bool> Update_Device_Config(string imei, string configName)
        {
            if (await Check_Last_Connection_Time(imei))
            {
                if (await Check_Device_State(imei))
                {
                    return await Update_Devices_Config(Get_List_From_One_Item(imei), configName);
                }
                else
                {
                    Logging.Logs_WriteLine("Device stuck updating");
                }
            }
            else
            {
                Logging.Logs_WriteLine("Device didn't connect to xdm");
            }

            
            return false;
        }

        public async Task<bool> Update_Devices_Config(List<string> imeis, int configId)
        {
            return await Update_Mass_Devices_SDK(imeis, null, configId, null, null);
        }

        public async Task<bool> Update_Device_Config(string imei, int configId)
        {
            return await Update_Devices_Config(Get_List_From_One_Item(imei), configId);
        }

        /// <summary>
        /// changes firmware in XDM for list of devices
        /// </summary>
        /// <param name="imeis"></param>
        /// <param name="firmwareName"></param>
        /// <returns>status</returns>
        public async Task<bool> Update_Devices_Firmware(List<string> imeis, string firmwareName)
        {
            int firmwareId = await Get_SdkDevice_Firmware_Id(imeis, firmwareName);
            if (firmwareId == 0)
            {
                await Make_Firmware_Available(firmwareName);
                firmwareId = await Get_SdkDevice_Firmware_Id(imeis, firmwareName);
            }

            if (firmwareId == 0)
            {
                Logging.Logs_WriteLine("Firmware not found");
            }
            return await Update_Mass_Devices_SDK(imeis, firmwareId, null, null, null);
        }

        /// <summary>
        /// changes firmware in XDM for one device
        /// </summary>
        /// <param name="imei"></param>
        /// <param name="firmwareName"></param>
        /// <returns>status</returns>
        public async Task<bool> Update_Device_Firmware(string imei, string firmwareName)
        {
            return await Update_Devices_Firmware(Get_List_From_One_Item(imei), firmwareName);
        }

        /// <summary>
        /// Updates multiple sdk devices
        /// </summary>
        /// <param name="imeis"></param>
        /// <param name="newFirmawreId"></param>
        /// <param name="newConfigId"></param>
        /// <param name="notes"></param>
        /// <returns>status</returns>
        public async Task<bool> Update_Mass_Devices_SDK(List<string> imeis, int? newFirmawreId, int? newConfigId, string notes, int? dealerId)
        {
            var httpClient = new HttpClient();
            var url = XDMLiveUrl + "/api/external/v1/devicesSdk/multiple";
            httpClient.DefaultRequestHeaders.Authorization = new ("bearer", xdmApiAccessToken);

            Mass_Update_Devices mass_Update_Devices_data = new ();
            mass_Update_Devices_data.imeis = imeis;
            mass_Update_Devices_data.newConfigId = newConfigId;
            mass_Update_Devices_data.newFirmwareId = newFirmawreId;
            mass_Update_Devices_data.newDealerId = dealerId;
            mass_Update_Devices_data.notes = notes;

            HttpResponseMessage response = await httpClient.PutAsync(url, new StringContent(JsonConvert.SerializeObject(mass_Update_Devices_data), Encoding.UTF8, "application/json"));
            

            if (((int)response.StatusCode) == 200)
            {
                return true;
            }
            else if (((int)response.StatusCode) == 500)
            {
                string content = await response.Content.ReadAsStringAsync();
                if (content.Contains("Provided config does not exist"))
                {
                    Logging.Logs_WriteLine("Provided config does not exist");
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <returns></returns>
        public async Task<bool> Check_Device_State(string imei)
        {
            var configUpdateStatus = await Get_SdkDevice_Information_ByIMEI(imei);

            string deviceState = "";

            Stopwatch timeoutWatch = new();
            timeoutWatch.Start();

            while (deviceState != "Finished")
            {
                if (timeoutWatch.ElapsedMilliseconds > 180000)
                {
                    timeoutWatch.Stop();
                    return false;
                }
                Thread.Sleep(500);

                configUpdateStatus = await Get_SdkDevice_Information_ByIMEI(imei);
                if (configUpdateStatus.results != null)
                {
                    deviceState = configUpdateStatus.results[0].information.configurationUpdate.configUpdateState;
                }
            }

            timeoutWatch.Stop();
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <returns></returns>
        public async Task<bool> Check_Last_Connection_Time(string imei)
        {
            long timeOnRequest = DateTimeOffset.Now.ToUnixTimeSeconds();
            var configUpdateStatus = await Get_SdkDevice_Information_ByIMEI(imei);
            Stopwatch timeoutWatch = new();
            timeoutWatch.Start();


            while (timeoutWatch.ElapsedMilliseconds < 120000)
            {
                if (configUpdateStatus.results != null)
                {
                    if (configUpdateStatus.results[0].information.activityUpdate.lastActivity > timeOnRequest)
                    {
                        timeoutWatch.Stop();
                        return true;
                    }
                }
                
                configUpdateStatus = await Get_SdkDevice_Information_ByIMEI(imei);
                Thread.Sleep(500);
            }


            timeoutWatch.Stop();
            return false;
        }

        /// <summary>
        /// handles http POST request to XDM api
        /// </summary>
        /// <param name="absolutePath"></param>
        /// <param name="requestData"></param>
        /// <returns>unparsed request response content data</returns>
        public async Task<string> XDM_Api_Post(string absolutePath, object requestData)
        {
            Check_Xdm_Api_Access_Token();
            var httpClient = new HttpClient();
            var url = XDMLiveUrl + absolutePath;
            httpClient.DefaultRequestHeaders.Authorization = new ("bearer", xdmApiAccessToken);
            StringContent request_Content = new (JsonConvert.SerializeObject(requestData), Encoding.UTF8, "application/json");
            HttpResponseMessage response = await httpClient.PostAsync(url, request_Content);
            string responseFromServer = await response.Content.ReadAsStringAsync();
            try
            {
                return responseFromServer;
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
            }

            return null;
        }



        /// <summary>
        /// creates paginator object
        /// </summary>
        /// <returns>paginator object</returns>
        private static Paginator Return_paginator()
        {
            Paginator paginator = new ();
            paginator.firstRecord = 0;
            paginator.itemsPerPage = 100;
            return paginator;
        }

        /// <summary>
        /// creates a list and adds an item to it.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>list with one item in it</returns>
        private static List<string> Get_List_From_One_Item(string item)
        {
            List<string> itemList = new ();
            itemList.Add(item);
            return itemList;
        }

        /// <summary>
        /// returns hardware id by the hardware name in XDM
        /// </summary>
        /// <param name="imei"></param>
        /// <returns>status</returns>
        public async Task<bool> Check_Hardware_Type(long imei)
        {
            var hardware = await Get_SdkDevice_Information_ByIMEI(imei.ToString());
            if (hardware.results != null)
            {
                var hardwareVersionName = hardware.results[0].settings.hardware.name;

                int hardwareType = Hardware_Type.Unknown_Hardware;

                if (hardwareVersionName == "Light 2G")
                {
                    hardwareType = Hardware_Type.Light_2G;
                }
                else if (hardwareVersionName == "Light 3G")
                {
                    hardwareType = Hardware_Type.Light_3G;
                }
                else if (hardwareVersionName == "Light 2G 32Mb")
                {
                    hardwareType = Hardware_Type.Light_2G_32Mb;
                }
                else if (hardwareVersionName == "Light LTE Cat M1 XG3780U00C1")
                {
                    hardwareType = Hardware_Type.Light_LTE_Cat_M1;
                }
                else if (hardwareVersionName == "Light LTE Cat 1 XG3742U00C1")
                {
                    hardwareType = Hardware_Type.Light_LTE_Cat_1;
                }
                else if (hardwareVersionName == "Light+ 2G")
                {
                    hardwareType = Hardware_Type.Light_plus_2G;
                }
                else if (hardwareVersionName == "Light+ 3G")
                {
                    hardwareType = Hardware_Type.Light_plus_3G;
                }
                else if (hardwareVersionName == "Light+ 2G 32Mb")
                {
                    hardwareType = Hardware_Type.Light_plus_2G_32Mb;
                }
                else if (hardwareVersionName == "Light+ LTE Cat M1 XG3780U00C2")
                {
                    hardwareType = Hardware_Type.Light_plus_LTE_Cat_M1;
                }
                else if (hardwareVersionName == "Light+ LTE Cat 1 XG3742U00C2")
                {
                    hardwareType = Hardware_Type.Light_plus_LTE_Cat_1;
                }
                else if (hardwareVersionName == "StCAN 2G")
                {
                    hardwareType = Hardware_Type.StCAN_2G;
                }
                else if (hardwareVersionName == "StCAN 3G")
                {
                    hardwareType = Hardware_Type.StCAN_3G;
                }
                else if (hardwareVersionName == "StCAN 2G 32Mb")
                {
                    hardwareType = Hardware_Type.StCAN_2G_32Mb;
                }
                else if (hardwareVersionName == "StCAN LTE Cat M1 XG3780U00C3")
                {
                    hardwareType = Hardware_Type.StCAN_LTE_Cat_M1;
                }
                else if (hardwareVersionName == "StCAN LTE Cat 1 XG3742U00C3")
                {
                    hardwareType = Hardware_Type.StCAN_LTE_Cat_1;
                }
                else if (hardwareVersionName == "XtCAN 2G")
                {
                    hardwareType = Hardware_Type.XtCAN_2G;
                }
                else if (hardwareVersionName == "XtCAN 3G")
                {
                    hardwareType = Hardware_Type.XtCAN_3G;
                }
                else if (hardwareVersionName == "XtCAN 2G 32Mb")
                {
                    hardwareType = Hardware_Type.XtCAN_2G_32Mb;
                }
                else if (hardwareVersionName == "XtCAN LTE Cat M1 XG3780U00C5")
                {
                    hardwareType = Hardware_Type.XtCAN_LTE_Cat_M1;
                }
                else if (hardwareVersionName == "XtCAN LTE Cat 1 XG3742U00C5")
                {
                    hardwareType = Hardware_Type.XtCAN_LTE_Cat_1;
                }
                else if (hardwareVersionName == "XtCAN 2G 128MB")
                {
                    hardwareType = Hardware_Type.XtCAN_2G_128Mb;
                }
                else if (hardwareVersionName == "Tacho 2G")
                {
                    hardwareType = Hardware_Type.Tacho_2G;
                }
                else if (hardwareVersionName == "Tacho 3G")
                {
                    hardwareType = Hardware_Type.Tacho_3G;
                }
                else if (hardwareVersionName == "Tacho 2G 32Mb")
                {
                    hardwareType = Hardware_Type.Tacho_2G_32Mb;
                }
                else if (hardwareVersionName == "Tacho LTE Cat M1 XG3780U00C4")
                {
                    hardwareType = Hardware_Type.Tacho_LTE_Cat_M1;
                }
                else if (hardwareVersionName == "Tacho LTE Cat 1 XG3742U00C4")
                {
                    hardwareType = Hardware_Type.Tacho_LTE_Cat_1;
                }
                else if (hardwareVersionName == "XG4780")
                {
                    hardwareType = Hardware_Type.XG4780;
                }
                else
                {
                    Logging.Logs_WriteLine("Unknown HW type " + hardwareVersionName);
                }

                GlobalVariables.hardwareType = hardwareType;
                GlobalVariables.hardwareTypeID = hardware.results[0].settings.hardware.id;
                return true;
            }
            return false;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        public async Task<string> Get_User_Template_Categories(int templateId)
        {
            return await XDM_Api_Get("/api/external/v1/userTemplates/" + templateId);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        public async Task<int> Get_UserTemplate_Category_ByName(int templateId, string categoryName)
        {
            var results = JsonConvert.DeserializeObject<UserTemplates>(await Get_User_Template_Categories(templateId));
            return Get_Id_By_Name(results.categories, categoryName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="categoryName"></param>
        /// <param name="categorieID"></param>
        /// <returns></returns>
        public async Task<int> Get_UserTemplate_Category_ByName(int templateId, string categoryName, int categorieID)
        {
            var results = JsonConvert.DeserializeObject<UserTemplates>(await Get_UserTEmplateCategory(templateId, categorieID));
            return Get_Id_By_Name(results.subCategories, categoryName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private static int Get_Id_By_Name(object data, string name)
        {
            var list = JsonConvert.DeserializeObject<List<Config>>(JsonConvert.SerializeObject(data));
            if (list[0].name != null)
            {
                foreach (var item in list)
                {
                    if (item.name.Contains(name))
                    {
                        return item.id;
                    }
                }
            }
            else
            {
                var newlist = JsonConvert.DeserializeObject<List<Firmwares>>(JsonConvert.SerializeObject(data));
                foreach (var item in newlist)
                {
                    if (item.version == name)
                    {
                        return item.id;
                    }
                }
            }

            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="categorieID"></param>
        /// <returns></returns>
        public async Task<string> Get_UserTEmplateCategory(int templateId, int categorieID)
        {
            return await XDM_Api_Get("/api/external/v1/userTemplates/" + templateId + "/categories/" + categorieID);
        }

        /// <summary>
        /// handles http GET request to XDM api
        /// </summary>
        /// <param name="absolutePath"></param>
        /// <returns>unparsed request response content data</returns>
        public async Task<string> XDM_Api_Get(string absolutePath)
        {
            Check_Xdm_Api_Access_Token();
            var httpClient = new HttpClient();
            var url = XDMLiveUrl + absolutePath;
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", xdmApiAccessToken);
            HttpResponseMessage response = await httpClient.GetAsync(url);
            string responseFromServer = await response.Content.ReadAsStringAsync();
            try
            {
                return responseFromServer;
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
            }

            return null;
        }


    }
}
