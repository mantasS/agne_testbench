using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Test_bench_AGNE.Models.Global;
using Test_bench_AGNE.Models.XDMData;
using Test_bench_AGNE.Services.Wialon.Models;
using Test_bench_AGNE.Services.Wialon.Models.XDMData;
using Test_bench_AGNE.Services.Wialon.Models.XDMData.Firmware;

namespace Test_bench_AGNE.Services.XDM
{
    public partial class XDM
    {
        public string xdmAccessToken;
        public long xdmAccessTokenTimeout;
        private readonly int myAccountDealerId = 2065;
        private readonly string XDMLiveUrl = "https://xdm.xgfleet.eu";
        public string configurationFilesPath = AppDomain.CurrentDomain.BaseDirectory + "Device_Configuration_Files";
        public string firmwaresPath = AppDomain.CurrentDomain.BaseDirectory + "Firmwares";


        public XDM()
        {
            Get_API_Access_Token().Wait();
            Check_If_Firmwares_Folder_Exist();
            Check_If_Configurations_Folder_Exist();
            Get_XDM_Access_Token().Wait();

        }

        /// <summary>
        /// Checks if tests config files folder exists, if not creates one
        /// </summary>
        public void Check_If_Configurations_Folder_Exist()
        {
            if (!Directory.Exists(configurationFilesPath))
            {
                Directory.CreateDirectory(configurationFilesPath);
            }
        }

        /// <summary>
        /// Checks if devices firmware folder exists, if not creates one
        /// </summary>
        public void Check_If_Firmwares_Folder_Exist()
        {
            if (!Directory.Exists(firmwaresPath))
            {
                Directory.CreateDirectory(firmwaresPath);
            }
        }

        /// <summary>
        /// Gets XDM live access token
        /// </summary>
        /// <returns>XDM access token, XDM access token timeout</returns>
        public async Task Get_XDM_Access_Token()
        {
            var httpClient = new HttpClient();
            var url = "https://auth.xgfleet.eu/connect/token";
            HttpResponseMessage response = await httpClient.PostAsync(url, new StringContent("grant_type=password&scope=openid%20profile%20" +
                "email%20publicApi%20fmset.api.resource%20offline_access%20identity.server%20tvs.api.resource%20tvs.ecoWatcher.resource%20" +
                "tvs.fileExportService.resource%20tvs.web.resource%20tvs.webSockets.resource&username=mantasserpetauskis&password" +
                "=piktaskiskis15963%23&client_id=fmsetWebClient&client_secret=publicApi", Encoding.UTF8, "application/x-www-form-urlencoded"));
            var responseFromServer = await response.Content.ReadAsStringAsync();
            Logging.Write_Response(responseFromServer);
            var sessionId = JsonConvert.DeserializeObject<XDMData>(responseFromServer);
            xdmAccessToken = sessionId.access_token;
            xdmAccessTokenTimeout = DateTimeOffset.Now.ToUnixTimeSeconds() + sessionId.expires_in;
        }

        /// <summary>
        /// Checks XDM token timeout, if it timed out, calls a function to get a new access token
        /// </summary>
        private async void Check_Xdm_Access_Token()
        {
            if (xdmAccessTokenTimeout <= DateTimeOffset.Now.ToUnixTimeSeconds())
            {
                await Get_XDM_Access_Token();
            }
        }

        /// <summary>
        /// Downloads needed device config (settings template) from XDM live
        /// </summary>
        /// <param name="configName"></param>
        /// <param name="imei"></param>
        /// <returns></returns>
        public async Task<bool> Download_Config(string configName, string imei)
        {
            Check_Xdm_Access_Token();
            try
            {
                var http = new HttpClient();

                var configId = await Get_SdkDevice_Configs_Id_ByIMEI(Get_List_From_One_Item(imei), configName);
                var url = XDMLiveUrl + "/api/UserSettingTemplates/GetTemplateFile/" + configId;
                http.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", xdmAccessToken);
                var response = await http.GetAsync(url);
                var bytes = await response.Content.ReadAsByteArrayAsync();
                
                if (bytes.Length != 0)
                {
                    File.WriteAllBytes(configurationFilesPath + "\\" + configName + ".bin", bytes);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
                return false;
            }
        }

        /// <summary>
        /// Downloads needed device firmware from XDM live
        /// </summary>
        /// <param name="firmwareName"></param>
        /// <param name="imei"></param>
        /// <returns></returns>
        public async Task<bool> Download_Firmware(string firmwareName, string imei)
        {
            try
            {
                Check_Xdm_Access_Token();
                var http = new HttpClient();
                var url = XDMLiveUrl + "/api/Firmware/DownloadFile";

                int firmwareId = await Get_SdkDevice_Firmware_Id(Get_List_From_One_Item(imei), firmwareName);

                http.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", xdmAccessToken);
                var response = await http.GetAsync(url + "?id=" + firmwareId + "&type=0");


                var bytes = await response.Content.ReadAsByteArrayAsync();
                string str = Encoding.ASCII.GetString(bytes);
                var str2 = str.Substring(1);
                str = str[1..];
                str2 = str.Substring(0, str.Length - 1);
                str = str[0..(str.Length-1)];
                var buffer = Convert.FromBase64String(str);
                File.WriteAllBytes(firmwaresPath + "\\" + firmwareName + ".aes", buffer);

                return true;
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex); 
                return false;
            }
        }

        /// <summary>
        /// Changes either firmware, config or phone number for device
        /// </summary>
        /// <param name="firmwareID"></param>
        /// <param name="configName"></param>
        /// <param name="deviceId"></param>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        public async Task Change_Firmware_Config_SDK(int? firmwareID, string configName, int deviceId, string phoneNumber)
        {
            int? configID = await Get_Config_Id_For_Hardware(GlobalVariables.hardwareTypeID, configName, 1);
            
            Config config_Parameters = new ();
            config_Parameters.notes = null;
            config_Parameters.newFirmwareId = firmwareID;
            config_Parameters.dealerId = myAccountDealerId;
            config_Parameters.newConfigId = configID;
            config_Parameters.phoneNumber = phoneNumber;
            config_Parameters.propertiesChanged = new ();
            if(configID != null)
                config_Parameters.propertiesChanged.Add("newConfigId");
            if (firmwareID != null)
                config_Parameters.propertiesChanged.Add("newFirmwareId");
            config_Parameters.configOverrides = new ();
            config_Parameters.configOverrides.modified = new ();
            config_Parameters.configOverrides.deleted = new ();
            config_Parameters.id = deviceId;

            await XDM_Post("/api/Devices2/UpdateDeviceSdk", config_Parameters);
        }

        /// <summary>
        /// Gets Config (settings template) ID, needed to download it or set it to device
        /// </summary>
        /// <param name="hardwareVersionId"></param>
        /// <param name="configTempateName"></param>
        /// <param name="type"></param>
        /// <returns>return Config ID</returns>
        public async Task<int> Get_Config_Id_For_Hardware(int hardwareVersionId, string configTempateName, int type)
        {
            int? id = 0;

            Config_Search_Parameters config_Search_Parameters = new ();
            config_Search_Parameters.searchParameters = new ();
            config_Search_Parameters.searchParameters.searchColumn = "name";
            config_Search_Parameters.searchParameters.searchString = configTempateName;
            config_Search_Parameters.paginator = Return_paginator();
            config_Search_Parameters.hardwareVersionId = hardwareVersionId;
            config_Search_Parameters.dealerId = myAccountDealerId;
            var parsedData = JsonConvert.DeserializeObject<XDMData>(await XDM_Post("/api/ConfigSerializer/GetForHardware", config_Search_Parameters));

            if (parsedData.results.Count != 0)
            {
                if (type == 1) // returns config id for updating module
                {
                    id = parsedData.results[0].id;
                }
                else          // returns id for downloading config
                {
                    id = parsedData.results[0].userConfigTemplateId;
                }
            }

            if (id == null)
            {
                id = Error.noData;
            }
            return (int)id;
        }

        /// <summary>
        /// Gets all firmware parameters 
        /// </summary>
        /// <param name="firmware"></param>
        /// <returns>XDM_Firmware_Data object</returns>
        public async Task<XDM_Firmware_Data> Get_Firmwares(string firmware)
        {
            Get_All_Firmwares_Parameters get_All_Firmwares_Parameters = new();
            get_All_Firmwares_Parameters.parameters = new();
            get_All_Firmwares_Parameters.parameters.version = firmware;
            get_All_Firmwares_Parameters.paginator = Return_paginator();

            await ImportFirmwares();

            var firmwareData = await XDM_Post("/api/Firmware/GetFirmwares", get_All_Firmwares_Parameters);
            return JsonConvert.DeserializeObject<XDM_Firmware_Data>(firmwareData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task ImportFirmwares()
        {
            await XDM_Get("/api/Firmware/ImportFirmwares");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="makeAvailable"></param>
        /// <param name="makeVisible"></param>
        /// <param name="firmware"></param>
        /// <returns></returns>
        public async Task<bool> Change_Firmware_Status(bool makeAvailable, bool makeVisible, string firmware)
        {
            var firmwareData = await Get_Firmwares(firmware);
            if (firmwareData.results.Count == 0)
            {
                return false;
            }
            
            var dealers = await Get_Firmware_By_Type_And_Id(firmwareData.results[0].id);

            Update_Firmware firmwareUpdateParams = new ();
            firmwareUpdateParams.parameters = new ();
            firmwareUpdateParams.parameters.dealers = new ();
            firmwareUpdateParams.parameters.notes = "";

            foreach (var firmwareId in dealers.assignedDealers)
            {
                firmwareUpdateParams.parameters.dealers.Add(firmwareId.id);
            }

            if (!firmwareUpdateParams.parameters.dealers.Contains(myAccountDealerId))
            {
                firmwareUpdateParams.parameters.dealers.Add(myAccountDealerId);
            }

            firmwareUpdateParams.parameters.id = firmwareData.results[0].id;

            if (makeAvailable)
            {
                firmwareUpdateParams.parameters.available = makeAvailable;
            }
            else
            {
                firmwareUpdateParams.parameters.available = firmwareData.results[0].available;
            }

            if (makeVisible)
            {
                firmwareUpdateParams.parameters.visibleToAll = makeVisible;
            }
            else
            {
                firmwareUpdateParams.parameters.visibleToAll = firmwareData.results[0].visibleToAll;
            }


            var result = await XDM_Post("/api/Firmware/UpdateFirmware", firmwareUpdateParams);

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="firmwareName"></param>
        /// <returns></returns>
        public async Task<bool> Make_Firmware_Available(string firmwareName)
        {
            return await Change_Firmware_Status(true, false, firmwareName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="firmwareName"></param>
        /// <returns></returns>
        public async Task<bool> Make_Firmware_Visible(string firmwareName)
        {
            return await Change_Firmware_Status(false, true, firmwareName);
        }

        public async Task<Firmware_Dealers> Get_Firmware_By_Type_And_Id(int firmwareId)
        {
            var firmwareData = await XDM_Get("/api/Firmware/GetFirmwareByTypeAndId?id=" + firmwareId + "&type=0");
            
            return JsonConvert.DeserializeObject<Firmware_Dealers>(firmwareData);
        }

        public async Task<bool> Send_SMS(string imei, string message, bool sendToScript)
        {
            XDM_SMS xdm_SMS = new();
            xdm_SMS.addSignature = !sendToScript;
            xdm_SMS.devicesImeis = new();
            xdm_SMS.devicesImeis.Add(imei);
            xdm_SMS.message = message;
            xdm_SMS.smsType = "Custom";

            var firmwareData = await XDM_Post("/api/Sms/SendSmsToDevicesSdk", xdm_SMS);
            if(firmwareData.Contains(imei) && firmwareData.Contains("error_OK"))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="absolutePath"></param>
        /// <param name="requestData"></param>
        /// <returns></returns>
        public async Task<string> XDM_Post(string absolutePath, object requestData)
        {
            try
            {
                Check_Xdm_Access_Token();
                var httpClient = new HttpClient();
                var url = XDMLiveUrl + absolutePath;
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", xdmAccessToken);
                StringContent request_Content = new (JsonConvert.SerializeObject(requestData), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await httpClient.PostAsync(url, request_Content);

                return await response.Content.ReadAsStringAsync();
            }
            catch(Exception ex)
            {
                Logging.Write_Exceptions(ex);
                return "";
            }
            
        }

        public async Task<string> XDM_Get(string absolutePath)
        {
            Check_Xdm_Access_Token();
            var httpClient = new HttpClient();
            var url = XDMLiveUrl + absolutePath;
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", xdmAccessToken);
            HttpResponseMessage response = await httpClient.GetAsync(url);

            return await response.Content.ReadAsStringAsync();
        }
    }
}
