﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Test_bench_AGNE.Models.Wialon;

namespace Test_bench_AGNE.Services.Wialon
{
    public partial class Wialon
    {
        private readonly string wialonTokenPath = AppDomain.CurrentDomain.BaseDirectory + "\\WialonToken.cs";
        static string wialonAuthenticationToken = "";
        public static bool authenticationTokenTimeout;
        private static Token tokenParams;

        static long wialonSessionId_Timeout;


        public Wialon()
        {
            if (!Check_Authentication_Token().Result)
            {
                if (!Get_New_Authentification_Token().Result)
                {
                    Get_Wialon_Session_ID().Wait();
                }
            }

            //checkIfConnected(862785044218272).Wait();
            //Get_Item_InternalID(862785044218272).Wait();
            //Get_Item_Commands(862785044218272).Wait();
            //Add_Command(862785044218272, "naujakomanda", "naujakomanda").Wait();
            //Send_GPRS_Command_ByName(862785044218272, "naujakomanda").Wait();
        }

        /// <summary>
        /// Checks if there currently is wialon Authentication Token file
        /// Reads it and checks if its correct format
        /// Then checks if authentication token is still valid
        /// </summary>
        /// <returns>status</returns>
        private async Task<bool> Check_Authentication_Token()
        {
            if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\WialonToken.cs"))
            {
                return false;
            }

            try
            {
                tokenParams = JsonConvert.DeserializeObject<Token>(File.ReadAllText(wialonTokenPath));
            }
            catch
            {
                return false;
            }

            return await Authentication_Token_Validity();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static async Task<bool> Authentication_Token_Validity()
        {
            if (tokenParams.timeout_date >= DateTimeOffset.Now.ToUnixTimeSeconds() + 10)
            {
                wialonAuthenticationToken = tokenParams.th;
                return true;
            }
            else
            {
                return await Get_New_Authentification_Token();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sessionParameters"></param>
        private void Save_Authentication_Token(SessionParameters sessionParameters)
        {
            Token tokenParams = new ();
            var tokenParameters = JsonConvert.DeserializeObject<TokenParams>(sessionParameters.token);
            tokenParams.th = sessionParameters.th;
            tokenParams.timeout_date = tokenParameters.ct + tokenParameters.dur;

            string jsonInString = JsonConvert.SerializeObject(tokenParams, Formatting.Indented);
            File.WriteAllText(wialonTokenPath, jsonInString);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static async Task<bool> Get_New_Authentification_Token()
        {
            var httpClient = new HttpClient();
            var url = "https://hst-api.wialon.com/oauth/authorize.html?" +
                "p=y5vLYPOBWfOnbQt%2BVS3dvrt1xlA0iSWh9VtdRisOzGKjZoWMRCp81teHSnVprgOZPosoCI%2FYeVHbqrN4%2BKtOkQ%3D%3D" +
                "&redirect_uri=https://hosting.wialon.com/login.html&response_type=token";
            HttpResponseMessage responseServer = await httpClient.GetAsync(url);
            var newurl = responseServer.RequestMessage.RequestUri.Query;
            if (newurl.Contains("access_token"))
            {
                var token_start = newurl.IndexOf("=");
                var token_end = newurl.IndexOf("&") - token_start - 1;
                wialonAuthenticationToken = newurl.Substring(token_start + 1, token_end);
                return true;
            }
            return false;
        }







        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task Get_Wialon_Session_ID()
        {
            var httpClient = new HttpClient();
            var url = wialonURL + "/ajax.html?svc=token/login&params=%7b%22token%22:%22" + wialonAuthenticationToken + "%22%7d";
            var responseServer = await httpClient.GetStringAsync(url);
            var sessionParameters = JsonConvert.DeserializeObject<SessionParameters>(responseServer);

            if (sessionParameters.eid != null)
            {
                Save_Authentication_Token(sessionParameters);
                wialonSessionId = sessionParameters.eid;
                wialonSessionId_Timeout = DateTimeOffset.Now.ToUnixTimeSeconds() + 260;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task Check_Session_ID()
        {
            if (wialonSessionId_Timeout <= DateTimeOffset.Now.ToUnixTimeSeconds())
                await Get_Wialon_Session_ID();
        }
    }
}
