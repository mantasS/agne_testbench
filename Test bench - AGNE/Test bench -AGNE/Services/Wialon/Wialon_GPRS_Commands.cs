﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Test_bench_AGNE.Models.Wialon;
using Test_bench_AGNE.Models.Wialon.Requests;

namespace Test_bench_AGNE.Services.Wialon
{
    public partial class Wialon
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <param name="commandName"></param>
        /// <returns></returns>
        public async Task<bool> Add_Command_ByName(long imei, string commandName)
        {
            return await Add_Command(imei, commandName, commandName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <param name="commandName"></param>
        /// <param name="commandData"></param>
        /// <returns></returns>
        public async Task<bool> Add_Command_CustomData(long imei, string commandName, string commandData)
        {
            return await Add_Command(imei, commandName, commandData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <param name="commandName"></param>
        /// <param name="commandData"></param>
        /// <returns></returns>
        private async Task<bool> Add_Command(long imei, string commandName, string commandData)
        {
            Wialon_Batch_Update_Data wialon_Batch_Update_Data = new ();
            Wialon_Batch_Update_Params @params = new ();
            @params.svc = "unit/update_command_definition";
            @params.@params = new ();

            @params.@params.id = 0;
            @params.@params.n = commandName;
            @params.@params.c = "bce_iotm_out";
            @params.@params.l = "";
            @params.@params.p = "13|" + commandData + "|1";
            @params.@params.a = 1;
            @params.@params.f = "0";
            @params.@params.que_length = 0;
            @params.@params.itemId = (int)await Get_Item_InternalID(imei);
            @params.@params.callMode = "create";

            wialon_Batch_Update_Data.@params = new ();
            wialon_Batch_Update_Data.@params.Add(@params);
            wialon_Batch_Update_Data.flags = 0;


            var result = await Wialon_Post_Request_urlencoded("/ajax.html?svc=core/batch&sid=" + wialonSessionId, wialon_Batch_Update_Data);

            if (result.IsSuccessStatusCode)
            {
                var str = await result.Content.ReadAsStringAsync();
                if (str.Contains(commandName))
                {
                    return true;
                }
            }

            return false;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <param name="commandName"></param>
        /// <returns></returns>
        public async Task<bool> Check_If_Item_Has_Command(long imei, string commandName)
        {
            var allCommands = await Get_Item_Commands(imei);

            foreach (var commands in allCommands.cmds)
            {
                if (commands.n == commandName)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <returns></returns>
        public async Task<Items> Get_Item_Commands(long imei)
        {
            return await Get_Parsed_Item_Parameters(imei, WialonDataFlags.availableForCurrentMomentCommands);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <param name="commandName"></param>
        /// <returns></returns>
        public async Task<bool> Send_GPRS_Command_ByName(long imei, string commandName)
        {
            if (await Check_If_Item_Has_Command(imei, commandName))
            {
                return await Send_GPRS_Command(imei, commandName);
            }
            else
            {
                if (await Add_Command_ByName(imei, commandName))
                {
                    return await Send_GPRS_Command(imei, commandName);
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <param name="commandName"></param>
        /// <returns></returns>
        public async Task<bool> Send_GPRS_Command(long imei, string commandName)
        {
            return await Send_GPRS_Command(imei, commandName, "", "", 0, 0x0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <param name="commandName"></param>
        /// <param name="linkType"></param>
        /// <param name="param"></param>
        /// <param name="timeout"></param>
        /// <param name="flags"></param>
        /// <returns></returns>
        public async Task<bool> Send_GPRS_Command(long imei, string commandName, string linkType, string param, int timeout, int flags)
        {
            if (!await checkIfConnected(imei))
                return false;

            Wialon_GPRS_Command_Request requestData = new ();
            requestData.itemId = (int)await Get_Item_InternalID(imei);
            requestData.commandName = commandName;
            requestData.linkType = linkType;             // linkType = ""; // empty string = auto, tcp = TCP, udp = UDP, vrt = virtual, gsm = SMS
            requestData.param = param;                   // param = "";
            requestData.timeout = timeout;               // int timeout = 0;  //Time out for command to wait in commands queue, in seconds
            requestData.flags = flags;                   // flags = 0; // 0x0 = use any phone number, 0x1 = use primary, 0x2 = use secondary, 0x10 = send param in JSON format

            var response = await Wialon_GetString_Request("/ajax.html?svc=unit/exec_cmd&params=", requestData);
            Logging.Write_Response(response);
            if (response != string.Empty && response != "{}\n")
            {
                Logging.Logs_WriteLine("Error executing remote command: " + response);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <returns></returns>
        public async Task<bool> checkIfConnected(long imei)
        {
            byte connected = 1;
            var timeout = 60;

            while (timeout > 0)
            {
                var wialonData = await Get_Item_Net_Connection_Status(imei);

                if (wialonData.netconn == connected)
                {
                    return true;
                }

                Thread.Sleep(1000);
                timeout--;
            }

            Logging.Logs_WriteLine("Module isn't connected to wialon");
            return false;
        }
    }
}
