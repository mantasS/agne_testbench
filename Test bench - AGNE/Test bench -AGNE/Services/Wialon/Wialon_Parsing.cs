﻿using System.Threading.Tasks;
using Test_bench_AGNE.Models.Wialon.Requests;

namespace Test_bench_AGNE.Services.Wialon
{
    public partial class Wialon
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data_string"></param>
        /// <param name="string_to_search"></param>
        /// <param name="string_end_symbol"></param>
        /// <param name="start_index"></param>
        /// <returns></returns>
        private static string get_value(string data_string, string string_to_search, string string_end_symbol, int start_index)
        {
            var index = data_string.IndexOf(string_to_search, start_index) + string_to_search.Length;
            if (index == -1)
            {
                return "";
            }
            var end_symbol_index = data_string.IndexOf(string_end_symbol, index);

            
            var return_val =  data_string.Substring(index, end_symbol_index - index);
            //var value = data_string[index..(end_symbol_index - index)];
            return return_val;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <param name="sensor_Name"></param>
        /// <returns></returns>
        public async Task<string[]> Get_Sensor_Data(long imei, string sensor_Name)
        {
            string[] sensor_Parameters = new string[4];
            var response_Contents = await Get_Item_Parameters(imei, WialonDataFlags.messageParameters);

            if (response_Contents.Contains(sensor_Name))
            {
                var start_index = response_Contents.IndexOf(sensor_Name);
                sensor_Parameters[0] = get_value(response_Contents, "v\":", ",", start_index);
                sensor_Parameters[1] = get_value(response_Contents, "at\":", "}", start_index);
                sensor_Parameters[2] = sensor_Parameters[0].Trim('"').Length.ToString();
                sensor_Parameters[3] = Get_Repetitions_Count(sensor_Parameters[0].Trim('"', ' ')).ToString();
            }

            return sensor_Parameters;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static int Get_Repetitions_Count(string data)
        {
            var count = data.Split(" ");
            return count.Length;
        }
    }
}
