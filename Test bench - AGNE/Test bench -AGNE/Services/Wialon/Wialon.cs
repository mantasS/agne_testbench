﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Test_bench_AGNE.Models.Wialon;
using Test_bench_AGNE.Models.Wialon.Requests;

namespace Test_bench_AGNE.Services.Wialon
{
    public partial class Wialon
    {
        private static string wialonSessionId;
        private readonly string wialonURL = "https://hst-api.wialon.com/wialon";

        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <returns></returns>
        public async Task<Items> Get_Item_Last_Message_Parameters(long imei)
        {
            return await Get_Parsed_Item_Parameters(imei, WialonDataFlags.lastMessageAndPosition);
        }

        public async Task<Items> Get_Item_Net_Connection_Status(long imei)
        {
            return await Get_Parsed_Item_Parameters(imei, WialonDataFlags.unitConnectionStatus);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <returns></returns>
        public async Task<Items> Get_Item_All_Message_Parameters(long imei)
        {
            return await Get_Parsed_Item_Parameters(imei, WialonDataFlags.messageParameters);
        }

        public async Task<int?> Get_Item_InternalID(long imei)
        {
            var internalID = await Get_Parsed_Item_Parameters(imei, WialonDataFlags.baseflag);
            return internalID.id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <param name="flag"></param>
        /// <returns></returns>
        public async Task<Items> Get_Parsed_Item_Parameters(long imei, uint flag)
        {
            string contentResponse = await Get_Item_Parameters(imei, flag);
            var wialonData = JsonConvert.DeserializeObject<WialonData>(contentResponse);

            return wialonData.items[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imei"></param>
        /// <param name="flag"></param>
        /// <returns></returns>
        public async Task<string> Get_Item_Parameters(long imei, uint flag)
        {
            Item_Params_Request item_Params_Request_Data = new ();
            item_Params_Request_Data.spec = new ();
            item_Params_Request_Data.spec.itemsType = "avl_unit";
            item_Params_Request_Data.spec.propName = "sys_unique_id";
            item_Params_Request_Data.spec.propValueMask = imei;
            item_Params_Request_Data.spec.sortType = "sys_name";
            item_Params_Request_Data.force = 1;
            item_Params_Request_Data.flags = flag;
            item_Params_Request_Data.from = 0;
            item_Params_Request_Data.to = 0;

            var response = await Wialon_GetString_Request("/ajax.html?svc=core/search_items&params=", item_Params_Request_Data);
            return response;
        }

        


        /// <summary>
        /// 
        /// </summary>
        /// <param name="absolutePath"></param>
        /// <param name="requestData"></param>
        /// <returns></returns>
        public async Task<string> Wialon_GetString_Request(string absolutePath, object requestData)
        {
            await Check_Session_ID();
            var httpClient = new HttpClient();
            return await httpClient.GetStringAsync(wialonURL + absolutePath + JsonConvert.SerializeObject(requestData) + "&sid=" + wialonSessionId);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="absolutePath"></param>
        /// <param name="requestData"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> Wialon_Post_Request_urlencoded(string absolutePath, object requestData)
        {
            await Check_Session_ID();
            var httpClient = new HttpClient();
            var requestContent = new StringContent("&params=" + JsonConvert.SerializeObject(requestData) + "&sid=" + wialonSessionId, Encoding.UTF8, "application/x-www-form-urlencoded");
            var response = await httpClient.PostAsync(wialonURL + absolutePath, requestContent);

            return response;
        }
    }
}
