using System;
using System.Net;
using System.Net.Sockets;
using System.Text;


namespace Test_bench_AGNE.Services
{
    public class UDPAsynchronousSocketListener
    {
        public static bool connected = false;
        public static string receivedDataFromDevice;
        public static string data = null;
        public static int listenPort = 1894;

        public static void StartListening()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());

            UdpClient udpClient = new (listenPort);
            IPEndPoint device_end_point = new (IPAddress.Any, 0);
            Socket udp_socket = new (AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            try
            {
                while(true)
                {
                    byte[] sentdata = udpClient.Receive(ref device_end_point);
                    receivedDataFromDevice = Encoding.UTF8.GetString(sentdata);
                    udpClient.Send(sentdata, sentdata.Length, device_end_point);
                }
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
            }

        }
    }
}

