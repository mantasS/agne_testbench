using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;


namespace Test_bench_AGNE.Services
{
    public class StateObject
    {
        public const int BufferSize = 1024;
        public byte[] buffer = new byte[BufferSize];
        public StringBuilder sb = new ();
        public Socket workSocket = null;
    }
    public class TcpAsynchronousSocketListener
    {
        public static bool connected = false;
        public static string received_data_from_Device;
        public static string data = null;
        public static bool acception_Ended = true;
        public static int socketPort = 1894;

        public static void StartListening()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());

            foreach (var ipAddress in ipHostInfo.AddressList)
            {
                string ipString = ipAddress.ToString();
                if (!String.IsNullOrWhiteSpace(ipString) && !connected)
                {
                    if (ipString.Split('.').Length == 4)
                    {
                        IPEndPoint localEndPoint = new (ipAddress, socketPort);
                        Socket Com_Socket = new (AddressFamily.InterNetwork,
                        SocketType.Stream, ProtocolType.Tcp);

                        try
                        {
                            Com_Socket.Bind(localEndPoint);
                            Com_Socket.Listen(10);

                            while (true)
                            {
                                if (acception_Ended)
                                {
                                    acception_Ended = false;
                                    Com_Socket.BeginAccept(new AsyncCallback(AcceptCallback), Com_Socket);
                                    connected = true;
                                }
                                Thread.Sleep(1500);
                            }
                        }
                        catch (Exception ex)
                        {
                            Logging.Write_Exceptions(ex);
                        }
                    }
                }
            }
        }


        public static void AcceptCallback(IAsyncResult asyncResult)
        {
            Socket listener = (Socket)asyncResult.AsyncState;
            Socket handler = listener.EndAccept(asyncResult);
            acception_Ended = true;
            StateObject state = new ();
            state.workSocket = handler;
            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);
        }

        public static void ReadCallback(IAsyncResult asyncResult)
        {
            String content = String.Empty;

            StateObject state = (StateObject)asyncResult.AsyncState;
            Socket handler = state.workSocket;

            int bytesRead = handler.EndReceive(asyncResult);

            if (bytesRead > 0)
            {
                state.sb.Clear().Append(Encoding.ASCII.GetString(
                    state.buffer, 0, bytesRead));

                content = state.sb.ToString();
                received_data_from_Device = content;
                Send(handler, received_data_from_Device);
                if (content.IndexOf("<EOF>") > -1)
                {
                    Console.WriteLine("Read {0} bytes from socket. \n Data : {1}",
                        content.Length, content);
                }
                else
                {
                    handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), state);
                }
            }
        }

        private static void Send(Socket handler, String data)
        {
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            handler.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), handler);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                Socket handler = (Socket)ar.AsyncState;

                int bytesSent = handler.EndSend(ar);



            }
            catch (Exception e)
            {
                Logging.Write_Exceptions(e);
            }
        }

        public static void Close_Socket()
        {
            //handler.Shutdown(SocketShutdown.Both);
            //handler.Close();
        }

    }
}

