using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Test_bench_AGNE.Services
{
    public class HttpServer
    {

        public static string url = "http://localhost:1894/";
        public static int pageViews = 0;
        public static bool acception_Ended = true;
        public static int requestCount = 0;
        public static bool runServer = true;
        public Form1 _form;
        public static string pageData =

            "<!DOCTYPE>" +
            "<html>" +
            "  <head>" +
            "    <title>HttpListener Example</title>" +
            "  </head>" +
            "  <body>" +
            "    <p>Page Views: {0}</p>" +
            "    <form method=\"post\" action=\"shutdown\">" +
            "      <input type=\"submit\" value=\"Shutdown\" {1}>" +
            "    </form>" +
            "  </body>" +
            "</html>";

        public HttpServer(Form1 form)
        {
            _form = form;
        }


        public async Task HandleIncomingConnections(HttpListener listener)
        {

            while (runServer)
            {
                HttpListenerContext ctx = await listener.GetContextAsync();

                HttpListenerRequest request = ctx.Request;
                HttpListenerResponse response = ctx.Response;

                Logging.Write_HttpServer_Communication("Request #: " + ++requestCount);
                Logging.Write_HttpServer_Communication(request.Url.ToString());
                Logging.Write_HttpServer_Communication(request.HttpMethod);
                Logging.Write_HttpServer_Communication(request.UserHostName);
                Logging.Write_HttpServer_Communication(request.UserAgent);
                Logging.Write_HttpServer_Communication(" ");
                Logging.Write_HttpServer_Communication(request.HttpMethod + ":" + request.Url.ToString());

                if ((request.HttpMethod == "POST"))
                {
                    response = Handle_POST_Request(request, response);

                }
                else if (request.HttpMethod == "PUT")
                {
                    response = Handle_PUT_Request(request, response);
                }
                if (request.HttpMethod == "GET")
                {

                    response = Handle_GET_Request(request, response);
                }
                else
                {
                    // Make sure we don't increment the page views counter if `favicon.ico` is requested
                    if (request.Url.AbsolutePath != "/favicon.ico")
                        pageViews += 1;
                    // Write the response info
                    string disableSubmit = !runServer ? "disabled" : "";
                    byte[] data = Encoding.UTF8.GetBytes(String.Format(pageData, pageViews, disableSubmit));
                    response.ContentType = "text/html";
                    response.ContentEncoding = Encoding.UTF8;
                    response.ContentLength64 = data.LongLength;

                    // Write out to the response stream (asynchronously), then close it
                    await response.OutputStream.WriteAsync(data, 0, data.Length);
                }
                response.Close();
            }
        }
        private HttpListenerResponse Handle_PUT_Request(HttpListenerRequest request, HttpListenerResponse response)
        {
            Stream dataStream = request.InputStream;
            HttpListenerResponse tempResponse = response;
            try
            {
                int bytesRead;
                var contents = new List<byte>();
                var buffer = new byte[2048];
                while ((bytesRead = dataStream.Read(buffer)) != 0)
                {
                    contents.AddRange(buffer.Take(bytesRead));
                }
                var str = Encoding.Default.GetString(contents.ToArray());
                Logging.Write_HttpServer_Communication(str);
                tempResponse.StatusCode = 200;
            }
            catch
            {
                tempResponse.StatusCode = 404;
            }
            
            return tempResponse;
        }
        private HttpListenerResponse Handle_GET_Request(HttpListenerRequest request, HttpListenerResponse response)
        {
            HttpListenerResponse tempResponse = response;

            var data = request.Url.AbsolutePath.Split("/");

            switch (data[0])
            {
                case "autotest":
                    {
                        try
                        {
                            switch (data[1])
                            {
                                case "config":
                                    {
                                        var rshit = Form1.device_Parameters;
                                        break;
                                    }
                                case "results":
                                    {
                                        try
                                        {

                                        }
                                        catch
                                        {

                                        }
                                        break;
                                    }
                                default:
                                    {
                                        tempResponse.StatusCode = 400;
                                        break;
                                    }
                            }
                        }
                        catch
                        {
                            tempResponse.StatusCode = 400;
                        }

                        break;
                    }
                default:
                    {
                        tempResponse.StatusCode = 400;
                        break;
                    }
            }

            try
            {
                byte[] image_bytes = File.ReadAllBytes(AppDomain.CurrentDomain.BaseDirectory + request.Url.AbsolutePath.Trim('/'));


                tempResponse.ContentType = "text/plain";
                tempResponse.ContentLength64 = image_bytes.Length;

                Stream OutputStream = tempResponse.OutputStream;

                OutputStream.Write(image_bytes, 0, image_bytes.Length);
                tempResponse.StatusCode = 200;
                OutputStream.Close();
                Logging.Write_HttpServer_Communication(request.Url.AbsolutePath + " file sent");

            }
            catch (FileNotFoundException)
            {
                Logging.Write_HttpServer_Communication("File not found: " + request.Url.AbsolutePath);
                tempResponse.StatusCode = 404;
            }
            catch { }

            return response;
        }
        private HttpListenerResponse Handle_POST_Request(HttpListenerRequest request, HttpListenerResponse response)
        {
            HttpListenerResponse tempResponse = response;


            var data = request.Url.AbsolutePath.Split("/");

            switch (data[0])
            {
                case "autotest":
                    {
                        try
                        {
                            switch (data[1])
                            {
                                case "shutdown":
                                    {
                                        try
                                        {
                                            Console.WriteLine("Shutdown requested");
                                            runServer = false;
                                        }
                                        catch
                                        {
                                            tempResponse.StatusCode = 404;
                                        }
                                        break;
                                    }
                                case "config":
                                    {

                                        break;
                                    }
                                case "start":
                                    {

                                        break;
                                    }
                                default:
                                    {
                                        tempResponse.StatusCode = 400;
                                        break;
                                    }
                            }
                        }
                        catch
                        {
                            tempResponse.StatusCode = 400;
                        }
                        break;
                    }
                default:
                    {
                        tempResponse.StatusCode = 400;
                        break;
                    }
            }

            return tempResponse;
        }

        public async void Start()
        {
            HttpListener listener;

            listener = new HttpListener();
            listener.Prefixes.Add(url);
            listener.Start();
            Console.WriteLine("Listening for connections on {0}", url);

            await HandleIncomingConnections(listener);

            listener.Close();
        }
    }
}
