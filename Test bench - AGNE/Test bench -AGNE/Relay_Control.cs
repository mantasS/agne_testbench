using System.IO.Ports;
using Test_bench_AGNE.Services;

namespace Test_bench_AGNE
{
    public class Relay_Control
    {
        public Modbus _modbus;
        public SerialPort _port;
        public Main_Relay_Control _mainRelay;
        public Device_Relay_Control _deviceRelay;
        public Relay_Control(Modbus modbus, SerialPort port)
        {
            _port = port;
            _modbus = modbus;
            _mainRelay = new Main_Relay_Control(modbus, port);
            _deviceRelay = new Device_Relay_Control(modbus, port);
        }
    }
}
