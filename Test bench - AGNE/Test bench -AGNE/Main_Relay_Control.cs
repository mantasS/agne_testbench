using System.IO.Ports;
using Test_bench_AGNE.Services.Wialon.Models;
using System.Threading;
using Test_bench_AGNE.Services;

namespace Test_bench_AGNE
{
    public class Main_Relay_Control
    {
        private Modbus _modbus;
        private SerialPort _port;
        public Main_Relay_Control(Modbus modbus, SerialPort port)
        {
            _port = port;
            _modbus = modbus;
        }

        public bool Connect_IN2_ADC4_ADCmeaseure()
        {
            return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.IN2_ADC4);
        }

        public bool Connect_IN3_OUT1_As_Output()
        {
            return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.IN3_OUT1);
        }

        public bool Connect_IN3_OUT1_To_ADCmeasure()
        {
            return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.IN3_OUT1);
        }

        public bool Connect_IN4_ADC3_To_ADCmeaseure()
        {
            return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.IN4_ADC3);
        }

        public bool Connect_IN5_ADC5_To_ADCmeaseure()
        {
            return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.IN5_ADC5);
        }

        public bool Connect_LLSTo_RS485()
        {
            if (_modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.RS485A))
            {
                Thread.Sleep(50);
                return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.RS485B);
            }
            return false;
        }

        public bool Connect_LLS_To_RS232()
        {
            if (_modbus.Turn_On_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.RS232_TX))
            {
                Thread.Sleep(50);
                return _modbus.Turn_On_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.RS232_RX);
            }
            return false;
        }

        public bool Connect_LLS_To_RS232(byte Time)
        {
            if (_modbus.Turn_On_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.RS232_TX, Time))
            {
                Thread.Sleep(50);
                return _modbus.Turn_On_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.RS232_RX, Time);
            }
            return false;
        }

        public bool Connect_RS232_To_COMPort()
        {
            if (_modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.RS232_TX))
            {
                Thread.Sleep(50);
                return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.RS232_RX);
            }
            return false;
        }

        public bool Connect_CAN2_To_CAN1()
        {
            if (_modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.CAN2H_Relay1))
            {
                if (_modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.CAN1H_Relay))
                {
                    if (_modbus.Turn_Off_Relay(_port, Main_Control_Relays.Second_main_Control_Board_Slave_ID, Main_Control_Relays.CAN1_OUT4))
                    {
                        if (_modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.CAN1L_Relay))
                        {
                            if (_modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.CAN2H_Relay2))
                            {
                                Thread.Sleep(50);
                                return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.CAN2L_Relay1);
                            }
                        }
                    }
                }
            }
            return false;
        }

        public bool Connect_CAN2_To_Tacho()
        {
            if (Connect_CAN2_Relay1_To_CAN2_Relay2())
            {
                Thread.Sleep(50);
                return Connect_CAN2_Relay2_To_Tacho();

            }
            return false;
        }

        public bool Connect_CAN2_As_Out3()
        {
            if (Connect_CAN2_Relay1_To_CAN2_Relay2())
            {
                Thread.Sleep(50);
                return Connect_CAN2_Relay2_As_OUT3();

            }
            return false;
        }

        public bool Connect_CAN1_As_Out1()
        {
            return _modbus.Turn_On_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.CAN1L_Relay);
        }

        public bool Connect_CAN1_As_Out4()
        {
            if (_modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.CAN1H_Relay))
            {
                return _modbus.Turn_On_Relay(_port, Main_Control_Relays.Second_main_Control_Board_Slave_ID, Main_Control_Relays.CAN1_OUT4);
            }
            return false;

        }

        public bool Connect_CAN1_As_ADC5()
        {
            if(_modbus.Turn_Off_Relay(_port, Main_Control_Relays.Second_main_Control_Board_Slave_ID, Main_Control_Relays.CAN1_OUT4))
            {
                return (_modbus.Turn_On_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.CAN1H_Relay));
            }
            return false;
        }

        public bool Connect_One_Wire_To_Ibutton()
        {
            if (Reset_One_Wire_Relays())
            {
                if (Connect_One_Wire_Relay1_To_Relay3())
                {
                    return Connect_One_Wire_Relay3_To_Ibutton();
                }
            }
            return false;
        }

        public bool Connect_One_Wire_To_Temp_Sensor()
        {
            if (Reset_One_Wire_Relays())
            {
                if (Connect_One_Wire_Relay1_To_Relay3())
                {
                    if (Connect_One_Wire_Relay3_To_Relay4())
                    {
                        return Connect_One_Wire_Relay4_To_Temp_sensor();
                    }
                }
            }
            return false;
        }

        public bool Connect_One_Wire_To_ADC_Inputs()
        {
            if (Reset_One_Wire_Relays())
            {
                if (Connect_One_Wire_Relay1_To_Relay3())
                {
                    if (Connect_One_Wire_Relay3_To_Relay4())
                    {
                        return Connect_One_Wire_Relay4_To_ADCinputs();
                    }
                }
            }
            return false;
        }

        public bool Connect_One_Wire_To_D8()
        {
            if (Reset_One_Wire_Relays())
            {
                if (Connect_One_Wire_Relay1_To_Relay2())
                {
                    return Connect_One_Wire_Relay2_To_D8();
                }
            }
            return false;
        }

        public bool Connect_One_Wire_To_BleExtender()
        {
            if (Reset_One_Wire_Relays())
            {
                if (Connect_One_Wire_Relay1_To_Relay2())
                {
                    return Connect_One_Wire_Relay2_To_BleExtender();
                }
            }
            return false;
        }

        public bool Connect_Secondary_Power_Supply()
        {
            return _modbus.Turn_On_Relay(_port, Main_Control_Relays.Second_main_Control_Board_Slave_ID, Main_Control_Relays.Secondary_Power_Supply);
        }

        public bool Disconnect_Secondary_Power_Supply()
        {
            return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.Second_main_Control_Board_Slave_ID, Main_Control_Relays.Secondary_Power_Supply);
        }


        private bool Connect_CAN2_Relay2_To_Tacho()
        {
            if (_modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.CAN2H_Relay2))
            {
                Thread.Sleep(50);
                return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.CAN2L_Relay2);
            }
            return false;
        }

        private bool Connect_CAN2_Relay2_As_OUT3()
        {
            return _modbus.Turn_On_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.CAN2H_Relay2);
        }

        private bool Connect_CAN2_Relay1_To_CAN2_Relay2()
        {
            if (_modbus.Turn_On_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.CAN2H_Relay1))
            {
                Thread.Sleep(50);
                return _modbus.Turn_On_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.CAN2L_Relay1);
            }
            return false;
        }

        private bool Connect_One_Wire_Relay1_To_Relay2()
        {
            return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.One_wire_Relay1);
        }

        private bool Connect_One_Wire_Relay1_To_Relay3()
        {
            return _modbus.Turn_On_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.One_wire_Relay1);
        }

        private bool Connect_One_Wire_Relay2_To_D8()
        {
            return _modbus.Turn_On_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.One_wire_Relay2);
        }

        private bool Connect_One_Wire_Relay2_To_BleExtender()
        {
            return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.One_wire_Relay2);
        }

        private bool Connect_One_Wire_Relay3_To_Relay4()
        {
            return _modbus.Turn_On_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.One_wire_Relay3);
        }

        private bool Connect_One_Wire_Relay4_To_Temp_sensor()
        {
            return _modbus.Turn_On_Relay(_port, Main_Control_Relays.Second_main_Control_Board_Slave_ID, Main_Control_Relays.One_wire_Relay4);
        }
        private bool Connect_One_Wire_Relay4_To_ADCinputs()
        {
            return _modbus.Turn_On_Relay(_port, Main_Control_Relays.Second_main_Control_Board_Slave_ID, Main_Control_Relays.One_wire_Relay4);
        }

        public bool Reset_One_Wire_Relays()
        {
            if (_modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.One_wire_Relay1))
            {
                if (_modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.One_wire_Relay2))
                {
                    if (_modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.One_wire_Relay3))
                    {
                        return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.Second_main_Control_Board_Slave_ID, Main_Control_Relays.One_wire_Relay4);
                    }
                }
            }
            return false;
        }

        private bool Connect_One_Wire_Relay3_To_Ibutton()
        {
            return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.One_wire_Relay3);
        }

        public bool Turn_On_USB_Relay()
        {
            return _modbus.Turn_On_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Device_Control_Relays.USB_Relay);
        }
        public bool Turn_Off_USB_Relay()
        {
            return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Device_Control_Relays.USB_Relay);
        }
        
        public bool Dissconnect_IN5ADC5()
        {
            return _modbus.Turn_On_Relay(_port, Main_Control_Relays.Second_main_Control_Board_Slave_ID, Main_Control_Relays.IN5_ADC5);
        }

        public bool Connect_IN5ADC5()
        {
            return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.Second_main_Control_Board_Slave_ID, Main_Control_Relays.IN5_ADC5);
        }

        public bool Dissconnect_IN4ADC3()
        {
            return _modbus.Turn_On_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.IN4_ADC3);
        }

        public bool Connect_IN4ADC3()
        {
            return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.IN4_ADC3);
        }

        public bool Dissconnect_IN3OUT1()
        {
            return _modbus.Turn_On_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.IN3_OUT1);
        }

        public bool Connect_IN3OUT1()
        {
            return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.IN3_OUT1);
        }

        public bool Dissconnect_IN2ADC4()
        {
            return _modbus.Turn_On_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.IN2_ADC4);
        }

        public bool Connect_IN2ADC4()
        {
            return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.IN2_ADC4);
        }

        public bool Dissconnect_Input(byte relay, byte board)
        {
            return _modbus.Turn_On_Relay(_port, board, relay);
        }

        public bool Connect_Input(byte relay, byte board)
        {
            return _modbus.Turn_Off_Relay(_port, board, relay);
        }

        public bool Dissconnect_All_Inputs()
        {
            if (Dissconnect_IN2ADC4())
            {
                if (Dissconnect_IN3OUT1())
                {
                    if (Dissconnect_IN4ADC3())
                    {
                        return Dissconnect_IN5ADC5();
                    }
                }
            }
            return false;
        }

        public bool Connect_All_Inputs()
        {
            if (Connect_IN2ADC4())
            {
                if (Connect_IN3OUT1())
                {
                    if (Connect_IN4ADC3())
                    {
                        return Connect_IN5ADC5();
                    }
                }
            }
            return false;
        }
    }
}
