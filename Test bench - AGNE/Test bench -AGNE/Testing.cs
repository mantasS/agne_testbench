using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Test_bench_AGNE.Models.Device;
using Test_bench_AGNE.Models.Global;
using Test_bench_AGNE.Services;
using Test_bench_AGNE.Services.Wialon.Models;
using Test_bench_AGNE.Services.Wialon.Models.XDMData;
using Test_bench_AGNE.Services.XDM;

namespace Test_bench_AGNE
{
    public class Testing
    {
        private XDM _xdm;
        private Relay_Control _relayControl;
        private Modbus _modbus;
        public Device_Parameters _deviceParameters;
        public Test_Results testsResults = new();
        Stopwatch watch = new();


        public bool stopTest = false;
        public bool testStopped = true;

        private static readonly byte minute = 60;
        private static readonly int minuteInMili = 60000;

        public Testing ()
        {
            _xdm = new XDM();

            _modbus = new Modbus();
            _relayControl = new Relay_Control(_modbus, Form1.serialParser.modbusCommPort);
        }

        public async Task Start_Tests(List<Device_Parameters> devices, string testFirmwareName)
        {
            testStopped = false;
            Reset_Control_Relays();
            try
            {
                foreach (var device in devices)
                {
                    Turn_Off_All_Device_Relays(device.Relay_ID);
                    Thread.Sleep(10);
                }

                //while (true)
                //{
                    foreach (var device in devices)
                    {
                        _deviceParameters = device;

                        Logging.Logs_WriteLine("\r\nCurrently tested device IMEI: " + device.IMEI + ", HardwareVersion: " + device.Hardware_Version + ", RelayID: " + device.Relay_ID);
                        if (Turn_On_All_Device_Relays(device.Relay_ID))
                        {
                            if (await Get_Device_Ready_For_Test(device, device.Hardware_Version + "_" + testFirmwareName, false) && !stopTest)
                            {
                                //await Main_Test();
                                //await Sleep_Test();
                                await Test_CAN1();
                                await Test_CAN2();
                                await Test_ADC2();
                                await Test_ADC3();
                                await Test_ADC4();
                                await Test_ADC5();
                                await Test_IN1_Motion();
                                await Test_IN2();
                                await Test_IN3();
                                await Test_IN4();
                                await Test_IN5();
                                await Test_IN6();
                                await Test_IN7();
                                await Test_IN8();
                                await Test_OUT1();
                                await Test_OUT2();
                                await Test_OUT3();
                                await Test_OUT4();
                                await Test_OUT12V();
                                await Test_RS232();
                                await Test_RS485();
                                await Test_SCRIPT();
                                Thread.Sleep(1000);
                            }
                        }
                        else
                        {
                            Logging.Logs_WriteLine("Failed to turn on device relay, check Relay block address: " + device.Relay_ID);
                        }

                        Turn_Off_All_Device_Relays(device.Relay_ID);
                        if (stopTest)
                        {
                            testStopped = true;
                            Logging.Logs_WriteLine("\r\nTest Stopped\r\n");
                            return;
                        }
                    }
                //}
                
            }
            catch (ThreadInterruptedException ex)
            {
                Logging.Write_Exceptions(ex);
                testStopped = true;
                return;
            }
            testStopped = true;
        }

        private bool Reset_Control_Relays()
        {
            if( _modbus.Turn_Off_All_Relays(Form1.serialParser.modbusCommPort, Main_Control_Relays.First_main_Control_Board_Slave_ID))
            {
                return _modbus.Turn_Off_All_Relays(Form1.serialParser.modbusCommPort, Main_Control_Relays.Second_main_Control_Board_Slave_ID);
            }
            return false;

        }

        private bool Restart_Device(Device_Parameters device)
        {
            if(Turn_Off_All_Device_Relays(device.Relay_ID))
            {
                Thread.Sleep(100);
                return Turn_On_All_Device_Relays(device.Relay_ID);
            }
            return false;
        }

        private bool Turn_On_All_Device_Relays(byte deviceRelayId)
        {
            return _modbus.Turn_On_All_Relays(Form1.serialParser.modbusCommPort, deviceRelayId);
        }

        private bool Turn_Off_All_Device_Relays(byte deviceRelayId)
        {
            return _modbus.Turn_Off_All_Relays(Form1.serialParser.modbusCommPort, deviceRelayId);
        }

        public async Task Main_Test()
        {
            watch.Start();

            if (await Test_Entry(Maintest_GlobalVariables.mainTestName, Maintest_GlobalVariables.mainTestName, false))
            {
                Wait_For_Answer(Maintest_GlobalVariables.mainTestName + ":DONE", Maintest_GlobalVariables.timeout * minuteInMili);
            }

            watch.Stop();
            Thread.Sleep(1000);
            Logging.Logs_WriteLine("Time elapsed for main test: " + watch.ElapsedMilliseconds/1000 + " seconds");
        }

        public async Task Sleep_Test()
        {
            if (stopTest)
                return;

            int result = 0;
            watch.Start();

            result += await Sleep_Test_Inputs();
            result += await Sleep_Test_SMS();
            result += await Sleep_Test_Timeout();

            watch.Stop();
            
            _relayControl._mainRelay.Connect_All_Inputs();

            Form1.serialParser.deviceResponse = null;
            Thread.Sleep(500);
            Logging.Logs_WriteLine("Time elapsed for sleep test: " + watch.ElapsedMilliseconds + "mS");

            if (result > 0)
            {
                Logging.Logs_WriteLines("Sleep test - FAILED\r\n");
            }
            else
            {
                Logging.Logs_WriteLines("Sleep test - PASSED\r\n");
            }
        }

        public async Task<int> Sleep_Test_Inputs()
        {
            int result = 0;

            if (await Test_Entry(Sleeptest_GlobalVariables.SleepTestNameInputs, Sleeptest_GlobalVariables.SleepTestNameInputs, false))
            {
                _relayControl._mainRelay.Dissconnect_All_Inputs();
                result += Test_wakeUp_from_IN5();
                result += Test_wakeUp_from_IN4();
                result += Test_wakeUp_from_IN3();
                result += Test_wakeUp_from_IN2();
            }

            return result;
        }

        public int Test_wakeUp_from_IN5()
        {
            Logging.Logs_WriteLines("Testing wakeup from IN5\r\n");
            if (Test_wakeUp_from_input(Main_Control_Relays.Second_main_Control_Board_Slave_ID, Main_Control_Relays.IN5_ADC5, Sleeptest_GlobalVariables.timeoutUntillSleep, Sleeptest_GlobalVariables.SleepTestNameInputs) == 0)
            {
                Logging.Logs_WriteLines("Device wake up from IN5 - SUCCESS\r\n");
                return 0;
            }
             
            return 1;
        }

        public int Test_wakeUp_from_IN4()
        {
            Logging.Logs_WriteLines("Testing wakeup from IN4\r\n");
            if (Test_wakeUp_from_input(Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.IN4_ADC3, Sleeptest_GlobalVariables.timeoutUntillSleep, Sleeptest_GlobalVariables.SleepTestNameInputs) == 0)
            {
                Logging.Logs_WriteLines("Device wake up from IN4 - SUCCESS\r\n");
                return 0;
            }

            return 1;
        }

        public int Test_wakeUp_from_IN3()
        {
            Logging.Logs_WriteLines("Testing wakeup from IN3\r\n");
            if (Test_wakeUp_from_input(Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.IN3_OUT1, Sleeptest_GlobalVariables.timeoutUntillSleep, Sleeptest_GlobalVariables.SleepTestNameInputs) == 0)
            {
                Logging.Logs_WriteLines("Device wake up from IN3 - SUCCESS\r\n");
                return 0;
            }

            return 1;
        }

        public int Test_wakeUp_from_IN2()
        {
            Logging.Logs_WriteLines("Testing wakeup from IN2\r\n");
            if (Test_wakeUp_from_input(Main_Control_Relays.First_main_Control_Board_Slave_ID, Main_Control_Relays.IN2_ADC4, Sleeptest_GlobalVariables.timeoutUntillSleep, Sleeptest_GlobalVariables.SleepTestNameInputs) == 0)
            {
                Logging.Logs_WriteLines("Device wake up from IN2 - SUCCESS\r\n");
                return 0;
            }

            return 1;
        }

        public async Task<int> Sleep_Test_SMS()
        {
            Logging.Logs_WriteLine("Testing wakeup from SMS");
            if (await Test_Entry(Sleeptest_GlobalVariables.SleepTestNameSMS, Sleeptest_GlobalVariables.SleepTestNameSMS, false))
            {
                return await Test_wakeUp_from_SMS(Sleeptest_GlobalVariables.timeoutUntillSleep, Sleeptest_GlobalVariables.SleepTestNameSMS);
            }

            return 1;
        }

        public async Task<int> Sleep_Test_Timeout()
        {
            Logging.Logs_WriteLine("Testing wakeup from Sleep Timeout");
            if (await Test_Entry(Sleeptest_GlobalVariables.SleepTestNameSMS, Sleeptest_GlobalVariables.SleepTestNameSMS, false))
            {
                return Test_wakeUp_from_SleepTimeout(Sleeptest_GlobalVariables.timeoutUntillSleep, Sleeptest_GlobalVariables.SleepTestNameSMS);
            }

            return 1;
        }

        public int Test_wakeUp_from_input(byte controlBoard, byte input, int timeoutTillSleep, string sleepTestName)
        {
            int deviceSystemTicksFromReset = 0;
            if(Check_If_Device_Is_Awake(sleepTestName, Sleeptest_GlobalVariables.deviceRunningMessage, sleepTestName + ":test", null, 120000))
            {
                int timeToSleep = Wait_Until_Sleep(timeoutTillSleep, ref deviceSystemTicksFromReset);

                if (timeToSleep > 0)
                {
                    if (deviceSystemTicksFromReset > 0)
                    {
                        Measure_Sleep_Current();
                        Thread.Sleep(25000);
                        if (_relayControl._mainRelay.Connect_Input(input, controlBoard))
                        {
                            if (Wait_Until_WakeUp(sleepTestName, Sleeptest_GlobalVariables.wakeupTimeout * 1000, Sleeptest_GlobalVariables.deviceRunningMessage, deviceSystemTicksFromReset))
                            {
                                _relayControl._mainRelay.Dissconnect_Input(input, controlBoard);
                                return 0;
                            }
                            else
                            {
                                Logging.Logs_WriteLine("Device didn't wake UP - ERROR");
                            }
                        }
                        else
                        {
                            Logging.Logs_WriteLines("Failed at controling modbus main relay");
                        }
                    }
                }
                else
                {
                    Logging.Logs_WriteLines("Device didn't go to sleep - ERROR\r\n");
                }
            }

            _relayControl._mainRelay.Dissconnect_Input(input, controlBoard);
            return 1;
        }

        public async Task<int> Test_wakeUp_from_SMS(int timeoutTillSleep, string sleepTestName)
        {
            int deviceSystemTicksFromReset = 0;

            int timeToSleep = Wait_Until_Sleep(timeoutTillSleep, ref deviceSystemTicksFromReset);

            _relayControl._mainRelay.Dissconnect_Input(Main_Control_Relays.IN5_ADC5, Main_Control_Relays.Second_main_Control_Board_Slave_ID);

            if (timeToSleep > 0)
            {
                if (deviceSystemTicksFromReset > 0)
                {
                    Measure_Sleep_Current();
                    Thread.Sleep(5000);
                    if (await _xdm.Send_SMS(_deviceParameters.IMEI.ToString(), "wakeUp", true))
                    {
                        if (Wait_Until_WakeUp(sleepTestName, 60 * 1000, Sleeptest_GlobalVariables.deviceRunningMessage, deviceSystemTicksFromReset))
                        {
                            Logging.Logs_WriteLine("Device failed to wake UP - SUCCESS");
                            return 0;
                        }
                        else
                        {
                            Logging.Logs_WriteLine("Device failed to wake UP - ERROR");
                        }
                    }
                    else
                    {
                        Logging.Logs_WriteLine("Failed sms send command in XDM");
                    }
                }
            }
            else
            {
                Logging.Logs_WriteLine("Device didn't go to sleep - ERROR\r\n");
            }

            _relayControl._mainRelay.Connect_Input(Main_Control_Relays.IN5_ADC5, Main_Control_Relays.Second_main_Control_Board_Slave_ID);
            return 1;
        }

        public int Test_wakeUp_from_SleepTimeout(int timeoutTillSleep, string sleepTestName)
        {
            int deviceSystemTicksFromReset = 0;

            int timeToSleep = Wait_Until_Sleep(timeoutTillSleep, ref deviceSystemTicksFromReset);

            _relayControl._mainRelay.Dissconnect_Input(Main_Control_Relays.IN5_ADC5, Main_Control_Relays.Second_main_Control_Board_Slave_ID);

            if (timeToSleep > 0)
            {
                if (deviceSystemTicksFromReset > 0)
                {
                    Measure_Sleep_Current();
                    Thread.Sleep(5000);

                    Logging.Logs_WriteLine("Waiting untill wakeup from sleep timeout");

                    if (Wait_Until_WakeUp(sleepTestName, Sleeptest_GlobalVariables.sleepTimeout * 1000, Sleeptest_GlobalVariables.deviceRunningMessage, deviceSystemTicksFromReset))
                    {
                        Logging.Logs_WriteLine("Device failed to wake UP - SUCCESS");
                        return 0;
                    }
                    else
                    {
                        Logging.Logs_WriteLine("Device failed to wake UP - ERROR");
                    }
                }
            }
            else
            {
                Logging.Logs_WriteLine("Device didn't go to sleep - ERROR\r\n");
            }

            _relayControl._mainRelay.Connect_Input(Main_Control_Relays.IN5_ADC5, Main_Control_Relays.Second_main_Control_Board_Slave_ID);
            return 1;
        }

        public int Wait_Until_Sleep(int timeout, ref int systemTicksFromReset)
        {
            Stopwatch sleepStopwatch = new();
            Stopwatch timeoutStopwatch = new();
            timeoutStopwatch.Start();

            Form1.serialParser.deviceResponse = null;

            while (timeoutStopwatch.ElapsedMilliseconds/1000 < timeout)
            {
                string temp = Form1.serialParser.deviceResponse;

                Thread.Sleep(1100);
                if (temp != Form1.serialParser.deviceResponse && Form1.serialParser.deviceResponse != null && Form1.serialParser.deviceResponse.Length > 3)
                {
                    sleepStopwatch.Stop();
                }
                else
                {
                    if(sleepStopwatch.IsRunning)
                    {
                        if(sleepStopwatch.ElapsedMilliseconds > 4000)
                        {
                            return (int)(timeoutStopwatch.ElapsedMilliseconds/1000);
                        }
                    }
                    else
                    {
                        sleepStopwatch.Start();
                    }
                }

                var tempSysTicks = Parse_SystemTicks();
                if (tempSysTicks > 0)
                {
                    systemTicksFromReset = tempSysTicks;
                }
                if(stopTest)
                {
                    break;
                }
            }
            return -1;
        }

        public bool Wait_Until_WakeUp(string answer, int timeout, string deviceRunningMessage, int systemTicksFromReset)
        {
            if(Wait_For_Answer(answer, timeout))
            {
                Form1.serialParser.deviceCommPort.Write(answer + ":test");

                if (Wait_For_Answer(deviceRunningMessage, timeout, answer, "Device restarted - FAIL"))
                {
                    
                    if (Parse_SystemTicks() > systemTicksFromReset)
                    {
                        return true;
                    }
                    else
                    {
                        Logging.Logs_WriteLine("Device restarded - FAIL!!");
                        return false;
                    }
                }
            }

            return false;
        }

        private bool Check_If_Device_Is_Awake(string message, string message2, string response, string response2, int timeout)
        {
            while (timeout > 0)
            {
                if (Form1.serialParser.deviceResponse.Contains(message))
                {
                    if (!string.IsNullOrWhiteSpace(response))
                    {
                        Form1.serialParser.deviceCommPort.Write(response);
                    }
                    return true;
                }
                if (Form1.serialParser.deviceResponse.Contains(message2))
                {
                    if(!string.IsNullOrWhiteSpace(response2))
                    {
                        Form1.serialParser.deviceCommPort.Write(response2);
                    }
                    return true;
                }
                Thread.Sleep(10);
                timeout -= 10;

                if (stopTest)
                {
                    Logging.Logs_WriteLine("Test stopped");
                    return false;
                }
            }
            return false;
        }


        private int Parse_SystemTicks()
        {
            for (int i = 0; i < 3; i++)
            {
                if(Form1.serialParser.deviceResponse != null)
                {
                    if (Form1.serialParser.deviceResponse.Contains(":"))
                    {
                        var index = Form1.serialParser.deviceResponse.IndexOf(":");
                        if (index > -1)
                        {
                            string systemTicksString = Form1.serialParser.deviceResponse.Substring(index + 2);
                            if (int.TryParse(systemTicksString, out int systemTicks))
                            {
                                return systemTicks;
                            }
                        }
                    }
                }
                Thread.Sleep(1100);
            }

            return -1;
        }

        public bool Measure_Sleep_Current()
        {
            return false;
        }

        

        public async Task<bool> Do_Test(List<Test_Data> testData, string usedHardwareName)
        {
            bool testsPassed = true;
            try
            {
                USB usb = new (Form1.serialParser.modbusCommPort, _relayControl);
                foreach (var test in testData)
                {
                    if (stopTest)
                    {
                        return false;
                    }
                    watch.Start();
                    try
                    {
                        Logging.Logs_WriteLine("\r\n");
                        Logging.Logs_WriteLine(usedHardwareName + ":Current test: " + test.TestName + ";");

                        if (Set_Test_Relays(test.Relays))
                        {

                            if (await Test_Entry(test.ConfigName, test.TestName, false))
                            {
                                Form1.serialParser.TestStartMessage = test.TestName;

                                if (!Wait_For_Answer(test.Result, test.Timeout, test.FailResult, null))
                                {
                                    Logging.Logs_WriteLine("Device didn't return desired result");
                                    testsPassed = false;
                                }
                            }
                        }
                        else
                        {
                            Logging.Logs_WriteLine("Failed setting relays for test");
                        }
                    }
                    catch (Exception ex)
                    {
                        if (test.TestName != null)
                        {
                            Logging.Logs_WriteLine("Failed to start " + test.TestName);
                        }
                        else
                        {
                            Logging.Logs_WriteLine("No TestName");
                        }
                        Logging.Write_Exceptions(ex);
                        testsPassed = false;
                    }


                    watch.Stop();

                    Thread.Sleep(300);
                    Logging.Logs_WriteLine("Time elapsed for test: " + watch.ElapsedMilliseconds + "mS");

                    Form1.serialParser.TestStartMessage = null;
                }

            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
                return false;
            }

            return testsPassed;
        }
        
        private async Task<bool> Get_Device_Ready_For_Test(Device_Parameters device, string testFirmwareName, bool useUSB)
        {

            if (useUSB)
            {
                if (!await Load_Firmware_USB(testFirmwareName))
                {
                    return false;
                }
            }

            if (await _xdm.Check_Hardware_Type(long.Parse(device.IMEI)))
            {
                if(await _xdm.Update_Device_Firmware(device.IMEI, testFirmwareName))
                {
                    if(await Wait_For_Firmware_Update())
                    {
                        Form1.serialParser._deviceParameters = device;
                        return true;
                    }
                    else
                    {
                        Logging.Logs_WriteLine("Firmware didn't update");
                    }
                }
                else
                {
                    Logging.Logs_WriteLine("Failed to change firmware in xdm");
                }
            }
            return false;
        }

        private async Task<bool> Test_Entry(string configName, string testName, bool useUSB)
        {
            Form1.serialParser.deviceStartFlag = false;
            Form1.serialParser.deviceStartString = testName;

            if (useUSB)
            {
                if(!await Load_Config_USB(configName))
                {
                    return false;
                }
            }
            else
            {
                if(await _xdm.Update_Device_Config(_deviceParameters.IMEI.ToString(), configName))
                {
                    if (!await Wait_For_Config_Update())
                    {
                        Logging.Logs_WriteLine("Device config update failed");
                        return false;
                    }
                }
                else
                {
                    Logging.Logs_WriteLine("Device config update failed");
                    return false;
                }
            }

            if (Form1.serialParser.deviceStartFlag)
            {
                Form1.serialParser.deviceStartString = null;
                Form1.serialParser.deviceStartFlag = false;
                Form1.serialParser.deviceCommPort.Write(testName + ":test");
                Logging.Logs_WriteLine(testName + ":test");
                return true;
            }


            if (Restart_Device(_deviceParameters))
            {
                if (Wait_For_Answer(testName, 25000))
                {
                    Form1.serialParser.deviceCommPort.Write(testName+":test");
                    return true;
                }
                else
                {
                    Logging.Logs_WriteLine("Config updated, but device did not start test");
                }
            }

            

            return false;
        }

        private async Task<bool> Load_Config_USB(string configName)
        {
            USB usb = new (Form1.serialParser.modbusCommPort, _relayControl);

            if (!await _xdm.Download_Config(configName, _deviceParameters.IMEI))
            {
                Logging.Logs_WriteLine("Failed to download config");
                return false;
                
            }

            if (!usb.Upload_Config(configName, _xdm.configurationFilesPath, _deviceParameters.Hardware_Version.Substring(0, 1)))
            {
                Logging.Logs_WriteLine("Failed to upload config");
                return false;
            }

            return true;
        }

        private async Task<bool> Load_Firmware_USB(string firmwareName)
        {
            USB usb = new (Form1.serialParser.modbusCommPort, _relayControl);

            if (!File.Exists(_xdm.firmwaresPath + "//" + firmwareName))
            {
                if (!await _xdm.Download_Firmware(firmwareName, _deviceParameters.IMEI))
                {
                    Logging.Logs_WriteLine("Failed to download config");
                    return false;
                }
            }

            if (!usb.Upload_Firmware(firmwareName, _xdm.firmwaresPath, _deviceParameters.Hardware_Version.Substring(0, 1)))
            {
                Logging.Logs_WriteLine("Failed to upload config");
            }

            return true;
        }

        private bool Wait_For_Answer(string answer, int timeout)
        {
            Form1.serialParser.deviceStartString = answer;

            Logging.Logs_WriteLine("Waiting:\"" + answer + "\" for " + timeout + "mS");
            while (timeout > 0)
            {
                if (Form1.serialParser.deviceStartFlag)
                {
                    Form1.serialParser.deviceStartString = null;
                    Form1.serialParser.deviceStartFlag = false;
                    return true;
                }
                Thread.Sleep(10);
                timeout -= 10;

                if (stopTest)
                {
                    Logging.Logs_WriteLine("Test stopped");
                    return false;
                }
            }
            return false;
        }
        
        private bool Wait_For_Answer(string answer, int timeout, string unwantedAnswer, string responseIfUnwanted)
        {
            Form1.serialParser.deviceStartString = answer;
            Form1.serialParser.deviceResponse = null;
            Logging.Logs_WriteLine("Waiting:\"" + answer + "\" for " + timeout + "mS");
            while (timeout > 0)
            {
                if (Form1.serialParser.deviceStartFlag)
                {
                    Form1.serialParser.deviceStartString = null;
                    Form1.serialParser.deviceStartFlag = false;
                    return true;
                }
                Thread.Sleep(10);
                timeout -= 10;

                if(Form1.serialParser.deviceResponse == unwantedAnswer)
                {
                    if(!string.IsNullOrWhiteSpace(responseIfUnwanted))
                    {
                        Logging.Logs_WriteLine(responseIfUnwanted);
                    }
                    return false;
                }

                if (stopTest)
                {
                    Logging.Logs_WriteLine("Test stopped");
                    return false;
                }
            }
            return false;
        }

        public bool Set_Test_Relays(string relays)
        {
            byte relay_id = 1;
            byte relay_module_id = Main_Control_Relays.First_main_Control_Board_Slave_ID;
            if (relays != null)
            {
                foreach (var relay in relays)
                {
                    if (relay == '0')
                    {
                        if (!_modbus.Turn_Off_Relay(Form1.serialParser.modbusCommPort, relay_module_id, relay_id))
                        {
                            return false;
                        }
                    }
                    else if (relay == '1')
                    {
                        if (!_modbus.Turn_On_Relay(Form1.serialParser.modbusCommPort, relay_module_id, relay_id))
                        {
                            return false;
                        }
                    }
                    relay_id++;
                    if (relay_id > 16)
                    {
                        if (relay_module_id == Main_Control_Relays.Second_main_Control_Board_Slave_ID)
                        {
                            Logging.Write_Exceptions("More than 32 relays in Tests>Test_Data>Relays");
                            return true;
                        }
                        relay_module_id = Main_Control_Relays.Second_main_Control_Board_Slave_ID;
                        relay_id = 1;

                    }
                }
            }
            return true;
        }

        public async Task<bool> Wait_For_Firmware_Update()
        {
            Thread.Sleep(1000);
            XDMData fwUpdateStatus;

            Stopwatch timeoutWatch = new();
            timeoutWatch.Start();

            int timeInPending = 0;
            int timeInInstalling = 0;
            var timeout = 15 * minuteInMili;

            while (timeoutWatch.ElapsedMilliseconds < timeout)
            {
                fwUpdateStatus = await _xdm.Get_SdkDevice_Information_ByIMEI(_deviceParameters.IMEI);

                switch(fwUpdateStatus.results[0].information.firmwareUpdate.firmwareUpdateState)
                {
                    case "Pending":
                        {
                            timeInPending++;
                            if (timeInPending > 3 * minute)
                            {
                                Logging.Logs_WriteLine("Device Firmware status is stuck in Pending state");
                                timeoutWatch.Stop();
                                return false;
                            }
                            break;
                        }
                    case "Finished":
                        {
                            Logging.Logs_WriteLine("Device Firmware update finished");
                            timeoutWatch.Stop();
                            return true;
                        }
                    case "Installing":
                        {
                            timeInInstalling++;
                            if(timeInInstalling > 4 * minute)
                            {
                                Logging.Logs_WriteLine("Device Firmware status is stuck in Installing state");
                                timeoutWatch.Stop();
                                return false;
                            }
                            break;
                        }
                }

                if(stopTest)
                {
                    Logging.Logs_WriteLine("Firmware update quit - test stopped");////////////////////////// add update cancel
                    return false;
                }

                Thread.Sleep(1000);
            }

            timeoutWatch.Stop();
            return false;
        }

        public async Task<bool> Wait_For_Config_Update()
        {
            Thread.Sleep(2000);
            XDMData configUpdateStatus;

            Stopwatch timeoutWatch = new();
            timeoutWatch.Start();

            int timeInPending = 0;
            int timeInInstalling = 0;
            var timeout = 6 * minuteInMili;

            while (timeoutWatch.ElapsedMilliseconds < timeout)
            {
                configUpdateStatus = await _xdm.Get_SdkDevice_Information_ByIMEI(_deviceParameters.IMEI);
                if (configUpdateStatus.results != null)
                {
                    switch (configUpdateStatus.results[0].information.configurationUpdate.configUpdateState)
                    {
                        case "Pending":
                            {
                                timeInPending++;
                                if (timeInPending > 3 * minute)
                                {
                                    Logging.Logs_WriteLine("Device Config status is stuck in Pending state");
                                    timeoutWatch.Stop();
                                    return false;
                                }
                                break;
                            }
                        case "Finished":
                            {
                                Logging.Logs_WriteLine("Device Config update finished");
                                timeoutWatch.Stop();
                                return true;
                            }
                        case "Installing":
                            {
                                timeInInstalling++;
                                if (timeInInstalling > 3 * minute)
                                {
                                    Logging.Logs_WriteLine("Device Config status is stuck in Installing state");
                                    timeoutWatch.Stop();
                                    return false;
                                }
                                break;
                            }
                    }
                }

                if(Form1.serialParser.deviceStartFlag)
                {
                    timeoutWatch.Stop();
                    return true;
                }

                if (stopTest)
                {
                    Logging.Logs_WriteLine("Config update quit - test stopped");////////////////////////// add update cancel
                    return false;
                }

                Thread.Sleep(1000);
            }

            timeoutWatch.Stop();
            return false;
        }
        


        public async Task Test_CAN1()
        {
            if(await Do_Test(_deviceParameters.Tests.CAN1, "CAN1"))
            {
                testsResults.CAN1 = "FAILED";
            }
        }

        public async Task Test_CAN2()
        {
            await Do_Test(_deviceParameters.Tests.CAN2, "CAN2");
        }

        public async Task Test_ADC2()
        {
            await Do_Test(_deviceParameters.Tests.ADC2, "ADC2");
        }

        public async Task Test_ADC3()
        {
            await Do_Test(_deviceParameters.Tests.ADC3, "ADC3");
        }

        public async Task Test_ADC4()
        {
            await Do_Test(_deviceParameters.Tests.ADC4, "ADC4");
        }

        public async Task Test_ADC5()
        {
            await Do_Test(_deviceParameters.Tests.ADC5, "ADC5");
        }

        public async Task Test_IN1_Motion()
        {
            await Do_Test(_deviceParameters.Tests.IN1_Motion, "IN1_Motion");
        }

        public async Task Test_IN2()
        {
            await Do_Test(_deviceParameters.Tests.IN2, "IN2");
        }

        public async Task Test_IN3()
        {
            await Do_Test(_deviceParameters.Tests.IN3, "IN3");
        }

        public async Task Test_IN4()
        {
            await Do_Test(_deviceParameters.Tests.IN4, "IN4");
        }

        public async Task Test_IN5()
        {
            await Do_Test(_deviceParameters.Tests.IN5, "IN5");
        }

        public async Task Test_IN6()
        {
            await Do_Test(_deviceParameters.Tests.IN6, "IN6");
        }

        public async Task Test_IN7()
        {
            await Do_Test(_deviceParameters.Tests.IN7, "IN7");
        }

        public async Task Test_IN8()
        {
            await Do_Test(_deviceParameters.Tests.IN8, "IN8");
        }

        public async Task Test_OUT1()
        {
            await Do_Test(_deviceParameters.Tests.OUT1, "OUT1");
        }

        public async Task Test_OUT2()
        {
            await Do_Test(_deviceParameters.Tests.OUT2, "OUT2");
        }

        public async Task Test_OUT3()
        {
            await Do_Test(_deviceParameters.Tests.OUT3, "OUT3");
        }

        public async Task Test_OUT4()
        {
            await Do_Test(_deviceParameters.Tests.OUT4, "OUT4");
        }

        public async Task Test_OUT12V()
        {
            await Do_Test(_deviceParameters.Tests.OUTPUT_12V, "OUTPUT_12V");
        }

        public async Task Test_RS232()
        {
             await Do_Test(_deviceParameters.Tests.RS232, "RS232");
        }

        public async Task Test_RS485()
        {
            await Do_Test(_deviceParameters.Tests.RS485, "RS485");
        }

        public async Task Test_SCRIPT()
        {
            await Do_Test(_deviceParameters.Tests.SCRIPT, "SCRIPT");
        }
    }
}
