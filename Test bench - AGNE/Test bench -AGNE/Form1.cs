﻿using System;
using System.Drawing;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Collections.Generic;
using Test_bench_AGNE.Services.CommPort;
using Test_bench_AGNE.Services;
using Test_bench_AGNE.Models.Device;

namespace Test_bench_AGNE
{
    public partial class Form1 : Form
    {
        string testingFirmware = "";

        public static List<Device_Parameters> device_Parameters = new ();
        Config_File_Handler reader = new (device_Parameters);
        private delegate void SetTextCallback(string text);
        private delegate void SetBoolCallback(bool state);
        private delegate void SetTextBoxCallback(TextBox textBox, string state);
        List<CheckBox> device_CheckBoxes = new ();
        public static Comm_Port_Parser serialParser = new ();
        PowerSupply powerSupply = new ();
        AuxiliaryPowerSupply auxiliaryPowerSupply = new ();
        ModbusPort_Thread modbus_Thread = new ();
        RS232_Thread RS232_thread = new ();
        Database database = new ();


        Thread testingTaskAsync;
        Thread tcpTestingTaskAsync;
        Thread udpTestingTaskAsync;
        Thread modbusCommPortThread;
        Thread rs232CommPortThread;
        Thread psuControlThread;
        Thread secondPSUControlThread;


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Logging.form1 = this;
            Logging.PrintHeader(20, "10");
            if (reader.Read_Config_File())
            {
                Write_Devices_To_TextBoxes();
            }
            else
            {
                Update_Info_textbox("Failed to load config file");
            }
            Start_Test_Button.Enabled = false;
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            psuControlThread = new Thread(() => powerSupply.Check_For_PSU_Comport(this));
            psuControlThread.Start();
            secondPSUControlThread = new Thread(() => auxiliaryPowerSupply.Check_For_Auxiliary_PSU_Comport(this));
            secondPSUControlThread.Start();
            modbusCommPortThread = new Thread(() => modbus_Thread.Check_Comport_State(this));
            modbusCommPortThread.Start();
            rs232CommPortThread = new Thread(() => RS232_thread.Check_Comport_State(this));
            rs232CommPortThread.Start();

            var tcp_socket_task = Task.Run(() => { TcpAsynchronousSocketListener.StartListening(); });
            //var udp_socket_task = Task.Run(() => { UDPAsynchronousSocketListener.StartListening(); });
            tcpTestingTaskAsync = new Thread(new ThreadStart(Update_InfoTextBox_With_TCPSocketData));
            tcpTestingTaskAsync.Start();
            udpTestingTaskAsync = new Thread(new ThreadStart(Update_InfoTextBox_With_UDPSocketData));
            udpTestingTaskAsync.Start();

            Firmware_ComboBox.DataSource = database.Get_SDK_Firmwares();
        }


        private void Form1_Close(object sender, EventArgs e)
        {
            if (testingTaskAsync != null && testingTaskAsync.IsAlive)
                testingTaskAsync.Interrupt();
            if (tcpTestingTaskAsync != null && tcpTestingTaskAsync.IsAlive)
                tcpTestingTaskAsync.Interrupt();
            if (udpTestingTaskAsync != null && udpTestingTaskAsync.IsAlive)
                udpTestingTaskAsync.Interrupt();
            if (modbusCommPortThread != null && modbusCommPortThread.IsAlive)
                modbusCommPortThread.Interrupt();
            if (rs232CommPortThread != null && rs232CommPortThread.IsAlive)
                rs232CommPortThread.Interrupt();

            if (serialParser.deviceCommPort.IsOpen)
            {
                serialParser.deviceCommPort.Close();
            }
        }

        public void Update_DeviceCommPortName_Label(string message)
        {
            try
            {
                if (this.deviceComPortName.InvokeRequired)
                {
                    Form1.SetTextCallback setTextCallback = new (this.Update_DeviceCommPortName_Label);
                    object[] objArray = new object[] { message };
                    base.Invoke(setTextCallback, objArray);
                    return;
                }
                deviceComPortName.Text = message;
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
            }

        }

        public void Update_ModbusCommPortName_Label(string message)
        {
            try
            {
                if (this.modbusCommPortName.InvokeRequired)
                {
                    Form1.SetTextCallback setTextCallback = new (this.Update_ModbusCommPortName_Label);
                    object[] objArray = new object[] { message };
                    base.Invoke(setTextCallback, objArray);
                    return;
                }
                modbusCommPortName.Text = message;
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
            }

        }

        public void Update_PSUPortName_Label(string message)
        {
            try
            {
                if (this.psuPortName.InvokeRequired)
                {
                    SetTextCallback setTextCallback = new (Update_PSUPortName_Label);
                    object[] objArray = new object[] { message };
                    base.Invoke(setTextCallback, objArray);
                    return;
                }
                this.psuPortName.Text = message;
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
            }

        }


        public void Update_AuxPSUPortName_Label(string message)
        {
            try
            {
                if (this.auxPsuPortName.InvokeRequired)
                {
                    SetTextCallback setTextCallback = new(Update_AuxPSUPortName_Label);
                    object[] objArray = new object[] { message };
                    base.Invoke(setTextCallback, objArray);
                    return;
                }
                this.auxPsuPortName.Text = message;
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
            }

        }

        public void Update_StartTest_Button_Status(bool state)
        {
            try
            {
                if (this.Start_Test_Button.InvokeRequired)
                {
                    Form1.SetBoolCallback setBoolCallback = new(this.Update_StartTest_Button_Status);
                    object[] objArray = new object[] { state };
                    base.Invoke(setBoolCallback, objArray);
                    return;
                }

                Start_Test_Button.Enabled = state;
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
            }
        }

        

        public void Update_Info_textbox(string message)
        {
            try
            {
                if (this.Info_textBox.InvokeRequired)
                {
                    Form1.SetTextCallback setTextCallback = new (this.Update_Info_textbox);
                    object[] objArray = new object[] { message };
                    base.Invoke(setTextCallback, objArray);
                    return;
                }
                //this.Info_textBox.Text = message;
                if (message.Contains("fail"))
                {
                    Info_textBox.BackColor = Color.Red;
                }
                Info_textBox.Text = message;
                Logging.Logs_WriteLine(message);
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
            }
            
        }

        public void Update_CurrentTestLogBox(string message)
        {
            try
            {
                if (this.Info_textBox.InvokeRequired)
                {
                    Form1.SetTextCallback setTextCallback = new (this.Update_CurrentTestLogBox);
                    object[] objArray = new object[] { message };
                    base.Invoke(setTextCallback, objArray);
                    return;
                }
                
                if (message == "CLEAR")
                {
                    this.CurrentTestLogBox.Text = "";

                }
                else
                {
                    this.CurrentTestLogBox.AppendText(message);
                }
                    
                CurrentTestLogBox.BackColor = Color.LightYellow;
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
            }

        }

        public void Update_CurrentTestLogBox()
        {
            Update_CurrentTestLogBox("CLEAR");
        }

        private void Write_Devices_To_TextBoxes()
        {
            try
            {
                device_CheckBoxes.Clear();
                checkbox_update(device1_checkbox, reader._deviceParameters[0].Device_Name);
                checkbox_update(device2_checkbox, reader._deviceParameters[1].Device_Name);
                checkbox_update(device3_checkBox, reader._deviceParameters[2].Device_Name);
                checkbox_update(device4_checkBox, reader._deviceParameters[3].Device_Name);
                checkbox_update(device5_checkBox, reader._deviceParameters[4].Device_Name);
                checkbox_update(device6_checkBox, reader._deviceParameters[5].Device_Name);
                checkbox_update(device7_checkBox, reader._deviceParameters[6].Device_Name);
                checkbox_update(device8_checkBox, reader._deviceParameters[7].Device_Name);
                checkbox_update(device9_checkBox, reader._deviceParameters[8].Device_Name);
                checkbox_update(device10_checkBox, reader._deviceParameters[9].Device_Name);
                checkbox_update(device11_checkBox, reader._deviceParameters[10].Device_Name);
                checkbox_update(device12_checkBox, reader._deviceParameters[11].Device_Name);
                checkbox_update(device13_checkBox, reader._deviceParameters[12].Device_Name);
                checkbox_update(device14_checkBox, reader._deviceParameters[13].Device_Name);
                checkbox_update(device15_checkBox, reader._deviceParameters[14].Device_Name);
                checkbox_update(device16_checkBox, reader._deviceParameters[15].Device_Name);
                checkbox_update(device17_checkBox, reader._deviceParameters[16].Device_Name);
                checkbox_update(device18_checkBox, reader._deviceParameters[17].Device_Name);
                checkbox_update(device19_checkBox, reader._deviceParameters[18].Device_Name);
                checkbox_update(device20_checkBox, reader._deviceParameters[19].Device_Name);
                checkbox_update(device21_checkBox, reader._deviceParameters[20].Device_Name);
                checkbox_update(device22_checkBox, reader._deviceParameters[21].Device_Name);
                checkbox_update(device23_checkBox, reader._deviceParameters[22].Device_Name);
                checkbox_update(device24_checkBox, reader._deviceParameters[23].Device_Name);
                checkbox_update(device25_checkBox, reader._deviceParameters[24].Device_Name);
                checkbox_update(device26_checkBox, reader._deviceParameters[25].Device_Name);
                checkbox_update(device27_checkBox, reader._deviceParameters[26].Device_Name);
                checkbox_update(device28_checkBox, reader._deviceParameters[27].Device_Name);
                checkbox_update(device29_checkBox, reader._deviceParameters[28].Device_Name);
                checkbox_update(device30_checkBox, reader._deviceParameters[29].Device_Name);
                checkbox_update(device31_checkBox, reader._deviceParameters[30].Device_Name);
                checkbox_update(device32_checkBox, reader._deviceParameters[31].Device_Name);
                checkbox_update(device33_checkBox, reader._deviceParameters[32].Device_Name);
                checkbox_update(device34_checkBox, reader._deviceParameters[33].Device_Name);
                checkbox_update(device35_checkBox, reader._deviceParameters[34].Device_Name);
                checkbox_update(device36_checkBox, reader._deviceParameters[35].Device_Name);
            }
            catch
            {
            }
            Update_Info_textbox("Config file loaded");
        }

        private void checkbox_update(CheckBox checkBox, string device_name)
        {
            try
            {
                checkBox.Text = device_name;
                device_CheckBoxes.Add(checkBox);
                checkBox.Visible = true;
            }
            catch
            {
            }
        }

        private void Update_Hardware_Results_Boxes_New_Device(Device_Parameters device)
        {
            HardwareTextBox_TestingUpdate(CAN1_textBox, device.Tests.CAN1);
            HardwareTextBox_TestingUpdate(CAN2_textBox, device.Tests.CAN2);
            HardwareTextBox_TestingUpdate(ADC2_textBox, device.Tests.ADC2);
            HardwareTextBox_TestingUpdate(ADC3_textBox, device.Tests.ADC3);
            HardwareTextBox_TestingUpdate(ADC4_textBox, device.Tests.ADC4);
            HardwareTextBox_TestingUpdate(ADC5_textBox, device.Tests.ADC5);
            HardwareTextBox_TestingUpdate(IN1_Motion_textBox, device.Tests.IN1_Motion);
            HardwareTextBox_TestingUpdate(IN2_textBox, device.Tests.IN2);
            HardwareTextBox_TestingUpdate(IN3_textBox, device.Tests.IN3);
            HardwareTextBox_TestingUpdate(IN4_textBox, device.Tests.IN4);
            HardwareTextBox_TestingUpdate(IN5_textBox, device.Tests.IN5);
            HardwareTextBox_TestingUpdate(IN6_textBox, device.Tests.IN6);
            HardwareTextBox_TestingUpdate(IN7_textBox, device.Tests.IN7);
            HardwareTextBox_TestingUpdate(IN8_textBox, device.Tests.IN8);
            HardwareTextBox_TestingUpdate(OUT1_textBox, device.Tests.OUT1);
            HardwareTextBox_TestingUpdate(OUT2_textBox, device.Tests.OUT2);
            HardwareTextBox_TestingUpdate(OUT3_textBox, device.Tests.OUT3);
            HardwareTextBox_TestingUpdate(OUT4_textBox, device.Tests.OUT4);
            HardwareTextBox_TestingUpdate(OUT_12V_textBox, device.Tests.OUTPUT_12V);
            HardwareTextBox_TestingUpdate(RS232_textBox, device.Tests.RS232);
            HardwareTextBox_TestingUpdate(RS485_textBox, device.Tests.RS485);
            HardwareTextBox_TestingUpdate(Tachograph_textBox, device.Tests.TACHOGRAPH);
            HardwareTextBox_TestingUpdate(Temp_sensor_textBox, device.Tests.TEMP_SENSOR);
            HardwareTextBox_TestingUpdate(Ibutton_textBox, device.Tests.IBUTTON);
            HardwareTextBox_TestingUpdate(BLE_extender_textBox, device.Tests.BLE_EXTENDER);
            HardwareTextBox_TestingUpdate(Script_textBox, device.Tests.SCRIPT);
        }

        public void HardwareTextBox_TestingUpdate(TextBox textBox, List<Test_Data> hardware)
        {
            if (hardware.Count != 0)
            {
                Update_TextBox_Status(textBox, "Not tested yet");
            }
            else
            {
                Update_TextBox_Status(textBox, "Not testing");
            }

        }


        

        private async void Start_Test()
        {
            Testing testing = new ();
            testing.stopTest = false;

            Update_StartTest_Button_Status(false);

            try
            {
                if (testingFirmware != "Select Firmware" && testingFirmware != "")
                {
                    Logging.Change_Logs_Paths_By_Firmware(testingFirmware);
                    testing.Start_Tests(reader._deviceParameters, testingFirmware);
                    while (true)
                    {
                        Thread.Sleep(1500);
                        if (testing.testStopped)
                        {
                            break;
                        }
                        Update_Hardware_Results_Boxes_New_Device(testing._deviceParameters);
                        Thread.Sleep(500);
                        Update_TestResults(testing.testsResults);
                    }
                }
            }
            catch (ThreadInterruptedException ex)
            {
                testing.stopTest = true;
                Logging.Write_Exceptions(ex);
            }
            catch (Exception ex)
            {
                
                Logging.Write_Exceptions(ex);
            }
            try
            {
                while (!testing.testStopped)
                {
                    Thread.Sleep(100);
                }
            }
            catch
            {

            }

            Update_StartTest_Button_Status(true);
        }

        private void Update_TestResults(Test_Results testResults)
        {
            Update_TextBox_Status(CAN1_textBox, testResults.CAN1);
            Update_TextBox_Status(CAN2_textBox, testResults.CAN2);
            Update_TextBox_Status(ADC2_textBox, testResults.ADC2);
            Update_TextBox_Status(ADC3_textBox, testResults.ADC3);
            Update_TextBox_Status(ADC4_textBox, testResults.ADC4);
            Update_TextBox_Status(ADC5_textBox, testResults.ADC5);
            Update_TextBox_Status(IN1_Motion_textBox, testResults.IN1_Motion);
            Update_TextBox_Status(IN2_textBox, testResults.IN2);
            Update_TextBox_Status(IN3_textBox, testResults.IN3);
            Update_TextBox_Status(IN4_textBox, testResults.IN4);
            Update_TextBox_Status(IN5_textBox, testResults.IN5);
            Update_TextBox_Status(IN6_textBox, testResults.IN6);
            Update_TextBox_Status(IN7_textBox, testResults.IN7);
            Update_TextBox_Status(IN8_textBox, testResults.IN8);
            Update_TextBox_Status(OUT1_textBox, testResults.OUT1);
            Update_TextBox_Status(OUT2_textBox, testResults.OUT2);
            Update_TextBox_Status(OUT3_textBox, testResults.OUT3);
            Update_TextBox_Status(OUT4_textBox, testResults.OUT4);
            Update_TextBox_Status(OUT_12V_textBox, testResults.OUTPUT_12V);
            Update_TextBox_Status(RS232_textBox, testResults.RS232);
            Update_TextBox_Status(RS485_textBox, testResults.RS485);
            Update_TextBox_Status(Tachograph_textBox, testResults.TACHOGRAPH);
            Update_TextBox_Status(Temp_sensor_textBox, testResults.TEMP_SENSOR);
            Update_TextBox_Status(Ibutton_textBox, testResults.IBUTTON);
            Update_TextBox_Status(BLE_extender_textBox, testResults.BLE_EXTENDER);
            Update_TextBox_Status(Script_textBox, testResults.SCRIPT);
        }

        private void Update_InfoTextBox_With_TCPSocketData()
        {
            try
            {
                string previous_received_data = "";

                while (true)
                {
                    if (TcpAsynchronousSocketListener.received_data_from_Device != null && TcpAsynchronousSocketListener.received_data_from_Device != previous_received_data)
                    {
                        previous_received_data = TcpAsynchronousSocketListener.received_data_from_Device;
                        Update_Info_textbox("TCP socket received:" + TcpAsynchronousSocketListener.received_data_from_Device);
                    }
                    Thread.Sleep(100);
                }
            }
            catch (ThreadInterruptedException ex)
            {
                Logging.Write_Exceptions(ex);
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
            }
        }

        private void Update_InfoTextBox_With_UDPSocketData()
        {
            try
            {
                string previous_received_data = "";
                while (true)
                {
                    if (UDPAsynchronousSocketListener.receivedDataFromDevice != null && UDPAsynchronousSocketListener.receivedDataFromDevice != previous_received_data)
                    {
                        previous_received_data = UDPAsynchronousSocketListener.receivedDataFromDevice;
                        Update_Info_textbox("UDP socket received:" + UDPAsynchronousSocketListener.receivedDataFromDevice);
                    }
                    Thread.Sleep(100);
                }
            }
            catch (ThreadInterruptedException ex)
            {
                Logging.Write_Exceptions(ex);
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
            }
        }

        private void Start_Test_Button_Click(object sender, EventArgs e)
        {
            //Update_CurrentTestLogBox("CLEAR");
            Update_CurrentTestLogBox();

            

            testingTaskAsync = new Thread(new ThreadStart(Start_Test));
            testingTaskAsync.Start();
        }

        private async void Start_Test_Button_EnableChanged(object sender, EventArgs e)
        {
            if ((Firmware_ComboBox.Text == "Select Firmware" || modbusCommPortName.Text == "NOT_FOUND" || deviceComPortName.Text == "NOT_FOUND"
                 || string.IsNullOrWhiteSpace(Firmware_ComboBox.Text) || (testingTaskAsync != null && testingTaskAsync.IsAlive))/* && Start_Test_Button.Enabled != false*/)
            {
                Start_Test_Button.Enabled = false;
            }

        }

        private void Stop_Test_Button_Click(object sender, EventArgs e)
        {
            if (testingTaskAsync != null && testingTaskAsync.IsAlive)
                testingTaskAsync.Interrupt();

            Update_CurrentTestLogBox();
        }

        private void Firmware_ComboBox_Click(object sender, EventArgs e)
        {
            string temp = Firmware_ComboBox.Text;
            Firmware_ComboBox.DataSource = database.Get_SDK_Firmwares();
            if (temp != "Select Firmware")
            {
                Firmware_ComboBox.Text = temp;
            }
        }

        private void Firmware_ComboBox_IndexChanged(object sender, EventArgs e)
        {
            if (Firmware_ComboBox.Text != "Select Firmware" && Firmware_ComboBox.Text != "")
            {
                testingFirmware = Firmware_ComboBox.Text;
                Update_StartTest_Button_Status(true);
            }
        }

        private void New_device_cancel_button_Click(object sender, EventArgs e)
        {
            New_device_cancel();
        }

        private void New_device_cancel()
        {
            Hardware_result_groupBox.Visible = true;
            Peripheral_results_groupBox.Visible = true;
            Functions_Test_Results_groupBox.Visible = true;
            New_device_groupBox.Visible = false;
            newdevice_deviceName_textBox.Text = "";
            newDevice_RelayId_textBox.Text = "";
            newDevice_AssemblyRev_textBox.Text = "";
            newDevice_IMEI_textBox.Text = "";
            newDevice_hwVersion_textBox.Text = "";
            newDevice_phoneNumber_textBox.Text = "";
        }

        private void OpenLogsFolder_toolStripMenuItem_Click(object sender, EventArgs e)
        {
            var processInfo = new ProcessStartInfo() { FileName = AppDomain.CurrentDomain.BaseDirectory + "\\Tests Logs", UseShellExecute = true };
            Process.Start(processInfo);
        }

        private void OpenConfigFile_toolStripMenuItem_Click(object sender, EventArgs e)
        {
            var processInfo = new ProcessStartInfo() { FileName = reader.configFilePath, UseShellExecute = true };
            Process.Start(processInfo);
        }

        private void AddNewDeviceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hardware_result_groupBox.Visible = false;
            Peripheral_results_groupBox.Visible = false;
            Functions_Test_Results_groupBox.Visible = false;
            New_device_groupBox.Visible = true;
        }

        private void NewDevice_addDevice_button_Click(object sender, EventArgs e)
        {
            if (Check_NewDevice_Params())
            {
                return;
            }

            if (reader.Add_newDevice_To_Config(Get_NewDevice_From_NewDeviceBox(), ref reader._deviceParameters))
            {
                Update_Info_textbox("Config file updated succesfully");
            }
            else
            {
                Update_Info_textbox("Config file update failed");
                return;
            }

            New_device_cancel();
            Write_Devices_To_TextBoxes();
        }

        private bool Check_NewDevice_Params()
        {
            bool error = false;
            if (string.IsNullOrEmpty(newdevice_deviceName_textBox.Text))
            {
                newdevice_deviceName_textBox.BackColor = Color.Red;
                error = true;
            }
            if (string.IsNullOrEmpty(newDevice_RelayId_textBox.Text))
            {
                newDevice_RelayId_textBox.BackColor = Color.Red;
                error = true;
            }
            if (string.IsNullOrEmpty(newDevice_AssemblyRev_textBox.Text))
            {
                newDevice_AssemblyRev_textBox.BackColor = Color.Red;
                error = true;
            }
            if (string.IsNullOrEmpty(newDevice_IMEI_textBox.Text) || newDevice_IMEI_textBox.Text.Length < 15)
            {
                newDevice_IMEI_textBox.BackColor = Color.Red;
                error = true;
            }
            if (string.IsNullOrEmpty(newDevice_hwVersion_textBox.Text))
            {
                newDevice_hwVersion_textBox.BackColor = Color.Red;
                error = true;
            }
            if (string.IsNullOrEmpty(newDevice_phoneNumber_textBox.Text))
            {
                newDevice_phoneNumber_textBox.BackColor = Color.Red;
                error = true;
            }

            return error;
        }

        private Device_Parameters Get_NewDevice_From_NewDeviceBox()
        {
            Device_Parameters newDevice = new ();

            newDevice.Device_Name = newdevice_deviceName_textBox.Text;
            newDevice.Relay_ID = Parse_from_string_to_byte(newDevice_RelayId_textBox.Text);
            newDevice.Assembly_Revision = Parse_from_string_to_byte(newDevice_AssemblyRev_textBox.Text);
            newDevice.IMEI = newDevice_IMEI_textBox.Text;
            newDevice.Hardware_Version = newDevice_hwVersion_textBox.Text;
            newDevice.SIM_Number = newDevice_phoneNumber_textBox.Text;
            newDevice.SDK_device = Parse_checkbox_to_bool(newDevice_sdkdevice_checkBox);
            
            
            /*newDevice.CAN1 = Parse_checkbox_to_bool(Can1_checkbox);
            newDevice.CAN2 = Parse_checkbox_to_bool(Can2_checkbox);
            newDevice.ADC2 = Parse_checkbox_to_bool(Adc2_checkbox);
            newDevice.ADC3 = Parse_checkbox_to_bool(Adc3_checkbox);
            newDevice.ADC4 = Parse_checkbox_to_bool(Adc4_checkbox);
            newDevice.ADC5 = Parse_checkbox_to_bool(Adc5_checkbox);
            newDevice.IN1_Motion = Parse_checkbox_to_bool(In1_motion_checkbox);
            newDevice.IN2 = Parse_checkbox_to_bool(In2_checkbox);
            newDevice.IN3 = Parse_checkbox_to_bool(In3_checkbox);
            newDevice.IN4 = Parse_checkbox_to_bool(IN4_checkbox);
            newDevice.IN5 = Parse_checkbox_to_bool(In5_checkBox);
            newDevice.IN6 = Parse_checkbox_to_bool(In6_checkBox);
            newDevice.IN7 = Parse_checkbox_to_bool(In7_checkBox);
            newDevice.IN8 = Parse_checkbox_to_bool(In8_checkBox);
            newDevice.OUT1 = Parse_checkbox_to_bool(OUT1_checkBox);
            newDevice.OUT2 = Parse_checkbox_to_bool(OUT2_checkBox);
            newDevice.OUT3 = Parse_checkbox_to_bool(OUT3_checkBox);
            newDevice.OUT4 = Parse_checkbox_to_bool(OUT4_checkBox);
            newDevice.OUTPUT_12V = Parse_checkbox_to_bool(OUT_12V_checkBox);
            newDevice.RS232 = Parse_checkbox_to_bool(RS232_checkBox);
            newDevice.RS485 = Parse_checkbox_to_bool(RS485_checkBox); 
            newDevice.TACHOGRAPH = Parse_checkbox_to_bool(Tachograph_checkBox);
            newDevice.IBUTTON = Parse_checkbox_to_bool(Ibutton_checkBox);
            newDevice.BLE_EXTENDER = Parse_checkbox_to_bool(Ble_Extender_checkBox);
            newDevice.TEMP_SENSOR = Parse_checkbox_to_bool(Temp_sensor_checkBox);*/

            return newDevice;
        }

        private static byte Parse_from_string_to_byte(string data)
        {
            if (byte.TryParse(data, out byte temp))
            {
                return temp;
            }
            return 0;
        }

        private static bool Parse_checkbox_to_bool(CheckBox checkBox)
        {
            return Convert.ToBoolean(checkBox.CheckState);
        }
        
        private void Device_checkbox_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;
            if (!checkBox.Checked)
            {
                checkBox.BackColor = Color.LightYellow;
            }
            else
            {
                checkBox.BackColor = Color.LimeGreen;
            }
        }

        private void Update_TextBox_Status(TextBox textBox, string state)
        {
            if (textBox.Text != "Not testing")
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(state) || textBox.Text == state)
                    {
                        return;
                    }

                    if (textBox.InvokeRequired)
                    {
                        Form1.SetTextBoxCallback setTextBoxCallback = new(this.Update_TextBox_Status);
                        object[] objArray = new object[] { textBox, state };
                        base.Invoke(setTextBoxCallback, objArray);
                        return;
                    }
                    
                    switch(state)
                    {
                        case "PASSED":
                            {
                                textBox.BackColor = Color.LimeGreen;
                                break;
                            }

                        case "FAILED":
                            {
                                textBox.BackColor = Color.Red;
                                break;
                            }

                        case "Not tested yet":
                            {
                                textBox.BackColor = Color.Red;
                                break;
                            }

                        case "Not testing":
                            {

                                textBox.BackColor = Color.LightYellow;
                                break;
                            }
                    }

                    textBox.Text = state;
                }
                catch (Exception ex)
                {
                    Logging.Write_Exceptions(ex);
                }
            }
        }
    }
}
