using System.IO.Ports;
using Test_bench_AGNE.Services.Wialon.Models;
using Test_bench_AGNE.Services;

namespace Test_bench_AGNE
{
    public class Device_Relay_Control
    {
        public Modbus _modbus;
        public SerialPort _port;
        public Device_Relay_Control (Modbus modbus, SerialPort port)
        {
            _port = port;
            _modbus = modbus;
        }

        public bool Turn_On_USB_Relay()
        {
            return _modbus.Turn_On_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Device_Control_Relays.USB_Relay);
        }

        public bool Turn_Off_USB_Relay()
        {
            return _modbus.Turn_Off_Relay(_port, Main_Control_Relays.First_main_Control_Board_Slave_ID, Device_Control_Relays.USB_Relay);
        }
    }
}
