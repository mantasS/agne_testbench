using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Test_bench_AGNE.Models.Device;

namespace Test_bench_AGNE
{
    public class Config_File_Handler
    {
        public string directoryPath = AppDomain.CurrentDomain.BaseDirectory;
        public string configPath = AppDomain.CurrentDomain.BaseDirectory + "\\Config";
        public string configFilePath;
        public List<Device_Parameters> _deviceParameters;
        public Config_File_Handler(List<Device_Parameters> device_Parameters)
        {
            _deviceParameters = device_Parameters;
            configFilePath = configPath + "\\Config.cfg";
        }

        public bool Read_Config_File()
        {
            if (!Directory.Exists(configPath))
            {
                Directory.CreateDirectory(configPath);
                File.Create(configFilePath);
            }
            string lines = Get_Lines();

            if (lines != null)
            {
                Parse_File(lines);
                return true;
            }
            return false;
        }

        private string Get_Lines()
        {
            try
            {
                string lines = File.ReadAllText(configFilePath);
                return lines;
            }
            catch (FileNotFoundException)
            {
                Logging.Write_Exceptions("Config.cfg file not found");
                return null;
            }
            catch (IOException)
            {
                Logging.Write_Exceptions("Failed opening Config.cfg");
                return null;
            }
            catch
            {
                return null;
            }
        }

        private bool Parse_File(string lines)
        {
            try
            {
                _deviceParameters = JsonConvert.DeserializeObject<List<Device_Parameters>>(lines);
                if (_deviceParameters != null)
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
            return false;
        }

        public bool Add_newDevice_To_Config(Device_Parameters newDevice, ref List<Device_Parameters> deviceParameters)
        {
            try
            {
                if (deviceParameters != null)
                {
                    deviceParameters.Add(newDevice);
                }
                else
                {
                    deviceParameters = new ();
                    deviceParameters.Add(newDevice);
                }
                    
                string jsonInString = JsonConvert.SerializeObject(deviceParameters, Formatting.Indented);
                File.WriteAllText(configFilePath, jsonInString);
                return true;
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
            }

            return false;
        }
    }

}
