﻿using System.Collections.Generic;

namespace Test_bench_AGNE.Services.Wialon.Models.XDMData
{
    public class XDMData
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public List<XDM_Results> results { get; set; }
        public Paginator paginator { get; set; }
        public Filter filter { get; set; }
    }

    public class Paginator
    {
        public int firstRecord { get; set; }
        public int itemsPerPage { get; set; }
        public int recordCount { get; set; }
        public byte sortOrderAsc { get; set; }
        public string sortField { get; set; }
        public string dbSortField { get; set; }
        public string dbSortOrder { get; set; }
    }
    public class Filter
    {
        public List<string> imeis { get; set; }
        public int dealerId { get; set; }
    }



    public class XDM_Results
    {
        public Settings_Data settings { get; set; }
        public Information_Data information { get; set; }
        public int id { get; set; }
        public int userConfigTemplateId { get; set; }
    }


    public class Settings_Data
    {
        public string imei { get; set; }
        public string notes { get; set; }
        public string phoneNumber { get; set; }
        public Firmware_Data firmware { get; set; }
        public Firmware_Data configuration { get; set; }
        public Dealer dealer { get; set; }
        public Hardware hardware { get; set; }
    }
    public class Firmware_Data
    {
        public string newFirmwareName { get; set; }
        public string currentFirmwareName { get; set; }
    }
    public class Configuration
    {
        public string newConfigName { get; set; }
        public string currentConfigName { get; set; }
    }
    public class Dealer
    {
        public int id { get; set; }
        public string name { get; set; }
    }
    public class Hardware
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class Information_Data
    {
        public string gpsFirmwareName { get; set; }
        public string modemFirmwareName { get; set; }
        public Firmware_Update firmwareUpdate { get; set; }
        public Configurations_Update configurationUpdate { get; set; }
        public Activity_Update activityUpdate { get; set; }
        public int createdInSystem { get; set; }
        public string imsi { get; set; }
        public string iccid { get; set; }
    }
    public class Firmware_Update
    {
        public double? firmwareUpdateProgress { get; set; }
        public string firmwareUpdateState { get; set; }
        public double? lastFirmwareUpdateTime { get; set; }
    }
    public class Configurations_Update
    {
        public double? configUpdateProgress { get; set; }
        public string configUpdateState { get; set; }
        public double? lastConfigUpdateTime { get; set; }
    }
    public class Activity_Update
    {
        public long? firstActivity { get; set; }
        public long? lastActivity { get; set; }
    }

    public class Mass_Update_Devices
    {
        public List<string> imeis { get; set; }
        public int? newConfigId { get; set; }
        public int? newFirmwareId { get; set; }
        public string notes { get; set; }
        public int? newDealerId { get; set; }
    }
}
