﻿using System.Collections.Generic;

namespace Test_bench_AGNE.Services.Wialon.Models.XDMData
{
    public class Config
    {
        public string notes { get; set; }
        public int? newFirmwareId { get; set; }
        public int dealerId { get; set; }
        public int? newConfigId { get; set; }
        public string phoneNumber { get; set; }
        public List<string> propertiesChanged { get; set; }
        public SettingsOverrides configOverrides { get; set; }
        public string name { get; set; }
        public int id { get; set; }
        public bool isConfigUpdateCanceled { get; set; }
        public bool isFirmwareUpdateCanceled { get; set; }

    }
    
    public class SettingsOverrides
    {
        public List<string> deleted { get; set; }
        public List<string> modified { get; set; }
    }

    public class UserTemplates
    {
        public List<Config> results { get; set; }
        public List<Categories> categories { get; set; }
        public List<Categories> subCategories { get; set; }
    }

    public class Categories
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
