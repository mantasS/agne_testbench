﻿namespace Test_bench_AGNE.Services.Wialon.Models.XDMData
{
    public class Sdk_Versions
    {
        public int id { get; set; }
        public string version { get; set; }
        public string name { get; set; }
        public string firmwareCheckRule { get; set; }
    }
}
