﻿using System.Collections.Generic;

namespace Test_bench_AGNE.Services.Wialon.Models.XDMData
{
    public class XDMSendData_GetItem
    {
        public Paginator paginator { get; set; }
        public Filter_GetItem filter { get; set; }
    }

    public class Filter_GetItem
    {
        public string currentConfigName { get; set; }
        public string dealerName { get; set; }
        public List<string> imeis { get; set; }
        public string newFirmwareName { get; set; }
    }

    public class XDMGetData_GetItem
    {
        public List<Results> results { get; set; }
    }

    public class Results
    {
        public int newFirmwareId { get; set; }
        public int currentConfigId { get; set; }
        public int newConfigId { get; set; }
        public int hardwareVersionId { get; set; }
        public int newUserSettingsTemplateId { get; set; }
        public int currentUserSettingsTemplateId { get; set; }
        public int id { get; set; }
        public string imei { get; set; }
        public string newFirmwareName { get; set; }
        public string currentFirmwareName { get; set; }
        public string currentConfigName { get; set; }
        public string newConfigName { get; set; }
        public string notes { get; set; }
        public int dealerId { get; set; }
        public string dealerName { get; set; }
        public string hardwareVersion { get; set; }
        public string hardwareVersionName { get; set; }
        public string gpsFirmwareName { get; set; }
        public string modemFirmwareName { get; set; }
        public UpdateProgress configUpdateProgress { get; set; }
        public int configUpdateState { get; set; }
        public string lastConfigUpdateTime { get; set; }
        public UpdateProgress firmwareUpdateProgress { get; set; }
        public int firmwareUpdateState { get; set; }
        public string lastFirmwareUpdateTime { get; set; }
        public string phoneNumber { get; set; }
        public string firstActivity { get; set; }
        public string lastActivity { get; set; }
        public string imsi { get; set; }
        public string iccid { get; set; }
        public string createdInSystem { get; set; }
        public string lastCustomCommandsSendTime { get; set; }
        public bool isXtDevice { get; set; }
        public int customCommandUpdateState { get; set; }
        public UpdateProgress customCommandUpdateProgress { get; set; }
        public string customCommandUpdateTime { get; set; }
        public string pcbVersion { get; set; }
        public string assemblyRevision { get; set; }
        public string deviceName { get; set; }

    }

    public class UpdateProgress
    {
        public int partNumber { get; set; }
        public int totalParts { get; set; }
    }

}
