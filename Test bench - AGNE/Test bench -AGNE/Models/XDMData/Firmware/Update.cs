﻿using System.Collections.Generic;

namespace Test_bench_AGNE.Services.Wialon.Models.XDMData
{
    public class Update_Firmware
    {
        public Update_Firmware_Parameters parameters { get; set; }

    }
    public class Update_Firmware_Parameters
    {
        public List<int> dealers { get; set; }
        public int id { get; set; }
        public bool visibleToAll { get; set; }
        public bool available { get; set; }
        public string notes { get; set; }
    }
}
