﻿using System.Collections.Generic;

namespace Test_bench_AGNE.Services.Wialon.Models.XDMData
{
    public class Firmwares
    {
        public int id { get; set; }
        public string version { get; set; }
        public string notes { get; set; }
    }

    public class Get_All_Firmwares_Parameters
    {
        public Parameters parameters { get; set; }
        public Paginator paginator { get; set; }
    }

    public class Parameters
    {
        public string version { get; set; }
    }

    public class XDM_Firmware_Data
    {
        public List<Firmware_Results> results { get; set; }
    }

    public class Firmware_Results
    {
        public string filename { get; set; }
        public bool available { get; set; }
        public bool visibleToAll { get; set; }
        public string version { get; set; }
        public int id { get; set; }
    }

    

}





