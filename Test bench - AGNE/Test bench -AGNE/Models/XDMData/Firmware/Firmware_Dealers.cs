﻿using System.Collections.Generic;

namespace Test_bench_AGNE.Services.Wialon.Models.XDMData.Firmware
{
    public class Firmware_Dealers
    {
        public List<Firmware_Dealers_Data> assignedDealers { get; set; }
        public List<Firmware_Dealers_Data> unassignedDealers { get; set; }
    }

    public class Firmware_Dealers_Data
    {
        public int id { get; set; }
        public string name { get; set; }
    }

}
