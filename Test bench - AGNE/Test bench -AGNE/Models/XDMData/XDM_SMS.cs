﻿using System.Collections.Generic;

namespace Test_bench_AGNE.Models.XDMData
{
    class XDM_SMS
    {
        public bool addSignature { get; set; }
        public List<string> devicesImeis { get; set; }
        public string message { get; set; }
        public string smsType { get; set; }
    }
}
