﻿namespace Test_bench_AGNE.Services.Wialon.Models.XDMData
{
    public class Config_Search_Parameters
    {
        public SearchParameters searchParameters { get; set; }
        public Paginator paginator { get; set; }
        public int hardwareVersionId { get; set; }
        public int dealerId { get; set; }
    }

    public class SearchParameters
    {
        public string searchColumn { get; set; }
        public string searchString { get; set; }
    }
}
