﻿namespace Test_bench_AGNE.Services.Wialon.Models
{
    public enum PortID
    {
        DeviceCommPort = 1,
        RS485ModbusPort = 2,
        PowerSupplyPort = 3
    }
}
