﻿using Test_bench_AGNE.Models.Device;

namespace Test_bench_AGNE.Models.Global
{
    public static class GlobalVariables
    {
        public static string currentDeviceImei = "";
        public static string RS485ModbusComPortName = "USB Serial Port (COM121)";
        public static string deviceComPortName = "Prolific USB-to-Serial Comm Port (COM143)";
        public static string PSUComPortName = "Silicon Labs CP210x USB to UART Bridge (COM144)";
        public static string SecondPSUComPortName = "Silicon Labs CP210x USB to UART Bridge (COM145)";

        public static bool modbusCommPortOpenAndWOrking = false;
        public static bool deviceCommPortOpenAndWOrking = false;
        public static bool PSUCommPortOpenAndWOrking = false;
        public static bool AuxiliaryPSUCommPortOpenAndWOrking = false;
        public static bool stateOff = false;
        public static bool stateOn = false;
        public static int hardwareTypeID = Hardware_Type.Unknown_Hardware;
        public static int hardwareType = Hardware_Type.Unknown_Hardware;
    }
}
