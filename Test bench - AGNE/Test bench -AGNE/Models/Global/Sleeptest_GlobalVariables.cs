﻿namespace Test_bench_AGNE.Models.Global
{
    class Sleeptest_GlobalVariables
    {
        public static string SleepTestNameInputs = "Sleep_test_controled_by_config_AGNE";
        public static string SleepTestNameSMS = "Sleep_test_controled_by_config_SMS_AGNE";
        public static string deviceRunningMessage = "Device working IN5-";
        public static readonly int timeoutUntillSleep = 90;
        public static readonly int sleepTimeout = 120;
        public static readonly int wakeupTimeout = 15;
    }
}
