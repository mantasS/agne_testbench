﻿namespace Test_bench_AGNE.Services.Wialon.Models
{
    public static class Modbus_Command
    {
        public static readonly byte turnOnRelay = 0x01;
        public static readonly byte turnOffRelay = 0x02;
        public static readonly byte turnOnRelayForSetTime = 0x06;
        public static readonly byte turnOnAllRelays = 0x07;
        public static readonly byte turnOffAllRelays = 0x08;
    }
}
