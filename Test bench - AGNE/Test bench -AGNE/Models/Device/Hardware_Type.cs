﻿namespace Test_bench_AGNE.Models.Device
{
    public class Hardware_Type
    {
        public static byte Unknown_Hardware = 255;
        public static byte Light_2G = 55;
        public static byte Light_3G = 56;
        public static byte Light_2G_32Mb = 57;
        public static byte Light_LTE_Cat_M1 = 58;
        public static byte Light_LTE_Cat_1 = 59;
        public static byte Light_plus_2G = 70;
        public static byte Light_plus_3G = 71;
        public static byte Light_plus_2G_32Mb = 72;
        public static byte Light_plus_LTE_Cat_M1 = 73;
        public static byte Light_plus_LTE_Cat_1 = 74;
        public static byte StCAN_2G = 85;
        public static byte StCAN_3G = 86;
        public static byte StCAN_2G_32Mb = 87;
        public static byte StCAN_LTE_Cat_M1 = 88;
        public static byte StCAN_LTE_Cat_1 = 89;
        public static byte XtCAN_2G = 100;
        public static byte XtCAN_3G = 101;
        public static byte XtCAN_2G_32Mb = 102;
        public static byte XtCAN_LTE_Cat_M1 = 103;
        public static byte XtCAN_LTE_Cat_1 = 104;
        public static byte XtCAN_2G_128Mb = 105;
        public static byte Tacho_2G = 115;
        public static byte Tacho_3G = 116;
        public static byte Tacho_2G_32Mb = 117;
        public static byte Tacho_LTE_Cat_M1 = 118;
        public static byte Tacho_LTE_Cat_1 = 119;
        public static byte XG4780 = 1;
    }
}
