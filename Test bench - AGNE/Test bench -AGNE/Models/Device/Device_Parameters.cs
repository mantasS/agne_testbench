﻿using System.Collections.Generic;

namespace Test_bench_AGNE.Models.Device
{
    public class Device_Parameters
    {
        public string Device_Name { get; set; }
        public byte Relay_ID { get; set; }
        public byte Assembly_Revision { get; set; }
        public string IMEI { get; set; }
        public string Hardware_Version { get; set; }
        public bool SDK_device { get; set; }
        public string SIM_Number { get; set; }
        public Tests Tests { get; set; }
    }

    public class Tests
    {
        public List<Test_Data> CAN1 { get; set; }
        public List<Test_Data> CAN2 { get; set; }
        public List<Test_Data> ADC2 { get; set; }
        public List<Test_Data> ADC3 { get; set; }
        public List<Test_Data> ADC4 { get; set; }
        public List<Test_Data> ADC5 { get; set; }
        public List<Test_Data> IN1_Motion { get; set; }
        public List<Test_Data> IN2 { get; set; }
        public List<Test_Data> IN3 { get; set; }
        public List<Test_Data> IN4 { get; set; }
        public List<Test_Data> IN5 { get; set; }
        public List<Test_Data> IN6 { get; set; }
        public List<Test_Data> IN7 { get; set; }
        public List<Test_Data> IN8 { get; set; }
        public List<Test_Data> OUT1 { get; set; }
        public List<Test_Data> OUT2 { get; set; }
        public List<Test_Data> OUT3 { get; set; }
        public List<Test_Data> OUT4 { get; set; }
        public List<Test_Data> OUTPUT_12V { get; set; }
        public List<Test_Data> RS232 { get; set; }
        public List<Test_Data> RS485 { get; set; }
        public List<Test_Data> TACHOGRAPH { get; set; }
        public List<Test_Data> TEMP_SENSOR { get; set; }
        public List<Test_Data> IBUTTON { get; set; }
        public List<Test_Data> BLE_EXTENDER { get; set; }
        public List<Test_Data> SCRIPT { get; set; }
    }
}
