﻿namespace Test_bench_AGNE.Models.Device
{
    public class Test_Data
    {
        public string TestName { get; set; }
        public string ConfigName { get; set; }
        public string Result { get; set; }
        public string FailResult { get; set; }
        public string Relays { get; set; }
        public int Timeout { get; set; }

    }

    public class Test_Results
    {
        public string CAN1 { get; set; }
        public string CAN2 { get; set; }
        public string ADC2 { get; set; }
        public string ADC3 { get; set; }
        public string ADC4 { get; set; }
        public string ADC5 { get; set; }
        public string IN1_Motion { get; set; }
        public string IN2 { get; set; }
        public string IN3 { get; set; }
        public string IN4 { get; set; }
        public string IN5 { get; set; }
        public string IN6 { get; set; }
        public string IN7 { get; set; }
        public string IN8 { get; set; }
        public string OUT1 { get; set; }
        public string OUT2 { get; set; }
        public string OUT3 { get; set; }
        public string OUT4 { get; set; }
        public string OUTPUT_12V { get; set; }
        public string RS232 { get; set; }
        public string RS485 { get; set; }
        public string TACHOGRAPH { get; set; }
        public string TEMP_SENSOR { get; set; }
        public string IBUTTON { get; set; }
        public string BLE_EXTENDER { get; set; }
        public string SCRIPT { get; set; }
    }
}
