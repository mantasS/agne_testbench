﻿namespace Test_bench_AGNE.Services.Wialon.Models
{
    public static class Device_Control_Relays
    {
        //Relay_Block_One
        public static readonly byte CAN1_H = 0;
        public static readonly byte CAN1_L = 1;
        public static readonly byte OUT2 = 2;
        public static readonly byte IN4_ADC3 = 3;
        public static readonly byte USB_Relay = 4;
        public static readonly byte Supply = 5;
        public static readonly byte RS232_TX = 6;
        public static readonly byte RS232_RX = 7;
        public static readonly byte CAN2_H = 8;
        public static readonly byte CAN2_L = 9;
        public static readonly byte One_Wire = 10;
        public static readonly byte IN5_ADC5 = 11;
        public static readonly byte IN3_OUT1 = 12;
        public static readonly byte CAN1L_Relay = 13;
        public static readonly byte IN2_ADC4 = 14;
        public static readonly byte RS485_B = 15;
        public static readonly byte RS485_A= 16;
    }
}
