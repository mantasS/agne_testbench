﻿namespace Test_bench_AGNE.Models.Wialon
{
    public class Position
    {
        public PositionValue v { get; set; }
        public int? ct { get; set; }
        public int? at { get; set; }
    }

    public class PositionValue
    {
        public float? y { get; set; }
        public float? x { get; set; }
        public float? z { get; set; }
        public float? c { get; set; }
        public float? sc { get; set; }

    }
}
