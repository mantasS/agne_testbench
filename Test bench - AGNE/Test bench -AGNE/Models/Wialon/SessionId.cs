﻿namespace Test_bench_AGNE.Models.Wialon
{
    public class SessionParameters
    {
        public string eid;
        public string token;
        public string th;
    }
    public class Token
    {
        public string th;
        public int timeout_date;
    }
    public class TokenParams
    {
        public int ct { get; set; }
        public int at { get; set; }
        public int dur { get; set; }
    }
}
