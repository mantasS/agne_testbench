﻿namespace Test_bench_AGNE.Models.Wialon
{
    public class LastMessage
    {
        public int? t { get; set; }
        public LastMessageParameters p { get; set; }
        public PositionValue pos { get; set; }
    }
    public class LastMessageParameters
    {
        public int? c2 { get; set; }
        public int? sensor16456 { get; set; }
    }
}
