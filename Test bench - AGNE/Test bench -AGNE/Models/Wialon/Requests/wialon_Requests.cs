﻿namespace Test_bench_AGNE.Models.Wialon.Requests
{
    class Wialon_GPRS_Command_Request
    {
        public int itemId { get; set; }
        public string commandName { get; set; }
        public string linkType { get; set; }
        public string param { get; set; }
        public int timeout { get; set; }
        public int flags { get; set; }
    }

    class Item_Params_Request
    {
        public Wialon_Spec spec { get; set; }
        public int force { get; set; }
        public uint flags { get; set; }
        public int from { get; set; }
        public int to { get; set; }
    }

    class Wialon_Spec
    {
        public string itemsType { get; set; }
        public string propName { get; set; }
        public long propValueMask { get; set; }
        public string sortType { get; set; }

    }
}
