﻿using System.Collections.Generic;

namespace Test_bench_AGNE.Models.Wialon.Requests
{
    class Wialon_Batch_Update_Data
    {
        public List<Wialon_Batch_Update_Params> @params { get; set; }
        public uint flags { get; set; }
    }

    public class Wialon_Batch_Update_Params
    {
        public string svc { get; set; }
        public Wialon_Batch_Update_Params_data @params { get; set; }

    }

    public class Wialon_Batch_Update_Params_data
    {
        public int id { get; set; }
        public string n { get; set; }
        public string c { get; set; }
        public string l { get; set; }
        public string p { get; set; }
        public int a { get; set; }
        public string f { get; set; }
        public int que_length { get; set; }
        public int itemId { get; set; }
        public string callMode { get; set; }
    }
}
