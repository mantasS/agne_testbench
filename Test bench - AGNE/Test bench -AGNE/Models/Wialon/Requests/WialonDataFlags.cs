﻿namespace Test_bench_AGNE.Models.Wialon.Requests
{
    public static class WialonDataFlags
    {
        public static uint baseflag = 0x1;
        public static uint customProperties  = 0x2;
        public static uint billingProperties  = 0x4;
        public static uint customFields  = 0x8;
        public static uint image = 0x10;
        public static uint messages = 0x20;
        public static uint GUID = 0x40;
        public static uint administrativeFields  = 0x80;
        public static uint advancedProperties  = 0x100;
        public static uint availableForCurrentMomentCommands  = 0x200;
        public static uint lastMessageAndPosition  = 0x400;
        public static uint sensors = 0x1000;
        public static uint counters = 0x2000;
        public static uint maintenance = 0x8000;
        public static uint unitConfigurationInReports = 0x20000;
        public static uint listAllPossibleCommands = 0x80000;
        public static uint messageParameters = 0x100000;
        public static uint unitConnectionStatus = 0x200000;
        public static uint profileFields  = 0x400000;
        public static uint allFlags = 0x800000;
    }
}
