﻿namespace Test_bench_AGNE.Models.Wialon
{
    public class Parameters
    {
        public ParameterInt lls_lvl_add1 { get; set; }
        public ParameterInt lls_temp_add1 { get; set; }
        public ParameterInt lls_lvl_add2 { get; set; }
        public ParameterInt lls_temp_add2 { get; set; }
        public ParameterInt in2 { get; set; }
        public ParameterInt in3 { get; set; }
        public ParameterInt in4 { get; set; }
        public ParameterInt in5 { get; set; }
        public ParameterInt in6 { get; set; }
        public ParameterInt in7 { get; set; }
        public ParameterInt in8 { get; set; }
        public ParameterFloat adc13 { get; set; }
        public ParameterFloat adc14 { get; set; }
        public ParameterFloat adc15 { get; set; }
        public ParameterFloat one_wire_temp1 { get; set; }
        public ParameterInt one_wire_temp1_id { get; set; }
        public ParameterInt c2 { get; set; }
        public ParameterInt c4 { get; set; }
        public ParameterInt f0 { get; set; }
        public ParameterInt f102 { get; set; }
        public ParameterInt f103 { get; set; }
        public ParameterInt f109 { get; set; }
        public Position posinfo { get; set; }
        public ParameterInt sensor12288 { get; set; }
        public ParameterInt sensor12289 { get; set; }
        public ParameterInt sensor12290 { get; set; }
        public ParameterInt sensor12291 { get; set; }
        public ParameterInt sensor12292 { get; set; }
        public ParameterInt sensor12293 { get; set; }
        public ParameterInt sensor16456 { get; set; }
        public ParameterInt sensor8199 { get; set; }   //Fuel level
        public ParameterInt sensor8225 { get; set; }   //adblue level
        public ParameterInt sensor12296 { get; set; }   //MCC
        public ParameterInt sensor8195 { get; set; }   //MNC
        public ParameterInt sensor16 { get; set; }   //IN2
        public ParameterInt sensor17 { get; set; }   //IN3
        public ParameterInt sensor18 { get; set; }   //IN4
        public ParameterInt sensor19 { get; set; }   //IN5
        public ParameterInt sensor20 { get; set; }   //IN7
        public ParameterInt war_id { get; set; }
        public ParameterInt fun_id { get; set; }
    }

    public class ParameterFloat
    {
        public float? v { get; set; }
        public int? ct { get; set; }
        public int? at { get; set; }
    }

    public class ParameterInt
    {
        public int? v { get; set; }
        public int? ct { get; set; } // last change time
        public int? at { get; set; } // last message time
    }
}
