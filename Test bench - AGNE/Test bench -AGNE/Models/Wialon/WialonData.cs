﻿using System.Collections.Generic;

namespace Test_bench_AGNE.Models.Wialon
{
    class WialonData
    {
        public List<Items> items { get; set; }
        public string access_token { get; set; }
        public List<Messages> messages { get; set; }
    }

    class SessionIdValid
    {
        public int? error { get; set; }
        public int? totalItemsCount { get; set; }
    }

    
}
