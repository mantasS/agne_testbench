﻿using System.Collections.Generic;

namespace Test_bench_AGNE.Models.Wialon
{
    public class Items
    {
        public int? id;
        public List<Commands> cmds { get; set; }
        public Parameters prms { get; set; }
        public PositionValue pos { get; set; }
        public LastMessage lmsg { get; set; }
        public int? netconn { get; set; }
    }
    public class Commands
    {
        public string n { get; set; }
        public string a { get; set; }
        public string t { get; set; }
        public string c { get; set; }
        public string p { get; set; }
        public string jp { get; set; }
    }
}
