﻿namespace Test_bench_AGNE.Services.Wialon.Models
{
    public static class Main_Control_Relays
    {
        //Relay_Block_One
        public static readonly byte First_main_Control_Board_Slave_ID = 0x00;
        public static readonly byte One_wire_Relay2 = 1;
        public static readonly byte One_wire_Relay3 = 2;
        public static readonly byte One_wire_Relay1 = 3;
        public static readonly byte CAN1H_Relay = 4;
        public static readonly byte CAN2L_Relay2 = 5;
        public static readonly byte CAN2H_Relay2 = 6;
        public static readonly byte CAN2L_Relay1 = 7;
        public static readonly byte CAN2H_Relay1 = 8;
        public static readonly byte RS485B = 9;
        public static readonly byte RS485A = 10;
        public static readonly byte RS232_TX = 11;
        public static readonly byte RS232_RX = 12;
        public static readonly byte CAN1L_Relay = 13;
        public static readonly byte IN3_OUT1 = 14;
        public static readonly byte IN2_ADC4 = 15;
        public static readonly byte IN4_ADC3= 16;

        //Relay_Block_Two
        public static readonly byte Second_main_Control_Board_Slave_ID = 0x01;
        public static readonly byte One_wire_Relay4 = 1;
        public static readonly byte Not_connected2 = 2;
        public static readonly byte IN5_ADC5 = 3;
        public static readonly byte OUT2 = 4;
        public static readonly byte Not_connected5 = 5;
        public static readonly byte Not_connected6 = 6;
        public static readonly byte Not_connected7 = 7;
        public static readonly byte Not_connected8 = 8;
        public static readonly byte CAN1_OUT4 = 9;
        public static readonly byte Secondary_Power_Supply = 10;
        public static readonly byte Not_connected11 = 11;
        public static readonly byte Not_connected12 = 12;
        public static readonly byte Not_connected13 = 13;
        public static readonly byte Not_connected14 = 14;
        public static readonly byte Not_connected15 = 15;
        public static readonly byte Not_connected16 = 16;
    }
}
