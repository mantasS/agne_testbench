using System;
using System.IO;
using System.Threading;

namespace Test_bench_AGNE
{
    public static class Logging
    {
        public static string _testResultLogFilename;
        public static string _fullTestLogFilename;
        public static string _responseFilename;
        public static string _exceptionsFilename;
        public static string _httpServerCommunicationsFile;
        public static string firwareForTesting = "firmware_not_provided";
        public static string TestsResultsPath = AppDomain.CurrentDomain.BaseDirectory + "\\Tests Logs\\" + firwareForTesting + "\\";
        public static string FullTestLogsPath = AppDomain.CurrentDomain.BaseDirectory + "\\Tests Logs\\" + firwareForTesting + "\\";
        public static string responsesPath = AppDomain.CurrentDomain.BaseDirectory + "\\Tests Logs\\" + firwareForTesting + "\\Responses\\";
        public static string ExceptionsPath = AppDomain.CurrentDomain.BaseDirectory + "\\Tests Logs\\" + firwareForTesting + "\\Exceptions\\";
        public static string HttpServerCommunicationsPath = AppDomain.CurrentDomain.BaseDirectory + "\\Tests Logs\\" + firwareForTesting + "\\Exceptions\\";
        public static Form1 form1;
        
        public static void Change_Logs_Paths_By_Firmware(string testFirmwareName)
        {
            if (testFirmwareName != "")
            {
                firwareForTesting = testFirmwareName;
                var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                TestsResultsPath = baseDirectory + "\\Tests Logs\\" + firwareForTesting + " - Results\\";
                FullTestLogsPath = baseDirectory + "\\Tests Logs\\" + firwareForTesting + "\\";
                responsesPath = baseDirectory + "\\Tests Logs\\" + firwareForTesting + "\\Responses\\";
                ExceptionsPath = baseDirectory + "\\Tests Logs\\" + firwareForTesting + "\\Exceptions\\";
                HttpServerCommunicationsPath = baseDirectory + "\\Tests Logs\\" + firwareForTesting + "\\Exceptions\\";

                Create_All_Log_Files(testFirmwareName);
            }
        }

        public static void Logs_WriteLine(string message)
        {
            Logs_Write(message + "\r\n");
            try
            {
                form1.Update_CurrentTestLogBox(message + "\r\n");
            }
            catch (Exception ex)
            {
                Logging.Write_Exceptions(ex);
            }
        }

        public static void Logs_WriteLines(string message)
        {
            var lines = message.Split('\n');
            foreach(var line in lines)
            {
                if(!string.IsNullOrWhiteSpace(line))
                {
                    try
                    {
                        using (StreamWriter sw = File.AppendText(FullTestLogsPath + _fullTestLogFilename))
                        {
                            Console.Write(DateTime.Now + ": " + line);
                            sw.Write(DateTime.Now + ": " + line);
                        }
                    }
                    catch
                    {
                        Thread.Sleep(500);
                        using (StreamWriter sw = File.AppendText(FullTestLogsPath + _fullTestLogFilename))
                        {
                            Console.Write(DateTime.Now + ": " + line);
                            sw.Write(DateTime.Now + ": " + line);
                        }
                    }

                    try
                    {
                        form1.Update_CurrentTestLogBox(line + "\n");
                    }
                    catch (Exception ex)
                    {
                        Logging.Write_Exceptions(ex);
                    }

                    if (line.Contains("PASSED") || line.Contains("FAIL"))
                    {
                        Logs_Write_Test_Results(line);
                        Space();
                    }
                }
            }
        }

        public static void Logs_Write(string message)
        {
            try
            {
                using (StreamWriter sw = File.AppendText(FullTestLogsPath + _fullTestLogFilename))
                {
                    Console.Write(DateTime.Now + ": " + message);
                    sw.Write(DateTime.Now + ": " + message);
                }
            }
            catch
            {
                Thread.Sleep(500);
                using (StreamWriter sw = File.AppendText(FullTestLogsPath + _fullTestLogFilename))
                {
                    Console.Write(DateTime.Now + ": " + message);
                    sw.Write(DateTime.Now + ": " + message);
                }
            }

        }
        public static void Logs_Write_Test_Results(string message)
        {
            try
            {
                using (StreamWriter sw = File.AppendText(TestsResultsPath + _testResultLogFilename))
                {
                    Console.Write(DateTime.Now + ": " + message);
                    sw.Write(DateTime.Now + ": " + message);
                }
            }
            catch
            {
                Thread.Sleep(500);
                using (StreamWriter sw = File.AppendText(TestsResultsPath + _testResultLogFilename))
                {
                    Console.Write(DateTime.Now + ": " + message);
                    sw.Write(DateTime.Now + ": " + message);
                }
            }

        }

        public static void Write_Response(string responseMessage)
        {
            try
            {
                Write_To_File(responsesPath + _responseFilename, responseMessage);
            }
            catch (DirectoryNotFoundException)
            {
                Create_HttpResponses_Log_File();
                Write_To_File(responsesPath + _responseFilename, responseMessage);
            }
            catch
            {
                Thread.Sleep(650);
                Write_To_File(responsesPath + _responseFilename, responseMessage);
            }
        }

        public static void Write_Exceptions(string exception)
        {
            try
            {
                Write_To_File(ExceptionsPath + _exceptionsFilename, exception);
            }
            catch (DirectoryNotFoundException)
            {
                Create_Exceptions_Log_File();
                Write_To_File(ExceptionsPath + _exceptionsFilename, exception.ToString());
            }
            catch
            {
                Thread.Sleep(150);
                Write_To_File(ExceptionsPath + _exceptionsFilename, exception);
            }
        }

        public static void Write_Exceptions(Exception exception)
        {
            try
            {
                Write_To_File(ExceptionsPath + _exceptionsFilename, exception.ToString());
            }
            catch (DirectoryNotFoundException)
            {
                Create_Exceptions_Log_File();
                Write_To_File(ExceptionsPath + _exceptionsFilename, exception.ToString());
            }
            catch
            {
                Thread.Sleep(150);
                Write_To_File(ExceptionsPath + _exceptionsFilename, exception.ToString());
            }
        }

        public static void Write_HttpServer_Communication(string message)
        {
            try
            {
                Write_To_File(HttpServerCommunicationsPath + _httpServerCommunicationsFile, message);
            }
            catch (DirectoryNotFoundException)
            {
                Create_HttpResponses_Log_File();
                Write_To_File(HttpServerCommunicationsPath + _httpServerCommunicationsFile, message);
            }
            catch
            {
                Thread.Sleep(150);
                Write_To_File(HttpServerCommunicationsPath + _httpServerCommunicationsFile, message);
            }
        }
        private static void Write_To_File(string path, string data)
        {
            try
            {
                using (StreamWriter aw = File.AppendText(path))
                {
                    aw.WriteLine(DateTime.Now + ": " + data);
                    aw.WriteLine();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void PrintHeader(long imei, string testingFirmware)
        {
            Create_All_Log_Files(testingFirmware);

            Logs_WriteLine("Testing device with IMEI: " + imei + ", Testing " + testingFirmware + " firmware");
        }

        private static void Create_Exceptions_Log_File()
        {
            _exceptionsFilename = "exceptions " + DateTime.Now.ToString("dd_MM_yyyy HH.mm.ss") + ".txt";
            Check_Existing_Folders(ExceptionsPath);
        }

        private static void Create_HttpResponses_Log_File()
        {
            _responseFilename = "responses " + DateTime.Now.ToString("dd_MM_yyyy HH.mm.ss") + ".txt";
            Check_Existing_Folders(responsesPath);
        }

        private static void Create_Log_File(string testingFirmware)
        {
            _fullTestLogFilename = testingFirmware + "   " + DateTime.Now.ToString("dd_MM_yyyy HH.mm.ss") + ".txt";
            Check_Existing_Folders(FullTestLogsPath);
        }
        private static void Create_Results_Log_File(string testingFirmware)
        {
            _testResultLogFilename = testingFirmware + "   " + DateTime.Now.ToString("dd_MM_yyyy HH.mm.ss") + ".txt";
            Check_Existing_Folders(TestsResultsPath);
        }

        private static void Create_All_Log_Files(string testingFirmware)
        {
            Create_Exceptions_Log_File();
            Create_HttpServer_Log_File();
            Create_HttpResponses_Log_File();
            Create_Log_File(testingFirmware);
            Create_Results_Log_File(testingFirmware);
        }

        private static void Create_HttpServer_Log_File()
        {
            _httpServerCommunicationsFile = DateTime.Now.ToString("dd_MM_yyyy HH.mm.ss") + ".txt";
            Check_Existing_Folders(HttpServerCommunicationsPath);
        }

        private static void Check_Existing_Folders(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        public static void Space()
        {
            Logs_WriteLine("");
        }
    }
}
